// Copyright 2020-2022 Solar Storm Interactive

#include "RyEditorK2NodesModule.h"

#define LOCTEXT_NAMESPACE "RyEditorK2NodesModule"

//---------------------------------------------------------------------------------------------------------------------
/**
*/
void FRyEditorK2NodesModule::StartupModule()
{

}

//---------------------------------------------------------------------------------------------------------------------
/**
*/
void FRyEditorK2NodesModule::ShutdownModule()
{

}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FRyEditorK2NodesModule, RyEditorK2Nodes)
