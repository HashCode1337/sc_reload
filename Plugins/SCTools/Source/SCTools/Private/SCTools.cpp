// Copyright Epic Games, Inc. All Rights Reserved.

#include "SCTools.h"
#include "SCToolsStyle.h"
#include "SCToolsCommands.h"
#include "LevelEditor.h"
#include "Widgets/Docking/SDockTab.h"
#include "Widgets/Layout/SBox.h"
#include "Widgets/Text/STextBlock.h"
#include "ToolMenus.h"
#include "Internationalization/Text.h"
#include "Widgets/Layout/SScrollBox.h"
#include "Widgets/SBoxPanel.h"
#include "Input/Reply.h"
#include "Engine/Level.h"
#include "Subsystems/AssetEditorSubsystem.h"

static const FName SCToolsTabName("SCTools");

#define LOCTEXT_NAMESPACE "FSCToolsModule"

void FSCToolsModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	FSCToolsStyle::Initialize();
	FSCToolsStyle::ReloadTextures();

	FSCToolsCommands::Register();
	
	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FSCToolsCommands::Get().OpenPluginWindow,
		FExecuteAction::CreateRaw(this, &FSCToolsModule::PluginButtonClicked),
		FCanExecuteAction());

	UToolMenus::RegisterStartupCallback(FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FSCToolsModule::RegisterMenus));
	
	FGlobalTabmanager::Get()->RegisterNomadTabSpawner(SCToolsTabName, FOnSpawnTab::CreateRaw(this, &FSCToolsModule::OnSpawnPluginTab))
		.SetDisplayName(LOCTEXT("FSCToolsTabTitle", "SCTools"))
		.SetMenuType(ETabSpawnerMenuType::Hidden);

	
}

void FSCToolsModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	UToolMenus::UnRegisterStartupCallback(this);
	UToolMenus::UnregisterOwner(this);
	FSCToolsStyle::Shutdown();
	FSCToolsCommands::Unregister();
	FGlobalTabmanager::Get()->UnregisterNomadTabSpawner(SCToolsTabName);
	AES = nullptr;
}

TSharedRef<SDockTab> FSCToolsModule::OnSpawnPluginTab(const FSpawnTabArgs& SpawnTabArgs)
{
	AES = GEditor->GetEditorSubsystem<UAssetEditorSubsystem>();

	FText WidgetText = FText::Format(
		LOCTEXT("WindowWidgetText", "Add code to {0} in {1} to override this window's contents"),
		FText::FromString(TEXT("FSCToolsModule::OnSpawnPluginTab")),
		FText::FromString(TEXT("SCTools.cpp"))
		);

	return SNew(SDockTab)
		.TabRole(ETabRole::MajorTab)
		[
			SNew(SScrollBox)
			+ SScrollBox::Slot()
			.Padding(10,5)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().HAlign(HAlign_Left)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot()
					[
						SNew(SButton)
						.Text(LOCTEXT("RootLevel", "Root Level"))
						.ButtonColorAndOpacity(FLinearColor::Yellow)
						.OnClicked_Lambda([&]() { AES->OpenEditorForAsset("/Game/Maps/Root"); return FReply::Handled(); })
					]
					+ SVerticalBox::Slot()
					[
						SNew(SButton)
						.Text(LOCTEXT("MainLevel", "Main Level"))
						.ButtonColorAndOpacity(FLinearColor::Yellow)
						.OnClicked_Lambda([&]() { AES->OpenEditorForAsset("/Game/Maps/MainLevel"); return FReply::Handled(); })
					]
					+ SVerticalBox::Slot()
					[
						SNew(SButton)
						.Text(LOCTEXT("HashCodeLevel", "HashCode Test Level"))
						.ButtonColorAndOpacity(FLinearColor::Yellow)
						.OnClicked_Lambda([&]() { AES->OpenEditorForAsset("/Game/Maps/AnimTest"); return FReply::Handled(); })
					]
					+ SVerticalBox::Slot()
					[
						SNew(SButton)
						.Text(LOCTEXT("SmolletLevel", "Smollet Test Level"))
						.ButtonColorAndOpacity(FLinearColor::Yellow)
						.OnClicked_Lambda([&]() { AES->OpenEditorForAsset("/Game/Maps/Polygon"); return FReply::Handled(); })
					]
					+ SVerticalBox::Slot()
					[
						SNew(SButton)
						.Text(LOCTEXT("OpenBPPlayer", "BP_Player"))
						.ButtonColorAndOpacity(FLinearColor::Blue)
						.OnClicked_Lambda([&](){ AES->OpenEditorForAsset("/Game/Blueprints/Prototyping/BP_Player"); return FReply::Handled(); })
					]
					+ SVerticalBox::Slot()
					[
						SNew(SButton)
						.Text(LOCTEXT("OpenBPMutantBase", "BP_MutantBase"))
						.ButtonColorAndOpacity(FLinearColor::Blue)
						.OnClicked_Lambda([&]() { AES->OpenEditorForAsset("/Game/AI/Mutants/BP_MutantBase"); return FReply::Handled(); })
					]
					+ SVerticalBox::Slot()
						[
							SNew(SButton)
							.Text(LOCTEXT("OpenBPMutantBaseLogic", "BT_MutantBase"))
						.ButtonColorAndOpacity(FLinearColor::FLinearColor(0.87f, 0.f, 1.f))
						.OnClicked_Lambda([&]() { AES->OpenEditorForAsset("/Game/AI/Mutants/BT_MutantBase"); return FReply::Handled(); })
					]
				]
			]
		];
}

void FSCToolsModule::PluginButtonClicked()
{
	FGlobalTabmanager::Get()->TryInvokeTab(SCToolsTabName);
}

void FSCToolsModule::RegisterMenus()
{
	// Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
	FToolMenuOwnerScoped OwnerScoped(this);

	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FSCToolsCommands::Get().OpenPluginWindow, PluginCommands);
		}
	}

	UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
	{
		FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("Settings");
		{
			FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FSCToolsCommands::Get().OpenPluginWindow));
			Entry.SetCommandList(PluginCommands);
		}
	}
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FSCToolsModule, SCTools)