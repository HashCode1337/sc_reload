// Copyright Epic Games, Inc. All Rights Reserved.

#include "SCToolsCommands.h"

#define LOCTEXT_NAMESPACE "FSCToolsModule"

void FSCToolsCommands::RegisterCommands()
{
	UI_COMMAND(OpenPluginWindow, "SCTools", "Bring up SCTools window", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE
