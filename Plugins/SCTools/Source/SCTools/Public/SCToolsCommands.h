// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "SCToolsStyle.h"

class FSCToolsCommands : public TCommands<FSCToolsCommands>
{
public:

	FSCToolsCommands()
		: TCommands<FSCToolsCommands>(TEXT("SCTools"), NSLOCTEXT("Contexts", "SCTools", "SCTools Plugin"), NAME_None, FSCToolsStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > OpenPluginWindow;
};