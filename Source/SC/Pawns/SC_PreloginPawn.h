// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpectatorPawn.h"
#include "Net/UnrealNetwork.h"
#include "Blueprint/UserWidget.h"
#include "SC_PreloginPawn.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FSessionReceived, FString, SessionId, APlayerController*, PlayerController);

class UUserWidget;
/**
 * 
 */
UCLASS(Blueprintable)
class SC_API ASC_PreloginPawn : public ASpectatorPawn
{
	GENERATED_BODY()
private:

	UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class UUserWidget> PreLoggedWidgetClass;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	UUserWidget* PreLoggedWidget;


public:

	UPROPERTY(BlueprintAssignable, meta = (ToolTip = "Executes only on server!"))
	FSessionReceived OnSessionReceived;

	UFUNCTION(Client, Reliable, BlueprintCallable)
	void Init_Client();
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void Init_Server(const FString& Session);
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SpawnRequest_Server(const FString& Session);

	UFUNCTION(BlueprintImplementableEvent, meta = (ToolTip = "Executes only on server!"))
	void SessionReceived(const FString& Session, APlayerController* PlayerController);

	void PossessedBy(AController* NewController) override;

public:

	//UFUNCTION(Server, Reliable, BlueprintCallable)
	//void SetSessionId_Server(const FString& SessionId);

	//UFUNCTION(Client, Reliable, BlueprintCallable)
	//void SetSessionId_Client(const FString& SessionId);

	//UFUNCTION(BlueprintNativeEvent, )
	//void OnSessionIdReceived_Client(const FString& SessionId);

protected:
	void BeginPlay() override;

};
