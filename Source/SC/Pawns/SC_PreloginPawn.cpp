// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_PreloginPawn.h"
#include "SC/Core/SC_StaticLibrary.h"
#include "SC/Core/SC_GameInstance.h"
#include "SC/Core/SC_PlayerController.h"
#include "SC/SCGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../SubSystems/SC_MetaDataSubsystem.h"

void ASC_PreloginPawn::Init_Client_Implementation()
{
	PreLoggedWidget = CreateWidget<UUserWidget>(GetWorld(), PreLoggedWidgetClass);
	PreLoggedWidget->AddToPlayerScreen();

	DisableInput(Cast<ASC_PlayerController>(GetController()));

	USC_GameInstance* GI = GetGameInstance<USC_GameInstance>();
	FString Session = GetGameInstance()->GetSubsystem<USC_MetaDataSubsystem>()->GetCommonUserData().session;

	USC_StaticLibrary::Log(ELogType::Log, "Init: Client init start");
	USC_StaticLibrary::Log(ELogType::Log, "Init: Session: " + Session);

	Init_Server(Session);

}

void ASC_PreloginPawn::Init_Server_Implementation(const FString& Session)
{
	SessionReceived(Session, Cast<ASC_PlayerController>(GetController()));
	OnSessionReceived.Broadcast(Session, Cast<ASC_PlayerController>(GetController()));
}

void ASC_PreloginPawn::SpawnRequest_Server_Implementation(const FString& Session)
{
	ASCGameMode* GM = Cast<ASCGameMode>(UGameplayStatics::GetGameMode(this));
	if (GM)
	{
		GM->PlayerSpawnRequest(Session, this);
	}
	else
	{
		USC_StaticLibrary::Log(ELogType::Fatal, "Invalid GameMode");
	}
}

void ASC_PreloginPawn::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	

	if (HasAuthority())
	{
		Init_Client();
	}
	else
	{
		Init_Client_Implementation(); 
	}

}

void ASC_PreloginPawn::BeginPlay()
{
	Super::BeginPlay();
}
