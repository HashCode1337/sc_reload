// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_DamageRifle.h"

USC_DamageRifle::USC_DamageRifle(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bCausedByWorld = false;
	bScaleMomentumByMass = true;
	bRadialDamageVelChange = true;
	DamageImpulse = 800.0f;
	DestructibleImpulse = 800.0f;
	DestructibleDamageSpreadScale = 1.0f;
	DamageFalloff = 1.0f;
}
