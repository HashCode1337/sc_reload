// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "SC_DamageRifle.generated.h"

/**
 * 
 */
UCLASS()
class SC_API USC_DamageRifle : public UDamageType
{
	GENERATED_BODY()
	
public:

	USC_DamageRifle(const FObjectInitializer& ObjectInitializer);

};
