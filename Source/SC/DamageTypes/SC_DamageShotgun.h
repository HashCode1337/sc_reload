// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "SC_DamageShotgun.generated.h"

/**
 * 
 */
UCLASS()
class SC_API USC_DamageShotgun : public UDamageType
{
	GENERATED_BODY()

public:

	USC_DamageShotgun(const FObjectInitializer& ObjectInitializer);
	
};
