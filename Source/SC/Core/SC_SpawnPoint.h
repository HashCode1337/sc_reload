// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TargetPoint.h"
#include "SC/Core/SC_CommonDataModels.h"
#include "SC_SpawnPoint.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class SC_API ASC_SpawnPoint : public ATargetPoint
{
	GENERATED_BODY()

protected:

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true", ExposeOnSpawn = "true"))
	FVectorRotation FactionSpawnOrientation;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true", ExposeOnSpawn = "true"))
	FVectorRotation GuestSpawnOrientation;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true", ExposeOnSpawn = "true"))
	EFaction Faction = EFaction::Loner;

public:

	UFUNCTION(BlueprintCallable)
	void Init();
	
};
