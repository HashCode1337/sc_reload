// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_StaticLibrary.h"
#include "Containers/UnrealString.h"
#include "Misc/FileHelper.h"
#include "Kismet/KismetStringLibrary.h"
#include "Internationalization/Regex.h"
#include "HAL/FileManager.h"
#include "JsonObjectConverter.h"
#include "Components/CapsuleComponent.h"

#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"

#include "AIController.h"
#include "NavigationSystem.h"
#include "NavigationSystemTypes.h"
#include "../AI/SC_AIPerceptionComponent.h"
#include "Perception/AISense_Sight.h"
#include "Misc/AssertionMacros.h"
#include "../AI/SC_AIStaticHelpers.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "PhysicalMaterials/PhysicalMaterialMask.h"
#include "Kismet/GameplayStatics.h"
#include "../SubSystems/SC_GameConfigSubsystem.h"

DEFINE_LOG_CATEGORY(LogConfigs);
DEFINE_LOG_CATEGORY(SCLoger);


//====================================================================================================//

const FString USC_StaticLibrary::BackendRegisterRequest = TEXT("/register?login=%s&email=%s&password=%s");

//====================================================================================================//


void USC_StaticLibrary::ParseCredentials(const FString& InputString, FString& Login, FString& Pwd)
{
	TArray<FString> ParsedArray;
	FString ParseSymbol = GetGlobalParseSymbol();
	FString Lgn;
	FString Pw;

	InputString.ParseIntoArray(ParsedArray, *ParseSymbol);

	Login = ParsedArray[1];
	Pwd = ParsedArray[2];
}

FString USC_StaticLibrary::GetVersion()
{
	FString Ver;

	GConfig->GetString(
		TEXT("/Script/EngineSettings.GeneralProjectSettings"),
		TEXT("ProjectVersion"),
		Ver,
		GGameIni
	);

	return Ver;
}

void USC_StaticLibrary::CombineLogin(const FString& Login, const FString& Pwd, FString& ResultMessage)
{
	FString StrEnum = UKismetStringLibrary::Conv_IntToString((int)ELoginType::Login);
	FString ParseSymbol = GetGlobalParseSymbol();
	ResultMessage = StrEnum + ParseSymbol + Login + ParseSymbol + Pwd;
}

void USC_StaticLibrary::CombineRegister(const FString& Login, const FString& Pwd, FString& ResultMessage)
{
	FString StrEnum = UKismetStringLibrary::Conv_IntToString((int)ELoginType::Register);
	FString ParseSymbol = GetGlobalParseSymbol();
	ResultMessage = StrEnum + ParseSymbol + Login + ParseSymbol + Pwd;
}

ELoginType USC_StaticLibrary::RecognizeMsg(FString Message)
{
	FString LeftMsg;

	Message.Split(USC_StaticLibrary::GetGlobalParseSymbol(), &LeftMsg, nullptr, ESearchCase::IgnoreCase, ESearchDir::FromStart);

	int32 ans = UKismetStringLibrary::Conv_StringToInt(LeftMsg);
	ELoginType CastedAnswer = (ELoginType)ans;

	switch (CastedAnswer)
	{
	case ELoginType::Login:
		break;
	case ELoginType::Register:
		break;
	default:
		USC_StaticLibrary::PrintMsg(0, FColor::Red, "ERROR: USC_StaticLibrary::RecognizeMsg(), switch case not implemented yet");
		break;
	}

	return CastedAnswer;
}

bool USC_StaticLibrary::GetBoolFromParsed(const FString& InputStr, int ParamIndex)
{
	TArray<FString> ResultArr;
	InputStr.ParseIntoArray(ResultArr, *GetGlobalParseSymbol());

	if (ResultArr.IsValidIndex(ParamIndex))
	{
		FString Res = ResultArr[ParamIndex];
		bool ConvertedRes = (bool)UKismetStringLibrary::Conv_StringToInt(Res);

		if (ConvertedRes)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

void USC_StaticLibrary::PrintMsg(int key, FColor Color, FString Msg)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(key, 5, Color, Msg);
	}
}

void USC_StaticLibrary::Log(ELogType SCLogType, FString Msg)
{
	switch (SCLogType)
	{
	case ELogType::Log:
		UE_LOG(SCLoger, Log, TEXT("%s"), *Msg);
		break;
	case ELogType::Warn:
		UE_LOG(SCLoger, Warning, TEXT("%s"), *Msg);
		break;
	case ELogType::Error:
		UE_LOG(SCLoger, Error, TEXT("%s"), *Msg);
		break;
	case ELogType::Fatal:
		UE_LOG(SCLoger, Fatal, TEXT("%s"), *Msg);
		break;
	default:
		break;
	}

}

bool USC_StaticLibrary::LoadTextFromFile(FString& Result, FString FileDestination)
{
	return FFileHelper::LoadFileToString(Result, *FileDestination);
}

bool USC_StaticLibrary::WriteTextToFile(FString String, FString FileDestination)
{
	return FFileHelper::SaveStringToFile(String, *FileDestination);
}

bool USC_StaticLibrary::SortByDistance(AActor* DistanceFrom, TArray<AActor*>& Actors)
{
	if (Actors.Num() <= 0) return false;

	Actors.Sort([DistanceFrom](const AActor& A, const AActor& B){
		float DistanceA = A.GetDistanceTo(DistanceFrom);
		float DistanceB = B.GetDistanceTo(DistanceFrom);
		return DistanceA < DistanceB;
	});

	return true;
}

bool USC_StaticLibrary::RegexMatch(const FString& Rules, const FString& Arg)
{
	const FRegexPattern Ptrn = FRegexPattern(Rules);
	FRegexMatcher Mtchs = FRegexMatcher(Ptrn, Arg);
	
	bool find = Mtchs.FindNext();

	return find;
}

bool USC_StaticLibrary::Ragdoll(UPrimitiveComponent* Subject)
{

	if (Cast<USkeletalMeshComponent>(Subject))
	{
		if (ACharacter* Character = Cast<ACharacter>(Subject->GetOwner()))
		{
			Character->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}
		Subject->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		Subject->SetSimulatePhysics(true);
		return true;
	}
	return false;

}

void USC_StaticLibrary::RagdollWithImpact(UPrimitiveComponent* MeshComponent, FVector ImpactLocation /*= FVector::ZeroVector*/, float ImpactForce /*= 0.0f*/)
{
	if (Ragdoll(MeshComponent) && ImpactLocation != FVector::ZeroVector && ImpactForce != 0.0f)
	{
		ImpactForce *= MeshComponent->GetMass();
		MeshComponent->AddRadialImpulse(ImpactLocation, 100.0f, ImpactForce, ERadialImpulseFalloff::RIF_Linear);
	}
}

FHitResult USC_StaticLibrary::TraceLine(AActor* Instigator, FVector StartLoc, FVector EndLoc, ECollisionChannel Chanel)
{
	FHitResult HitRes;
	
	FCollisionQueryParams QP;
	QP.bReturnPhysicalMaterial = true;
	QP.bTraceComplex = true;
	QP.bReturnFaceIndex = true;
	QP.AddIgnoredActor(Instigator);

	FCollisionResponseParams QRP;

	Instigator->GetWorld()->LineTraceSingleByChannel
	(
		HitRes,
		StartLoc,
		EndLoc,
		Chanel,
		QP,
		QRP
	);

	return HitRes;
}

int32 USC_StaticLibrary::GetPhysMatIndex(const FHitResult& Hit, int32 UVChannel, FVector2D& UV)
{
	if (!Hit.bBlockingHit || Hit.Component == nullptr) return -1;
	
	UMaterialInterface* Material = Hit.Component->GetMaterial(Hit.ElementIndex);
	if (!Material) return -1;
	
	UV = FVector2D(ForceInitToZero);
	bool bSuccess = UGameplayStatics::FindCollisionUV(Hit, UVChannel, UV);

	UPhysicalMaterialMask* PhysMatMask = Material->GetPhysicalMaterialMask();
	if (!bSuccess || !PhysMatMask) return -1;

	TArray<uint32> Mask;
	int32 SizeX = 0;
	int32 SizeY = 0;

	PhysMatMask->GenerateMaskData(Mask, SizeX, SizeY);
	return UPhysicalMaterialMask::GetPhysMatIndex(Mask, SizeX, SizeY, TA_Wrap, TA_Wrap, UV.X, UV.Y);

}

EPhysicalSurface USC_StaticLibrary::GetPhysMatType(const FHitResult& Hit, int32 UVChannel, FVector2D& UV)
{
	int32 PhysMatIndex = GetPhysMatIndex(Hit, UVChannel, UV);

	if (PhysMatIndex == -1)
	{
		return EPhysicalSurface::SurfaceType_Default;
	}

	// Adding +1 because RED is 0, so 0 is Default material and we dont want RED to be 0, because this is not default
	return static_cast<EPhysicalSurface>(PhysMatIndex + 1);
}

bool USC_StaticLibrary::LineTraceForPhysicalSurface
(
	const AActor* Instigator,
	const FVector& Start,
	const FVector& End,
	const FCollisionQueryParams& Query,
	FName Profile,
	FHitResult& OutResult
)
{
	FHitResult HitResult;
	TArray<AActor*> IgnoreUs;
	UKismetSystemLibrary::LineTraceSingleByProfile(Instigator, Start, End, Profile, true, IgnoreUs, EDrawDebugTrace::None, HitResult, true);

	// exit if no blocking hit
	if (!HitResult.bBlockingHit) return false;

	FVector2D UV(ForceInitToZero);
	EPhysicalSurface Surface = GetPhysMatType(HitResult, 0, UV);

	// exit if it has not material mask
	if (Surface == EPhysicalSurface::SurfaceType_Default)
	{
		OutResult = HitResult;
		return true;
	}

	HitResult.PhysMaterial->SurfaceType = Surface;
	OutResult = HitResult;

	return true;
}

bool USC_StaticLibrary::CapsuleTraceForPhysicalSurface
(
	const AActor* Instigator,
	const FVector& Start,
	const FVector& End,
	const FCollisionQueryParams& Query,
	FName Profile,
	FHitResult& OutResult
)
{
	FHitResult HitResult;
	TArray<AActor*> IgnoreUs;
	UKismetSystemLibrary::CapsuleTraceSingleByProfile(Instigator, Start, End, 50.f, 50.f, Profile, true, IgnoreUs, EDrawDebugTrace::ForDuration, HitResult, true);

	// exit if no blocking hit
	if (!HitResult.bBlockingHit) return false;

	FVector2D UV(ForceInitToZero);
	EPhysicalSurface Surface = GetPhysMatType(HitResult, 0, UV);

	// exit if it has not material mask
	if (Surface == EPhysicalSurface::SurfaceType_Default)
	{
		OutResult = HitResult;
		return true;
	}

	HitResult.PhysMaterial->SurfaceType = Surface;
	OutResult = HitResult;

	return true;
}

//void USC_StaticLibrary::GenerateMaskData(UPhysicalMaterialMask* InMask, TArray<int32>& OutMaskData, int32& OutSizeX, int32& OutSizeY)
//{
//	TArray<uint32> Converted;
//	Converted.Append(OutMaskData);
//
//	if (!InMask)
//	{
//		USC_StaticLibrary::PrintMsg(0, FColor::Red, "error");
//		return;
//	}
//
//	InMask->GenerateMaskData(Converted, OutSizeX, OutSizeY);
//
//	OutMaskData.Empty();
//	OutMaskData.Append(Converted);
//}
//
//int32 USC_StaticLibrary::GetPhysMatIndex(const TArray<int32>& MaskData, int32 SizeX, int32 SizeY, float U, float V)
//{
//	TArray<uint32> Converted;
//	Converted.Append(MaskData);
//
//	return UPhysicalMaterialMask::GetPhysMatIndex(Converted, SizeX, SizeY, TA_Wrap, TA_Wrap, U, V);
//}
