// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_GameState.h"
#include "SC/SCGameMode.h"
#include "Kismet/GameplayStatics.h"

ASC_GameState::ASC_GameState(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bReplicates = true;
}


void ASC_GameState::PostInitializeComponents()
{
	AGameStateBase::PostInitializeComponents();

	
}

void ASC_GameState::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ASC_GameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ASC_GameState, SkySphereRotation);
}