// Fill out your copyright notice in the Description page of Project Settings.


#include "ObjectExtended.h"

bool UObjectExtended::GetGameInstanceSubsystem(TSubclassOf<UGameInstanceSubsystem> SubsystemType, UGameInstanceSubsystem*& Subsystem)
{
	const TArray<UGameInstanceSubsystem*>& SubSystems = GetWorld()->GetGameInstance()->GetSubsystemArray<UGameInstanceSubsystem>();

	for (int i = 0; i < SubSystems.Num(); ++i)
	{
		UGameInstanceSubsystem* CurrentSubsystem = SubSystems[i];

		if (CurrentSubsystem->GetClass() == SubsystemType)
		{
			Subsystem = CurrentSubsystem;
			return true;
		}

	}

	return false;
}
