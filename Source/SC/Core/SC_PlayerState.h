// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "SC_CommonDataModels.h"
#include "SC/Interfaces/SC_DatabaseSavable.h"
#include "SC_PlayerState.generated.h"

/**
 * 
 */
UCLASS()
class SC_API ASC_PlayerState : public APlayerState
{
	GENERATED_BODY()

private:
	// Data for backend
	UPROPERTY()
	FDatabaseObjectDetails m_DatabaseObjectDetails;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	FPlayerAccountData m_PlayerAccountData;

public:
	UFUNCTION(BlueprintCallable)
	void SetNickAfterConnect(FString Name);
	UFUNCTION(BlueprintCallable)
	void GetDatabasableData(FDatabaseObjectDetails& DatabaseObjectDetails, FPlayerAccountData& PlayerAccountData);
	UFUNCTION(BlueprintPure)
	uint8 GetFactionId();

#pragma region BackendCaching
	UFUNCTION(BlueprintCallable)
	void SetDatabaseObjectDetails(const FDatabaseObjectDetails& DatabaseObjectDetails);
	UFUNCTION(BlueprintCallable)
	void SetPlayerAccountData(const FPlayerAccountData& PlayerAccountData);
	UFUNCTION(BlueprintCallable)
	void UpdateSavableCache(const FString& Cache);
	UFUNCTION(BlueprintCallable)
	void GetSavableCache(FString& Cache);

#pragma endregion
};
