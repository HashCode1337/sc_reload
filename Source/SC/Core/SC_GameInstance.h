// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "SC_CommonDataModels.h"
#include "SC_GameInstance.generated.h"

UCLASS(BlueprintType)
class SC_API USC_GameInstance : public UGameInstance
{
	GENERATED_BODY()

protected:
	virtual void Shutdown() override;
private:
	void SendPlayerExit();
	
public:
	void Init() override;
	UFUNCTION(BlueprintImplementableEvent)
	void InitDone();

};
