// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_GameInstance.h"
#include "JsonObjectConverter.h"
#include "SC_StaticLibrary.h"

#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"
#include "../SubSystems/SC_MetaDataSubsystem.h"

void USC_GameInstance::Shutdown()
{
	Super::Shutdown();
}

void USC_GameInstance::Init()
{
	Super::Init();
	InitDone();
}
