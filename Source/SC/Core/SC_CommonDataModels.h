// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "GameFramework/Character.h"
#include "../Service/MySQLBPLibrary.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GenericTeamAgentInterface.h"
#include "SC_CommonDataModels.generated.h"


// Weapon
//===============================================================================//

UENUM(BlueprintType)
enum class EWeaponClass : uint8
{
	Free			= 0 UMETA(DisplayName = "Free"),
	BM16			= 1 UMETA(DisplayName = "BM16"),
	KrissVector		= 2 UMETA(DisplayName = "KrissVector"),
	Ak74u			= 3 UMETA(DisplayName = "Ak74u")
};

UENUM(BlueprintType)
enum class EWeaponReloadingState : uint8
{
	Unknown			= 0 UMETA(DisplayName = "Unknown")/*,
	ReloadingStarted = 1 UMETA(DisplayName = "Unknown"),
	InsertMagazine = 2 UMETA(DisplayName = "Unknown"),
	InsertMagazine = 2 UMETA(DisplayName = "Unknown"),*/
	
};

UENUM(BlueprintType)
enum class EWeaponModuleType: uint8
{
	Unknown			= 0 UMETA(DisplayName = "Unknown"),
	Barrel			= 1 UMETA(DisplayName = "Barrel"),
	UnderBarrel		= 2 UMETA(DisplayName = "UnderBarrel"),
	PistolGrip		= 3 UMETA(DisplayName = "PistolGrip"),
	Scope			= 4 UMETA(DisplayName = "Scope"),
	Handguard		= 5 UMETA(DisplayName = "Handguard"),
	SightFront		= 6 UMETA(DisplayName = "SightFront"),
	SightRear		= 7 UMETA(DisplayName = "SightRear"),
	Stock			= 8 UMETA(DisplayName = "Stock"),
	Rail			= 9 UMETA(DisplayName = "Rail"),
	Muzzle			= 10 UMETA(DisplayName = "Muzzle")
};

UENUM(BlueprintType)
enum class ECharacterWeaponState : uint8
{
	Lowered			= 0 UMETA(DisplayName = "Lowered"),
	Raised			= 1 UMETA(DisplayName = "Raised"),
	Aiming			= 2 UMETA(DisplayName = "Aiming")
	// TODO Unarmed = 3 UMETA(DisplayName = "Unarmed")
};

UENUM(BlueprintType)
enum class EWeaponFireMode : uint8
{
	Safe			= 0 UMETA(DisplayName = "Safe"),
	Single			= 1 UMETA(DisplayName = "Single"),
	Burst			= 2 UMETA(DisplayName = "Burst"),
	Auto			= 3 UMETA(DisplayName = "Auto"),
	Douplet			= 4 UMETA(DisplayName = "Douplet")
};

// AI
//===============================================================================//



UENUM(BlueprintType)
enum class EForceTaskFinishType : uint8
{
	Ignore = 0 UMETA(DisplayName = "Ignore"),
	Fail = 1 UMETA(DisplayName = "Fail"),
	Success = 2 UMETA(DisplayName = "Success")
};

UENUM(BlueprintType)
enum class EAIState: uint8
{
	Unknown			= 0 UMETA(DisplayName = "Unknown"),
	WaitingTask		= 1 UMETA(DisplayName = "WaitingTask"),
	Walk			= 2 UMETA(DisplayName = "Walk"),
	Chase			= 3 UMETA(DisplayName = "Chase"),
	Attack			= 4 UMETA(DisplayName = "Attack"),
	Search			= 5 UMETA(DisplayName = "Search"),
	Danger			= 6 UMETA(DisplayName = "Danger"),
	Runaway			= 7 UMETA(DisplayName = "Runaway")

	//Sleep			= 0 UMETA(DisplayName = "Sleep"),
	//Walk			= 1 UMETA(DisplayName = "Walk"),
	//Eat			= 2 UMETA(DisplayName = "Eat"),
	//Attack		= 3 UMETA(DisplayName = "Attack"),
	//Danger		= 4 UMETA(DisplayName = "Danger"),
	//Search		= 5 UMETA(DisplayName = "Search"),
	//Idle			= 6 UMETA(DisplayName = "Idle"),
	//WaitingTask	= 7 UMETA(DisplayName = "WaitingTask"),
	//Hide			= 8 UMETA(DisplayName = "Hide"),
	//Chase			= 9 UMETA(DisplayName = "Chase"),
	//Unknown		= 10 UMETA(DisplayName = "Unknown")
};

// Common
//===============================================================================//

UENUM(BlueprintType)
enum class EMovementSpeed : uint8
{
	Walk			 = 0 UMETA(DisplayName = "Walk"),
	Run				 = 1 UMETA(DisplayName = "Run"),
	Sprint			 = 2 UMETA(DisplayName = "Sprint")
};

UENUM(BlueprintType)
enum class EHttpRequestType : uint8
{
	Get				= 0 UMETA(DisplayName = "Get"),
	Post			= 1 UMETA(DisplayName = "Post")
};

USTRUCT(BlueprintType)
struct FVectorRotation
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FVector Location;
	UPROPERTY(BlueprintReadWrite)
	FRotator Rotation;
};

UENUM(BlueprintType)
enum class EViewType : uint8
{
	MainMenuView	= 0 UMETA(DisplayName = "MainMenuView"),
	AuthView		= 1 UMETA(DisplayName = "AuthView"),
	LoggedInView	= 2 UMETA(DisplayName = "LoggedInView"),
	RegisterView	= 3 UMETA(DisplayName = "RegisterView")
};

UENUM(BlueprintType)
enum class ELogType : uint8
{
	Log				= 0 UMETA(DisplayName = "Log"),
	Warn			= 1 UMETA(DisplayName = "Warn"),
	Error			= 2 UMETA(DisplayName = "Error"),
	Fatal			= 3 UMETA(DisplayName = "Fatal")
};

// Factions
//===============================================================================//

UENUM(BlueprintType)
enum class EFaction : uint8
{
	Unknown			= 0 UMETA(DisplayName = "Unknown"),
	Loner			= 1 UMETA(DisplayName = "Loner"),
	Neutral			= 2 UMETA(DisplayName = "Neutral"),
	Bandit			= 3 UMETA(DisplayName = "Bandit"),
	Army			= 4 UMETA(DisplayName = "Army"),
	Dolg			= 5 UMETA(DisplayName = "Dolg"),
	Freedom			= 6 UMETA(DisplayName = "Freedom"),
	Science			= 7 UMETA(DisplayName = "Science"),
	Mercenary		= 8 UMETA(DisplayName = "Mercenary"),
	Monolith		= 9 UMETA(DisplayName = "Monolith"),
	Clearsky		= 10 UMETA(DisplayName = "Clearsky"),
	Traders			= 11 UMETA(DisplayName = "Traders"),
	Mutant_1		= 12 UMETA(DisplayName = "Mutant_1"),
	Mutant_2		= 13 UMETA(DisplayName = "Mutant_2"),
	Mutant_3		= 14 UMETA(DisplayName = "Mutant_3"),
	Mutant_4		= 15 UMETA(DisplayName = "Mutant_4"),
	Mutant_5		= 16 UMETA(DisplayName = "Mutant_5"),
	Custom_1		= 17 UMETA(DisplayName = "Custom_1"),
	Custom_2		= 18 UMETA(DisplayName = "Custom_2"),
	Custom_3		= 19 UMETA(DisplayName = "Custom_3"),
	Custom_4		= 20 UMETA(DisplayName = "Custom_4"),
	Custom_5		= 21 UMETA(DisplayName = "Custom_5")
};

USTRUCT(BlueprintType)
struct FFactionData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	EFaction id;
	UPROPERTY(BlueprintReadWrite)
	FString name;
	UPROPERTY(BlueprintReadWrite)
	int bank;
	UPROPERTY(BlueprintReadWrite)
	int max_member;
	UPROPERTY(BlueprintReadWrite)
	FVectorRotation respawn;
	UPROPERTY(BlueprintReadWrite)
	FVectorRotation guest;
};

USTRUCT(BlueprintType)
struct FFactionsDataArray
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	TArray<FFactionData> Factions;
};

USTRUCT(BlueprintType)
struct FFactionRelations
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<EFaction> Enemies;
};

// DataBase and relative
//===============================================================================//

USTRUCT(BlueprintType)
struct FSuperVisorConfig
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FString FreeCamera;
};

USTRUCT(BlueprintType)
struct FGameConfig
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FString Backend;

	UPROPERTY(BlueprintReadWrite)
	FSuperVisorConfig SuperVisorConfig;
};

// DataBase Account Model
USTRUCT(BlueprintType)
struct FCommonUserData
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintReadOnly)
	int64 id;
	UPROPERTY(BlueprintReadOnly)
	FString session;
	UPROPERTY(BlueprintReadOnly)
	FString guid;
	UPROPERTY(BlueprintReadOnly)
	FString account;
	UPROPERTY(BlueprintReadOnly)
	FString custom_name;
	UPROPERTY(BlueprintReadOnly)
	FString email;
	UPROPERTY(BlueprintReadOnly)
	FString password;
	UPROPERTY(BlueprintReadOnly)
	EFaction faction;
	UPROPERTY(BlueprintReadOnly)
	uint8 admin_level;
	UPROPERTY(BlueprintReadOnly)
	uint8 prem_level;
	UPROPERTY(BlueprintReadOnly)
	uint8 beta_tester;
	UPROPERTY(BlueprintReadOnly)
	uint8 whitelisted;
	UPROPERTY(BlueprintReadOnly)
	FString first_connect;
	UPROPERTY(BlueprintReadOnly)
	FString last_connect;
	UPROPERTY(BlueprintReadOnly)
	FString blocked_until;
	UPROPERTY(BlueprintReadOnly)
	int64 connects;
	UPROPERTY(BlueprintReadOnly)
	float time_played;
	UPROPERTY(BlueprintReadOnly)
	int32 kicks;
	UPROPERTY(BlueprintReadOnly)
	int32 bans;
};

USTRUCT(BlueprintType)
struct FPlayerAccountData
{
	GENERATED_BODY()
public:
	
	UPROPERTY(BlueprintReadWrite)
	FString guid;
	UPROPERTY(BlueprintReadWrite)
	FString account;
	UPROPERTY(BlueprintReadWrite)
	FString custom_name;
	UPROPERTY(BlueprintReadWrite)
	EFaction faction;
	UPROPERTY(BlueprintReadWrite)
	int admin_level;
	UPROPERTY(BlueprintReadWrite)
	int prem_level;
	UPROPERTY(BlueprintReadWrite)
	int beta_tester;
};

USTRUCT(BlueprintType)
struct FPlayerCharacterData
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintReadWrite)
	int database_id;
	UPROPERTY(BlueprintReadWrite)
	FVector location;
	UPROPERTY(BlueprintReadWrite)
	FRotator rotation;
	UPROPERTY(BlueprintReadWrite)
	uint8 health;
	UPROPERTY(BlueprintReadWrite)
	uint8 food;
	UPROPERTY(BlueprintReadWrite)
	uint8 water;
	UPROPERTY(BlueprintReadWrite)
	uint8 radiation;
	UPROPERTY(BlueprintReadWrite)
	uint8 psy;
};

UENUM(BlueprintType)
enum class EAnswerType : uint8
{
	Unknown = 0 UMETA(DisplayName = "Unknown"),
	AccountExist = 1 UMETA(DisplayName = "AccountExist"),
	GetWholeAccount = 2 UMETA(DisplayName = "GetWholeAccount")
};

UENUM(BlueprintType)
enum class ELoginType : uint8
{
	Login = 0 UMETA(DisplayName = "Login"),
	Register = 1 UMETA(DisplayName = "Register")
};

UENUM(BlueprintType)
enum class ERegisterError : uint8
{
	Unknown = 0 UMETA(DisplayName = "Unknown"),
	AlreadyExist = 1 UMETA(DisplayName = "AlreadyExist"),
	BadLogin = 2 UMETA(DisplayName = "BadLogin"),
	BadPassword = 3 UMETA(DisplayName = "BadPassword"),
	PwdNotMatch = 4 UMETA(DisplayName = "PwdNotMatch"),
	BadMail = 5 UMETA(DisplayName = "BadMail"),
	EmailAlreadyExist = 6 UMETA(DisplayName = "EmailAlreadyExist")
};

UENUM(BlueprintType)
enum class ELoginError : uint8
{
	Unknown = 0 UMETA(DisplayName = "Unknown"),
	InvalidCredentials = 1 UMETA(DisplayName = "InvalidCredentials"),
	BadLogin = 2 UMETA(DisplayName = "BadLogin"),
	BadPassword = 3 UMETA(DisplayName = "BadPassword"),
	NotExist = 4 UMETA(DisplayName = "NotExist"),
	AccountNotActivated = 5 UMETA(DisplayName = "AccountNotActivated"),
	NotWhiteListed = 6 UMETA(DisplayName = "NotWhiteListed"),
	OnlyDevsMode = 7 UMETA(DisplayName = "OnlyDevsMode")
};

UENUM(BlueprintType)
enum class ELoggedError : uint8
{
	Unknown = 0 UMETA(DisplayName = "Unknown"),
	NicknameAlreadyInUse = 1 UMETA(DisplayName = "NicknameAlreadyInUse"),
	InvalidNickname = 2 UMETA(DisplayName = "InvalidNickname")
};

// Delete it with dependencies (refactor)
USTRUCT(BlueprintType)
struct FDataBaseAnswer : public FTableRowBase
{
	GENERATED_BODY()

	EAnswerType AnswerType = EAnswerType::Unknown;
	TArray<FMySQLDataTable> ResultsByColumn;
	TArray<FMySQLDataRow> ResultsByRow;
};

// Backend
//===============================================================================//

// Base model of backend answer, Answer field must contain JSON
USTRUCT(BlueprintType)
struct FBackendAnswer : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	bool Success;
	UPROPERTY(BlueprintReadOnly)
	FString Answer;
	UPROPERTY(BlueprintReadOnly)
	FString Error;
};

// Data provided by backend answer
USTRUCT(BlueprintType)
struct FCommonGameInfo
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int64 sessions = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 version = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool dev_mode = true;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString game_server_addr = "";

};

//===============================================================================//Answer":"{\"sessions\":0,\"version\":0,\"dev_mode\":1,\"game_server_addr\":\"95.165.134.115:7777\"

UCLASS()
class SC_API USC_CommonDataModels : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintPure)
	static FName Tag_FootStep_BP() { return Tag_Footstep; };
	static const FName Tag_Footstep;

	UFUNCTION(BlueprintPure)
	static FName Tag_Damage_BP() { return Tag_Damage; };
	static const FName Tag_Damage;
};
