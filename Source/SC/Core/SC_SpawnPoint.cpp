// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_SpawnPoint.h"

void ASC_SpawnPoint::Init()
{
	SetActorLocationAndRotation(FactionSpawnOrientation.Location, FactionSpawnOrientation.Rotation);
}
