// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_TransitionGameMode.h"
#include "SC_GameInstance.h"

USC_GameInstance* ASC_TransitionGameMode::GetGameInstance()
{
	return Cast<USC_GameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
}


UWorld* ASC_TransitionGameMode::GetMainLevel()
{
	return MainLevel;
}
