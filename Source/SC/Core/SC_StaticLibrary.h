// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include <SC/Core/SC_CommonDataModels.h>
#include "Misc/DateTime.h"
#include "GenericTeamAgentInterface.h"
#include "Perception/AIPerceptionComponent.h"
#include "CollisionQueryParams.h"
#include "PhysicsEngine/BodyInstance.h"
#include "PhysicsEngine/BodySetup.h"
#include "Math/Vector2D.h"


#include "SC_StaticLibrary.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogConfigs, Display, All);
DECLARE_LOG_CATEGORY_EXTERN(SCLoger, Display, All);

class UBodySetup;
class AAIController;
class UPrimitiveComponent;
class USC_AIPerceptionComponent;
struct FAIStimulus;

UCLASS()
class SC_API USC_StaticLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	// Backend
	//====================================================================================================//

	/*1. Login
	2. Email
	3. Password*/
	static const FString BackendRegisterRequest;

	//====================================================================================================//

public:

	// Static Settings
	//====================================================================================================//

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse Symbol"))
	static FString GetGlobalParseSymbol() {return ";";};

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse Symbol"))
	static FString GetNicknameRgxPattern() { FString Pattern = TEXT("^[A-Za-z0-9]{4,20}$");	return Pattern; };

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse Symbol"))
	static FString GetPasswordRgxPattern() { FString Pattern = TEXT("^[A-Za-z0-9]{4,20}$");	return Pattern; };

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse Symbol"))
	static FString GetEmailRgxPattern() {FString Pattern = TEXT("^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$"); return Pattern; };

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Project Version"))
	static FString GetVersion();

	// Functions
	//====================================================================================================//

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse Credentials"))
	static void ParseCredentials(const FString& InputString, FString& Login, FString& Pwd);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Combine Login"))
	static void CombineLogin(const FString& Login, const FString& Pwd, FString& ResultMessage);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Combine Login"))
	static void CombineRegister(const FString& Login, const FString& Pwd, FString& ResultMessage);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse Recognize"))
	static ELoginType RecognizeMsg(FString Message);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "Parse"))
	static bool GetBoolFromParsed(const FString& InputStr, int ParamIndex);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	static bool RegexMatch(const FString& Rules, const FString& Arg);

	UFUNCTION(BlueprintCallable)
	static bool Ragdoll(UPrimitiveComponent* Subject);

	UFUNCTION(BlueprintCallable)
	static void RagdollWithImpact(UPrimitiveComponent* MeshComponent, FVector ImpactLocation = FVector::ZeroVector, float ImpactForce = 0.0f);


	// Util
	//====================================================================================================//

	UFUNCTION(BlueprintCallable)
	static void PrintMsg(int key, FColor Color, FString Msg);

	UFUNCTION(BlueprintCallable)
	static void Log(ELogType SCLogType, FString Msg);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	static bool LoadTextFromFile(FString& Result, FString FileDestination);

	UFUNCTION(BlueprintCallable)
	static bool WriteTextToFile(FString String, FString FileDestination);

	UFUNCTION(BlueprintCallable)
	static bool SortByDistance(AActor* DistanceFrom, TArray<AActor*>& Actors);

	// AI
	//====================================================================================================//

	
	// Testing
	//====================================================================================================//

	UFUNCTION(BlueprintCallable)
	static FHitResult TraceLine(AActor* Instigator, FVector StartLoc, FVector EndLoc, ECollisionChannel Chanel);

	/*UFUNCTION(BlueprintCallable)
	static void GenerateMaskData(UPhysicalMaterialMask* InMask, TArray<int32>& OutMaskData, int32& OutSizeX, int32& OutSizeY);

	UFUNCTION(BlueprintCallable)
	static int32 GetPhysMatIndex(const TArray<int32>& MaskData, int32 SizeX, int32 SizeY, float U, float V);*/

	UFUNCTION(BlueprintCallable)
	static int32 GetPhysMatIndex(const FHitResult& Hit, int32 UVChannel, FVector2D& UV);

	UFUNCTION(BlueprintCallable)
	static EPhysicalSurface GetPhysMatType(const FHitResult& Hit, int32 UVChannel, FVector2D& UV);

	static bool LineTraceForPhysicalSurface(const AActor* Instigator, const FVector& Start, const FVector& End, const FCollisionQueryParams& Query, FName Profile, FHitResult& OutResult);
	static bool CapsuleTraceForPhysicalSurface(const AActor* Instigator, const FVector& Start, const FVector& End, const FCollisionQueryParams& Query, FName Profile, FHitResult& OutResult);
};
