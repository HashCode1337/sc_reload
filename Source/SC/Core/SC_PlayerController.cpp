// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_PlayerController.h"
#include "SC_GameInstance.h"
#include "SC_PlayerState.h"
#include "SC_StaticLibrary.h"
#include "../Interfaces/SC_Interactable.h"
#include "../Interfaces/SC_Outlinable.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../SCCharacter.h"
#include "../SCBaseWeapon.h"
#include "../SubSystems/SC_MetaDataSubsystem.h"
#include "../Components/SC_PawnInteractionComponent.h"

FString ASC_PlayerController::GetMyName()
{
	USC_GameInstance* GI = Cast<USC_GameInstance>(GetGameInstance());
	FString PlayerName = GetGameInstance()->GetSubsystem<USC_MetaDataSubsystem>()->GetCommonUserData().custom_name;

	if (GI) return PlayerName;
	else return "ERROR";
}

void ASC_PlayerController::ApplyNickName_Implementation(const FString& Name)
{
	ASC_PlayerState* PS = GetPlayerState<ASC_PlayerState>();
	if (PS)PS->SetNickAfterConnect(Name);
	else USC_StaticLibrary::Log(ELogType::Error, "ERROR: ASC_PlayerController::ApplyNickName_Implementation, Can't cast to ASC_PlayerState! Are you testing something?");
}

void ASC_PlayerController::Respawn()
{
	OnRespawn.Broadcast();
}

void ASC_PlayerController::UpdateCursorTarget(const FHitResult& HitResult)
{
	if (!HitResult.bBlockingHit)
	{
		ISC_Interactable* OldInteractable = Cast<ISC_Interactable>(CursorTarget);
		if (OldInteractable) OldInteractable->SetOutlineEnabled(false);
		CursorTarget = nullptr;

		return;
	}

	AActor* NewTarget = HitResult.GetActor();

	if (CursorTarget != NewTarget)
	{
		ISC_Interactable* OldInteractable = Cast<ISC_Interactable>(CursorTarget);
		if (OldInteractable) OldInteractable->SetOutlineEnabled(false);
		CursorTarget = NewTarget;
	}

	ISC_Interactable* CurrentInteractable = Cast<ISC_Interactable>(NewTarget);
	if (CurrentInteractable->IsInteractable()) CurrentInteractable->SetOutlineEnabled(true);
	else CurrentInteractable->SetOutlineEnabled(false);

}

void ASC_PlayerController::RequestInteract()
{
	float CurrentTime = GetWorld()->GetUnpausedTimeSeconds();
	if (CurrentTime < LastInteractTime + InteractCooldown) return;

	LastInteractTime = CurrentTime;
	APawn* MyPawn = GetPawn();
	
	if (MyPawn && IsValid(CursorTarget))
	{
		USC_PawnInteractionComponent* InteractionComponent = Cast<USC_PawnInteractionComponent>(MyPawn->GetComponentByClass(USC_PawnInteractionComponent::StaticClass()));
		if (IsValid(InteractionComponent) && UKismetSystemLibrary::DoesImplementInterface(CursorTarget, USC_Interactable::StaticClass()))
		{
			InteractionComponent->RequestInteract(Cast<ISC_Interactable>(CursorTarget));
		}
		else
		{
			USC_StaticLibrary::Log(ELogType::Fatal, "No Interaction comp");
		}
	}

	//if (CursorTarget) S_Interact(CursorTarget);
}

void ASC_PlayerController::FirePressed()
{
	ASCCharacter* MyCharacterPawn = GetPawn<ASCCharacter>();
	if (MyCharacterPawn) MyCharacterPawn->SetFiring(true);
}

void ASC_PlayerController::FireReleased()
{
	ASCCharacter* MyCharacterPawn = GetPawn<ASCCharacter>();
	if (MyCharacterPawn) MyCharacterPawn->SetFiring(false);
}

FGenericTeamId ASC_PlayerController::GetFactionId()
{
	return GetGenericTeamId();
}

void ASC_PlayerController::S_Interact_Implementation(AActor* PlayerCursorTarget)
{
	
}

void ASC_PlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	FVector TraceForInteractionsStart = PlayerCameraManager->GetCameraLocation();
	FVector TraceForInteractionsEnd = TraceForInteractionsStart + (PlayerCameraManager->GetCameraRotation().Vector() * 300.f);

	TArray<AActor*> Ignore;
	FHitResult TraceResult;

	bool bTraceSuccess = UKismetSystemLibrary::LineTraceSingleForObjects
	(
		GetWorld(),
		TraceForInteractionsStart,
		TraceForInteractionsEnd,
		InteractableObjectTypes,
		false,
		Ignore,
		EDrawDebugTrace::None,
		TraceResult,
		true
	);

	UpdateCursorTarget(TraceResult);
}

#pragma region overrides

void ASC_PlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority()) ApplyNickName_Implementation(GetMyName());
	else ApplyNickName(GetMyName());
}

#pragma endregion

void ASC_PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	if (InputComponent)
	{
		InputComponent->BindAction("Interact", IE_Pressed, this, &ASC_PlayerController::RequestInteract);
		InputComponent->BindAction("Fire", IE_Pressed, this, &ASC_PlayerController::FirePressed);
		InputComponent->BindAction("Fire", IE_Released, this, &ASC_PlayerController::FireReleased);
	}
}

void ASC_PlayerController::SetGenericTeamId(const FGenericTeamId& TeamID) {}


FGenericTeamId ASC_PlayerController::GetGenericTeamId() const
{
	ASC_PlayerState* PS = GetPlayerState<ASC_PlayerState>();
	if (PS) return FGenericTeamId(PS->GetFactionId());
	return 255;
}
