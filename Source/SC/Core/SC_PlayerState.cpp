// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_PlayerState.h"

void ASC_PlayerState::SetNickAfterConnect(FString Name)
{
	auto OldName = GetOldPlayerName();
	SetPlayerName(Name);
}

void ASC_PlayerState::GetDatabasableData(FDatabaseObjectDetails& DatabaseObjectDetails, FPlayerAccountData& PlayerAccountData)
{
	DatabaseObjectDetails = this->m_DatabaseObjectDetails;
	PlayerAccountData = this->m_PlayerAccountData;
}

uint8 ASC_PlayerState::GetFactionId()
{
	return static_cast<uint8>(m_PlayerAccountData.faction);
}

void ASC_PlayerState::SetDatabaseObjectDetails(const FDatabaseObjectDetails& DatabaseObjectDetails)
{
	this->m_DatabaseObjectDetails = DatabaseObjectDetails;
}

void ASC_PlayerState::SetPlayerAccountData(const FPlayerAccountData& PlayerAccountData)
{
	this->m_PlayerAccountData = PlayerAccountData;
}

void ASC_PlayerState::UpdateSavableCache(const FString& Cache)
{
	this->m_DatabaseObjectDetails.JsonContent = Cache;
}

void ASC_PlayerState::GetSavableCache(FString& Cache)
{
	Cache = m_DatabaseObjectDetails.JsonContent;
}
