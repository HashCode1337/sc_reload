// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GenericTeamAgentInterface.h"
#include "SC_PlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRespawn);

/**
 * 
 */
UCLASS()
class SC_API ASC_PlayerController : public APlayerController, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<TEnumAsByte<EObjectTypeQuery>> InteractableObjectTypes;

public:

	UPROPERTY()
	AActor* CursorTarget;

	UPROPERTY()
	float LastInteractTime = -1.f;

	UPROPERTY()
	float InteractCooldown = 0.5f;

public:
	UFUNCTION(BlueprintCallable)
	FString GetMyName();

	UFUNCTION(BlueprintCallable, Server, Reliable)
	void ApplyNickName(const FString& Name);

	UFUNCTION(BlueprintCallable)
	void Respawn();

	UFUNCTION()
	void UpdateCursorTarget(const FHitResult& HitResult);

	UFUNCTION()
	void RequestInteract();

	UFUNCTION()
	void FirePressed();
	UFUNCTION()
	void FireReleased();


	UFUNCTION(BlueprintPure)
	FGenericTeamId GetFactionId();

	UFUNCTION(Server, Unreliable)
	void S_Interact(AActor* PlayerCursorTarget);

	UPROPERTY(BlueprintAssignable, Category = "SC")
	FRespawn OnRespawn;

public:

	void PlayerTick(float DeltaTime) override;

public:
	virtual void BeginPlay() override;
	
protected:

	void SetupInputComponent() override;

private:

	void SetGenericTeamId(const FGenericTeamId& TeamID) override;
	FGenericTeamId GetGenericTeamId() const override;

};
