// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ObjectExtended.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class SC_API UObjectExtended : public UObject
{
	GENERATED_BODY()

	UFUNCTION(BlueprintPure)
	bool GetGameInstanceSubsystem(TSubclassOf<UGameInstanceSubsystem> SubsystemType, UGameInstanceSubsystem*& Subsystem);
	
};
