// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "../Core/SC_CommonDataModels.h"
#include "SC_MetaDataSubsystem.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class SC_API USC_MetaDataSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

private:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta = (AllowPrivateAccess))
	FCommonGameInfo CommonGameInfo;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess))
	FCommonUserData UserData;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess))
	bool PlayerLogged = false;

private:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	void SendLogout();

public:
	UFUNCTION(BlueprintCallable)
	void SetName(FString NewName);
	UFUNCTION(BlueprintCallable)
	void SetCommonGameInfo(FCommonGameInfo NewCommonGameInfo);
	UFUNCTION(BlueprintCallable)
	void SetCommonUserData(FString BackendAnswer);

public:
	UFUNCTION(BlueprintPure)
	FCommonGameInfo GetCommonGameInfo() const { return CommonGameInfo; };
	UFUNCTION(BlueprintPure)
	FCommonUserData GetCommonUserData() const { return UserData; };

};
