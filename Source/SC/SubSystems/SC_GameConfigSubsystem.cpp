// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_GameConfigSubsystem.h"
#include "Misc/Paths.h"
#include "JsonObjectConverter.h"
#include "../Core/SC_StaticLibrary.h"

bool USC_GameConfigSubsystem::IsConfigExist()
{
	FString Path = FPaths::ProjectDir() + DefaultConfigName;
	return IFileManager::Get().FileExists(*Path);
}

void USC_GameConfigSubsystem::CreateConfig()
{
	GameConfig.Backend = DefaultBackendAddr;
	GameConfig.SuperVisorConfig.FreeCamera = DefaultFreeCameraKey;

	FString GameConfigString;
	FString Path = FPaths::ProjectDir() + DefaultConfigName;

	FJsonObjectConverter::UStructToJsonObjectString(GameConfig, GameConfigString);
	FFileHelper::SaveStringToFile(GameConfigString, *Path);
}

void USC_GameConfigSubsystem::LoadConfig()
{
	FString GameConfigString;
	FString Path = FPaths::ProjectDir() + DefaultConfigName;

	checkf(FFileHelper::LoadFileToString(GameConfigString, *Path), TEXT("USC_GameConfigSubsystem::LoadConfig(), Can't load game config!!!"));

	FJsonObjectConverter::JsonObjectStringToUStruct(GameConfigString, &GameConfig);
}

void USC_GameConfigSubsystem::Deinitialize()
{
}

void USC_GameConfigSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	if (!IsConfigExist())
	{
		CreateConfig();
		LoadConfig();
	}
	else
	{
		LoadConfig();
	}
}
