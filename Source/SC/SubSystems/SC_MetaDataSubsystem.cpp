// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_MetaDataSubsystem.h"
#include "JsonObjectConverter.h"
#include "SC_GameConfigSubsystem.h"
#include "../Service/SC_HttpRequest.h"

void USC_MetaDataSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{

}

void USC_MetaDataSubsystem::Deinitialize()
{
	SendLogout();
}

void USC_MetaDataSubsystem::SendLogout()
{
	if (!PlayerLogged) return;

	FString BackendAddr = GetGameInstance()->GetSubsystem<USC_GameConfigSubsystem>()->GetGameConfig().Backend;
	FString RequestBody = FString::Printf(TEXT("/disconnect?session=%s"), *UserData.session);
	FString FullRequest = FString(BackendAddr + RequestBody);

	USC_HttpRequest* Request = NewObject<USC_HttpRequest>(this);
	Request->EnableCallback(false);
	Request->MakeGet(FullRequest);

}

void USC_MetaDataSubsystem::SetName(FString NewName)
{
	UserData.custom_name = NewName;
}

void USC_MetaDataSubsystem::SetCommonGameInfo(FCommonGameInfo NewCommonGameInfo)
{
	this->CommonGameInfo = NewCommonGameInfo;
}

void USC_MetaDataSubsystem::SetCommonUserData(FString BackendAnswer)
{
	FJsonObjectConverter::JsonObjectStringToUStruct(BackendAnswer, &UserData);
	PlayerLogged = true;
}
