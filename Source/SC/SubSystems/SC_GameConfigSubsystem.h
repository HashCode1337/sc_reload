// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "../Core/SC_CommonDataModels.h"
#include "SC_GameConfigSubsystem.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class SC_API USC_GameConfigSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

private:
	const FString DefaultConfigName = TEXT("GameConfig.json");
	const FString DefaultBackendAddr = TEXT("95.165.134.115:1415");
	const FString DefaultFreeCameraKey = TEXT("Num 6");
	
	FGameConfig GameConfig;

private:
	bool IsConfigExist();
	void CreateConfig();
	void LoadConfig();

public:
	UFUNCTION(BlueprintPure)
	FGameConfig GetGameConfig() const { return GameConfig; };

public:
	void Deinitialize() override;
	void Initialize(FSubsystemCollectionBase& Collection) override;




};
