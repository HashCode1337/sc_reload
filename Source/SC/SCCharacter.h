// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Interfaces/SC_DatabaseSavable.h"
#include "Core/SC_CommonDataModels.h"
#include "Components/SC_HealthComponent.h"
#include "Components/FPSTemplate_CharacterComponent.h"
#include "Animation/SC_FirstPersonAnimInstance.h"
#include "Animation/SC_ThirdPersonAnimInstance.h"
#include "GenericTeamAgentInterface.h"
#include "Components/SC_CharWeaponHandler.h"
#include "Interfaces/SC_Damagable.h"
#include "Core/SC_StaticLibrary.h"
#include "Components/SC_PawnInteractionComponent.h"
#include "SCCharacter.generated.h"

class UInputComponent;
class UFPSTemplate_CharacterComponent;
class USC_WeaponFireStatusHandler;

UCLASS(config=Game)
class ASCCharacter : public ACharacter, public ISC_DatabaseSavable, public ISC_Damagable, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:

	UPROPERTY(Replicated, BlueprintReadWrite, EditDefaultsOnly)
	bool Aiming = false;

	UPROPERTY(Replicated, BlueprintReadWrite, EditDefaultsOnly)
	bool Sprinting = false;

	UPROPERTY(Replicated, BlueprintReadWrite, EditDefaultsOnly)
	ECharacterWeaponState WeaponState = ECharacterWeaponState::Lowered;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	USC_CharWeaponHandler* CharWeaponHandler;

	UPROPERTY(Replicated, BlueprintReadWrite, EditDefaultsOnly)
	USC_WeaponFireStatusHandler* WeaponStatusHandler;

	UPROPERTY(Replicated, BlueprintReadWrite, EditDefaultsOnly)
	USC_PawnInteractionComponent* InteractionComponent;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	FName RightHandSocketName = TEXT("bip01_r_hand");

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float DefaultFOV = 90.f;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float CurrentFOV = 90.f;

	void Tick(float DeltaSeconds) override;
	void PostInitializeComponents() override;


	// IDamagable
	void Damaged(FDamagedTenParams DamageDescription) override
	{
		M_PlaySoundPain();
	};
	
	void Die(FDamagedTenParams DamageDescription) override
	{
		M_ImpactRagdoll(DamageDescription.HitLocation, DamageDescription.DamageType.GetDefaultObject()->DamageImpulse);	
	};
	
	UFUNCTION(BlueprintPure)
	float GetHealth() override
	{
		USC_HealthComponent* HealthComp = CastChecked<USC_HealthComponent>(GetComponentByClass(USC_HealthComponent::StaticClass()));

		if (HealthComp)
		{
			return HealthComp->GetHealthUncompressed();
		}
		else
		{
			USC_StaticLibrary::Log(ELogType::Fatal, "ASCCharacter::GetHealth() Cast fatal error!");
			return 0;
		}
	};

protected:
	
	UPROPERTY()
	FPlayerCharacterData m_PlayerCharacterData;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	uint8 GetRemoteViewPitch() const { return RemoteViewPitch; };


#pragma region WeaponSway

	UPROPERTY(BlueprintReadOnly)
	FVector2D LastSwayOffset;

	UPROPERTY(BlueprintReadOnly)
	FVector2D CurrentSwayOffset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSway", meta = (AllowPrivateAccess = "true"))
	float SwaySpeed = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSway", meta = (AllowPrivateAccess = "true"))
	float SwayMultiplier = 1.0f;

#pragma endregion

#pragma region Core

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* Camera;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USkeletalMeshComponent* FirstPersonMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UAudioComponent* AudioComp;
	
	UPROPERTY(BlueprintReadOnly)
	USC_FirstPersonAnimInstance* FPAnimInstance;

	UPROPERTY(BlueprintReadOnly)
	USkeletalMeshComponent* ThirdPersonMesh = this->GetMesh();

	UPROPERTY(BlueprintReadOnly)
	USC_ThirdPersonAnimInstance* TPAnimInstance;

#pragma endregion

#pragma region Props

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USoundBase* SoundPain;

#pragma endregion

public:

#pragma region Core
	ASCCharacter(const FObjectInitializer& ObjectInitializer);
	void BeginPlay() override;
	void InputTurn(float val);
	void InputLookUp(float val);
	void CycleWeaponState(FKey Key);

	void AimPressed();
	void AimReleased();
	
	void SetFiring(bool bFiring);

	UFUNCTION(Server, Unreliable, BlueprintCallable)
	void S_SetAiming(bool bAim);
	UFUNCTION(Server, Unreliable, BlueprintCallable)
	void S_SetArmed(ECharacterWeaponState NewStatus);

	UFUNCTION(BlueprintCallable)
	ASCBaseWeapon* GetCurrentWeapon() { return CharWeaponHandler->CurrentWeapon; };

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FVector2D GetWeaponSwayOffset() { return CurrentSwayOffset; };

	UFUNCTION(BlueprintCallable)
	UCameraComponent* GetCameraComponent() { return Camera; };

	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void M_PlaySoundPain();

	UFUNCTION(NetMulticast, Unreliable)
	void M_ImpactRagdoll(FVector ImpactLocation = FVector::ZeroVector, float ImpactForce = 0.0f);

#pragma endregion

#pragma region AnimRelative
	// First person leadgun virtual bone transform
	UFUNCTION(BlueprintCallable)
	FTransform GetFPHandGunSocketTransform(ERelativeTransformSpace RTS);

	UFUNCTION(BlueprintCallable)
	void PlayFireMontage(UAnimMontage* TPFireMontage, UAnimMontage* FPFireMontage);
	
	UFUNCTION(BlueprintCallable)
	void PlayReloadMontage(UAnimMontage* TPReloadMontage, UAnimMontage* FPReloadMontage);


#pragma endregion

#pragma region BackendCaching
	UFUNCTION(BlueprintCallable)
	void SetPlayerAccountData(const FPlayerAccountData& PlayerAccountData);
	UFUNCTION(BlueprintCallable)
	void SetPlayerCharacterData(const FPlayerCharacterData& PlayerCharacterData);
#pragma endregion

#pragma region DatabaseSavable
	UFUNCTION(BlueprintCallable)
	void SetupDatabaseObjectDetails(int DatabaseId, FString DatabaseGuid, EDatabaseSaveRequestType Type) override;
	UFUNCTION(BlueprintCallable)
	void CacheContent() override;
	UFUNCTION(BlueprintCallable)
	void MakeFullSaveRequest() override;
#pragma endregion

	void UnPossessed() override;
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

private:
	FGenericTeamId GetGenericTeamId() const override;

};

