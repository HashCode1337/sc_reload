// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BTT_SC_Base.h"
#include "BTT_SC_Attack.generated.h"

/**
 * 
 */
UCLASS()
class SC_API UBTT_SC_Attack : public UBTT_SC_Base
{
	GENERATED_BODY()
	
protected:
	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

};
