// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTD_BoolKeyCheck.generated.h"

/**
 * 
 */

UCLASS()
class SC_API UBTD_BoolKeyCheck : public UBTDecorator
{
	GENERATED_BODY()

	UBTD_BoolKeyCheck(const FObjectInitializer& ObjectInitializer);

public:

	UPROPERTY(EditAnywhere)
	bool bCondition = true;

	/** blackboard key selector */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector BlackboardKey;
	
protected:
	bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
	void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:
	void InitializeFromAsset(UBehaviorTree& Asset) override;
	FString GetStaticDescription() const override;

};
