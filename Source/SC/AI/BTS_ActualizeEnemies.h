// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "SC_AIController.h"
#include "SC_AICharacterBase.h"
#include "BTS_ActualizeEnemies.generated.h"

/**
 * 
 */
UCLASS()
class SC_API UBTS_ActualizeEnemies : public UBTService
{
	GENERATED_BODY()

		UBTS_ActualizeEnemies(const FObjectInitializer& ObjectInitializer);

public:

	/* aggressor will be written into that key */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector AggressorKey;

	/* aggressor location will be written into that key */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector AggressorLocationKey;

	/* search location will be written into that key */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector SearchLocationKey;

	/* hide from location will be written into that key */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector HideFromLocationKey;

public:

	UPROPERTY(BlueprintReadOnly) ASC_AIController* AIController;
	UPROPERTY(BlueprintReadOnly) ASC_AICharacterBase* AICharacter;

	UPROPERTY(BlueprintReadOnly) TArray<AActor*> ActualEnemies;
	UPROPERTY(BlueprintReadOnly) TArray<AActor*> ExpiringEnemies;
	//UPROPERTY(BlueprintReadOnly) TArray<AActor*> ReachableEnemies;

private:

	UFUNCTION() void UpdateEnemies();
	UFUNCTION() void WorkActualEnemies();
	UFUNCTION() void WorkExpiringEnemies();
	UFUNCTION() void ClearBBKeys();

protected:
	void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	void OnSearchStart(FBehaviorTreeSearchData& SearchData) override;
private:
	void OnInstanceCreated(UBehaviorTreeComponent& OwnerComp) override;
};
