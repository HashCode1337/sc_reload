// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_AIStaticHelpers.h"
#include "Perception/AISense_Prediction.h"
#include "Perception/AIPerceptionComponent.h"
#include "NavigationSystem.h"
#include "Perception/AISense_Sight.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"
#include "../Core/SC_StaticLibrary.h"
#include "../Components/SC_HealthComponent.h"

bool USC_AIStaticHelpers::PredictActorPosition(APawn* Pawn, AActor* TargetActor, float PredictTime, FAIStimulus& ResultStimulus)
{
	bool Result = false;
	AAIController* Controller = Cast<AAIController>(Pawn->GetController());

	if (!Controller || !Controller->PerceptionComponent) return Result;
	
	UAISense_Prediction::RequestPawnPredictionEvent(Pawn, TargetActor, PredictTime);

	const FActorPerceptionInfo* ActorInfo = Controller->PerceptionComponent->GetActorInfo(*TargetActor);
	for (FAIStimulus CurStimulus : ActorInfo->LastSensedStimuli)
	{
		if (UAIPerceptionSystem::GetSenseClassForStimulus(Controller, CurStimulus) == UAISense_Prediction::StaticClass())
		{
			ResultStimulus = CurStimulus;
			Result = true;
			continue;
		}
	}

	return Result;
}

bool USC_AIStaticHelpers::GetPredictedChasingLoc(APawn* Pawn, AActor* TargetActor, float PredictUntilDistance, FVector& OutLocation)
{
	if (!Pawn || !TargetActor) return false;

	float DistanceBetween = FVector::Dist(Pawn->GetActorLocation(), TargetActor->GetActorLocation());
	float MaxDistance = 1000.f;

	if (DistanceBetween > MaxDistance || DistanceBetween < PredictUntilDistance)
	{
		OutLocation = TargetActor->GetActorLocation();
	}

	return true;
}

bool USC_AIStaticHelpers::CanReachLocation(AAIController* Controller, FVector Destination)
{
	if (!Controller) return false;

	UWorld* WorldContext = Controller->GetWorld();

	UNavigationSystemV1* NavSys = FNavigationSystem::GetCurrent<UNavigationSystemV1>(WorldContext);
	const FNavAgentProperties& AgentProps = Controller->GetNavAgentPropertiesRef();

	FNavLocation ProjectedLocation;
	if (NavSys && !NavSys->ProjectPointToNavigation(Destination, ProjectedLocation, INVALID_NAVEXTENT, &AgentProps))
	{
		return false;
	}

	const AActor& PawnAsActor = *Cast<AActor>(Controller->GetPawn());
	FVector PawnLoc = Controller->GetPawn()->GetActorLocation();

	const ANavigationData& NavData = *Cast<ANavigationData>(NavSys->GetNavDataForActor(PawnAsActor));

	FPathFindingQuery Query = FPathFindingQuery(Controller, NavData, PawnLoc, Destination);
	FPathFindingResult PathResult = NavSys->FindPathSync(Query);

	if (!PathResult.IsSuccessful())
	{
		return false;
	}

	if (PathResult.IsPartial())
	{
		return false;
	}

	return true;
}

bool USC_AIStaticHelpers::GetAliveSensedEnemies(UAIPerceptionComponent* PerceptionComp, TArray<AActor*>& OutEnemies)
{
	TArray<AActor*> AllEnemies;
	PerceptionComp->GetHostileActors(AllEnemies);

	// only alive actors
	AllEnemies = AllEnemies.FilterByPredicate([](AActor* IterActor) {
		USC_HealthComponent* HealthComp = Cast<USC_HealthComponent>(IterActor->GetComponentByClass(USC_HealthComponent::StaticClass()));
		if (HealthComp)
		{
			if (HealthComp->IsAlive() && !IterActor->IsHidden())
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		return true;

		});

	if (AllEnemies.Num() > 0)
	{
		OutEnemies = AllEnemies;
		return true;
	}

	return false;
}

bool USC_AIStaticHelpers::GetAliveSensedEnemiesByClass(UAIPerceptionComponent* PerceptionComp, TSubclassOf<UAISense> SenseClass, TArray<AActor*>& OutEnemies)
{
	TArray<AActor*> AllEnemies;
	PerceptionComp->GetHostileActorsBySense(SenseClass, AllEnemies);

	// only alive actors
	AllEnemies = AllEnemies.FilterByPredicate([](AActor* IterActor) {
		USC_HealthComponent* HealthComp = Cast<USC_HealthComponent>(IterActor->GetComponentByClass(USC_HealthComponent::StaticClass()));
		if (HealthComp)
		{
			if (HealthComp->IsAlive() && !IterActor->IsHidden())
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		return true;

		});

	if (AllEnemies.Num() > 0)
	{
		OutEnemies = AllEnemies;
		return true;
	}

	return false;
}

FAIStimulus USC_AIStaticHelpers::GetSightSenseAboutActor(AAIController* AIController, AActor* Actor)
{
	FAIStimulus Result;

	const FActorPerceptionInfo* ActorInfo = AIController->PerceptionComponent->GetActorInfo(*Actor);
	for (FAIStimulus CurStimulus : ActorInfo->LastSensedStimuli)
	{
		if (UAIPerceptionSystem::GetSenseClassForStimulus(AIController, CurStimulus) == UAISense_Sight::StaticClass())
		{
			return CurStimulus;
			continue;
		}
	}

	return Result;
}

AActor* USC_AIStaticHelpers::GetNearestReachableActor(AAIController* Controller, const TArray<AActor*>& InActors)
{
	AActor* ResultActor = nullptr;
	float NearestDist = 999999.f;

	for (AActor* IterActor : InActors)
	{
		// Check if can reach by navmesh
		if (!USC_AIStaticHelpers::CanReachLocation(Controller, IterActor->GetActorLocation()))
			continue;

		// Get distance between
		float DistToIterActor = FVector::Dist(Controller->GetPawn()->GetActorLocation(), IterActor->GetActorLocation());

		// Check if closer than previous
		if (DistToIterActor < NearestDist)
		{
			NearestDist = DistToIterActor;
			ResultActor = IterActor;
		}
		else
		{
			continue;
		}
	}

	return ResultActor;
}

AActor* USC_AIStaticHelpers::GetNearestActor(AAIController* Controller, TArray<AActor*>& InActors, bool& bSuccess)
{
	AActor* ResultActor = nullptr;
	float NearestDist = 999999.f;
	bSuccess = false;

	for (AActor* IterActor : InActors)
	{
		// Get distance between
		float DistToIterActor = FVector::Dist(Controller->GetPawn()->GetActorLocation(), IterActor->GetActorLocation());

		// Check if closer than previous
		if (DistToIterActor < NearestDist)
		{
			NearestDist = DistToIterActor;
			ResultActor = IterActor;
			bSuccess = true;
		}
		else
		{
			continue;
		}
	}

	return ResultActor;
}

bool USC_AIStaticHelpers::GetKnownEnemies(AAIController* AIController, const TArray<AActor*>& InEnemies, TMap<AActor*, bool>& OutEnemies)
{
	bool Result = false;

	for (AActor* IterEnemy : InEnemies)
	{
		const FActorPerceptionInfo* ActorInfo = AIController->PerceptionComponent->GetActorInfo(*IterEnemy);
		FAIStimulus CurStimulus = GetSightSenseAboutActor(AIController, IterEnemy);

		if (CurStimulus.IsValid())
		{
			bool FreshAge = CurStimulus.GetAge() > 0 ? false : true;
			OutEnemies.Add(IterEnemy, FreshAge);

			if (FreshAge)
				Result = true;

		}
	}

	return Result;
}

bool USC_AIStaticHelpers::GetSortedKnownEnemies(AAIController* AIController, const TArray<AActor*>& InEnemies, TArray<AActor*>& Expiring, TArray<AActor*>& Actual)
{
	TMap<AActor*, bool> EnemiesMapped;
	GetKnownEnemies(AIController, InEnemies, EnemiesMapped);

	Expiring.Empty();
	Actual.Empty();

	if (EnemiesMapped.Num() <= 0) return false;

	for (TPair<AActor*, bool> CurEnemy : EnemiesMapped)
	{
		if (CurEnemy.Value)
			Actual.Add(CurEnemy.Key);
		else
			Expiring.Add(CurEnemy.Key);
	}

	return true;
}
