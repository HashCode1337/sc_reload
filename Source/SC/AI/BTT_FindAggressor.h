// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_FindAggressor.generated.h"

/**
 * 
 */
UCLASS()
class SC_API UBTT_FindAggressor : public UBTTaskNode
{
	GENERATED_BODY()

	UBTT_FindAggressor(const FObjectInitializer& ObjectInitializer);
	
public:

	/* aggressor will be written into that key */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector AggressorKey;

	/* aggressor location will be written into that key */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector AggressorLocationKey;

	/* search location will be written into that key */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector SearchLocationKey;

	/* hide from location will be written into that key */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector HideFromLocationKey;

	/* predict enemy position until distance between us become lower than that value 
	after that will be used sight stimulus location */
	UPROPERTY(EditAnywhere)
	float PredictUntilDistance = 750.f;

private:

	UPROPERTY()
	TArray<AActor*> Actual;
	UPROPERTY()
	TArray<AActor*> Expiring;


private:
	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	FString GetStaticDescription() const override;

};
