// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GenericTeamAgentInterface.h"
#include "../Core/SC_CommonDataModels.h"
#include "../Components/SC_HealthComponent.h"
#include "../Interfaces/SC_Damagable.h"
#include "../Core/SC_StaticLibrary.h"
#include "SC_AIPerceptionComponent.h"
#include "SC_AIController.h"
#include "SC_AICharacterBase.generated.h"

class UAnimMontage;
class ASC_AIController;

UCLASS(BlueprintType)
class SC_API ASC_AICharacterBase : public ACharacter, public ISC_Damagable, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	EFaction OverrideControllerFaction = EFaction::Unknown;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FVector SpawnLocation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TMap<EMovementSpeed, float> MovementModes;

	// Anims

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<UAnimMontage*> StartSleepAnims;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<UAnimMontage*> StopSleepAnims;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<UAnimMontage*> MeleeAttackAnims;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<UAnimMontage*> DangerAnims;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<UAnimMontage*> EatAnims;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<UAnimMontage*> AttackAnims;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<UAnimMontage*> AttackInMoveAnims;

	// Sounds
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class USoundCue* AggressiveSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class USoundCue* PainSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class USoundCue* DeathSound;


	// Sockets
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<FName> AttackSockets;


	void PossessedBy(AController* NewController) override;

	// IDamagable
	void Damaged(FDamagedTenParams DamageDescription) override;
	void Die(FDamagedTenParams DamageDescription) override;
	
	UFUNCTION(BlueprintPure)
	float GetHealth() override
	{
		USC_HealthComponent* HealthComp = CastChecked<USC_HealthComponent>(GetComponentByClass(USC_HealthComponent::StaticClass()));

		if (HealthComp)
		{
			return HealthComp->GetHealthUncompressed();
		}
		else
		{
			USC_StaticLibrary::Log(ELogType::Fatal, TEXT("ASCCharacter::GetHealth() Cast fatal error!"));
			return 0;
		}
	};

public:
	ASC_AICharacterBase();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UAudioComponent* AudioComp;

public:

	UFUNCTION(BlueprintCallable)
	void SetMovementMode(EMovementSpeed NewSpeed);
	
	UFUNCTION(BlueprintPure)
	ASC_AIController* GetAIController()
	{
		if (ASC_AIController* MyController = Cast<ASC_AIController>(GetController()))
		{
			return MyController;
		}
		else
		{
			USC_StaticLibrary::Log(ELogType::Fatal, TEXT("ASC_AICharacterBase::GetAIController() Cast Controller fails!"));
			return nullptr;
		}
		
	}

	UFUNCTION(BlueprintPure)
	USC_AIPerceptionComponent* GetAIPerception()
	{
		if (USC_AIPerceptionComponent* MyPerception = Cast<USC_AIPerceptionComponent>(GetAIController()->PerceptionComponent))
		{
			return MyPerception;
		}
		else
		{
			USC_StaticLibrary::Log(ELogType::Fatal, TEXT("ASC_AICharacterBase::GetAIPerception() Cast perception fails!"));
			return nullptr;
		}
	}

	// Net
	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void M_PlaySoundAggressive();
	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void M_PlaySoundPain();
	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void M_PlaySoundDeath();
	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void M_PlayDangerMontageByIndex(int32 Index);
	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void M_PlayAttackMontageByIndex(int32 Index);
	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void M_PlayAttackInMoveMontageByIndex(int32 Index);
	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void M_ImpactRagdoll(FVector ImpactLocation = FVector::ZeroVector, float ImpactForce = 0.0f);


public:	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	FGenericTeamId GetGenericTeamId() const override;
	ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;

};
