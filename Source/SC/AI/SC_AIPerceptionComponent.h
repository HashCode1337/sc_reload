// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Perception/AIPerceptionComponent.h"
#include "../Core/SC_CommonDataModels.h"
#include "AIController.h"
#include "SC_AIPerceptionComponent.generated.h"

class ASC_AICharacterBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAIStateChangedSignature, EAIState, NewState, EAIState, OldState);

USTRUCT(BlueprintType)
struct FStimulusSightLost
{
	GENERATED_BODY();
	
	UPROPERTY(BlueprintReadOnly)	FAIStimulus StimulusSightLost;
	UPROPERTY(BlueprintReadOnly)	float TimeWhenLost;
	UPROPERTY(BlueprintReadOnly)	float MaxAge;

	FStimulusSightLost() {};
	FStimulusSightLost(float TimeWhenLost, float MaxAge)
	{
		this->TimeWhenLost			= TimeWhenLost;
		this->MaxAge				= MaxAge;
	}

	bool IsAgeExpired(const UObject* WoldContextObject)
	{
		float CurTime = WoldContextObject->GetWorld()->UnpausedTimeSeconds;
		bool bExpired = CurTime > TimeWhenLost + MaxAge;
		return bExpired;
	}
};

UCLASS()
class SC_API USC_AIPerceptionComponent : public UAIPerceptionComponent
{
	GENERATED_BODY()

public:

	//------------------------------------------------------------------------------------------//

	UPROPERTY(BlueprintReadOnly)	TMap<AActor*, FAIStimulus> StimuliSight;
	UPROPERTY(BlueprintReadOnly)	TMap<AActor*, FAIStimulus> StimuliHear;
	UPROPERTY(BlueprintReadOnly)	TMap<AActor*, FAIStimulus> StimuliDamaged;
	UPROPERTY(BlueprintReadOnly)	TMap<AActor*, FStimulusSightLost> StimuliSightLost;
	
	UPROPERTY(BlueprintReadOnly)	EAIState PreferredAIState = EAIState::WaitingTask;
	UPROPERTY(BlueprintReadOnly)	AActor* PreferredAggressor;
	UPROPERTY(BlueprintReadOnly)	FVector PreferredSearchLocation;

	//------------------------------------------------------------------------------------------//

	UPROPERTY(EditDefaultsOnly)		float RunawayHealthThreshold = 30.f;
	UPROPERTY(EditDefaultsOnly)		float RoutineUpdateFrequency = 30.f;

	UPROPERTY(BlueprintAssignable)
	FOnAIStateChangedSignature OnAIStateChanged;

	//------------------------------------------------------------------------------------------//


private:

	USC_AIPerceptionComponent(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintPure)
	FGenericTeamId GetMyTeam() { return GetTeamIdentifier(); };

	UFUNCTION()
	void OnPerception(AActor* Actor, FAIStimulus Stimulus);
	void AddLostSightEnemy(AActor* Actor, FAIStimulus Stimulus);
	void RefreshLostSightEnemiesMap();
	EAIState UpdateAiState();
	bool GetHealthIsCriticalLow();
	void Routine();

public:
	ASC_AICharacterBase* GetAICharacter();

	void OnRegister() override;
	void OnUnregister() override;

};
