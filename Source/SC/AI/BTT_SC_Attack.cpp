// Fill out your copyright notice in the Description page of Project Settings.


#include "BTT_SC_Attack.h"
#include "SC_AIPerceptionComponent.h"
#include "SC_AICharacterBase.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Tasks/AITask_MoveTo.h"
#include "Kismet/KismetMathLibrary.h"

EBTNodeResult::Type UBTT_SC_Attack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	EBTNodeResult::Type ParentResult = Super::ExecuteTask(OwnerComp, NodeMemory);
	AActor* EnemyActor = AIPerception->PreferredAggressor;
	
	FVector EnemyLocation = EnemyActor->GetActorLocation();
	FVector MyLocation = AICharacter->GetActorLocation();

	float DistToEnemy = FVector::Dist(MyLocation, EnemyLocation);
	float MySpeed = 600.f;
	float EnemySpeed = EnemyActor->GetVelocity().Size();

	FVector EnemyDirection = EnemyActor->GetVelocity();
	EnemyDirection.Normalize();

	FVector PredictedLocation = EnemyLocation + (EnemyDirection * EnemySpeed);
	FVector DirToPredictedLocation = PredictedLocation - MyLocation;
	DirToPredictedLocation.Z = 0.f;
	DirToPredictedLocation.Normalize();

	FVector Destination = EnemyLocation;
	int32 RandomAnim = -1;
	bool EnemyMoving = EnemySpeed > 0.1f;
	float AnimLength = 1.f;

	if (EnemyMoving)
	{
		RandomAnim = UKismetMathLibrary::RandomInteger(AICharacter->AttackInMoveAnims.Num());
		AnimLength = AICharacter->AttackInMoveAnims[RandomAnim]->GetPlayLength();

		TArray<AActor*> Ignored({ EnemyActor });
		FHitResult GroundHit;
		FHitResult ObstacleHit;
		Destination = MyLocation + (DirToPredictedLocation * AnimLength * 600.f);

		UKismetSystemLibrary::LineTraceSingleByProfile(this, Destination, FVector(Destination.X, Destination.Y, Destination.Z - 1000.f), "Landscape", false, Ignored, EDrawDebugTrace::ForDuration, GroundHit, true);
		if (GroundHit.bBlockingHit)
		{
			Destination = GroundHit.Location;
		}

		UKismetSystemLibrary::LineTraceSingleByProfile(this, EnemyLocation, Destination, "Landscape", false, Ignored, EDrawDebugTrace::ForDuration, GroundHit, true);
		if (ObstacleHit.bBlockingHit)
		{
			Destination = ObstacleHit.Location;
		}

		AICharacter->M_PlayAttackInMoveMontageByIndex(RandomAnim);
	}
	else
	{
		RandomAnim = UKismetMathLibrary::RandomInteger(AICharacter->AttackAnims.Num());
		AnimLength = AICharacter->AttackAnims[RandomAnim]->GetPlayLength();
		AICharacter->M_PlayAttackMontageByIndex(RandomAnim);
	}

	UKismetSystemLibrary::DrawDebugSphere(this, Destination, 50.f, 12, FColor::Red, 5.f, 1.f);

	FAIMoveRequest MoveRequest(Destination);
	MoveRequest.SetAcceptanceRadius(50.f);
	MoveRequest.SetProjectGoalLocation(true);
	MoveRequest.SetUsePathfinding(false);

	FPathFollowingRequestResult FollowingRequest = AIController->MoveTo(MoveRequest);
	//FollowingRequest.MoveId
	//AICharacter->
	//Move
	//AIController->MoveToLocation(PredictedLocation, 50.f, false, false);
	//UAITask_MoveTo* MTask = UAITask::NewAITask<UAITask_MoveTo>(*OwnerComp.GetAIOwner(), *this, TEXT("Behavior"));
	//MTask->SetUp(AIController, MoveRequest);
	//MTask->ConditionalPerformMove();

	FTimerDelegate TD;
	FTimerHandle TH;
	TD.BindLambda([&]() { FinishExecute(true); });

	GetWorld()->GetTimerManager().SetTimer(TH, TD, AnimLength, false);

	return EBTNodeResult::InProgress;
	//return ParentResult;

}
