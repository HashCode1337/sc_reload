// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_AIPerceptionComponent.h"
#include "../Core/SC_StaticLibrary.h"
#include "Perception/AISense.h"
#include "Perception/AISense_Hearing.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AISense_Damage.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Perception/AISenseConfig.h"
#include "SC_AIStaticHelpers.h"
#include "SC_AICharacterBase.h"
#include "Kismet/KismetMathLibrary.h"

USC_AIPerceptionComponent::USC_AIPerceptionComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	this->bAutoActivate = false;
	this->bAutoRegister = false;
}

void USC_AIPerceptionComponent::OnPerception(AActor* Actor, FAIStimulus Stimulus)
{
	TSubclassOf<UAISense> SenseClass = UAIPerceptionSystem::GetSenseClassForStimulus(this, Stimulus);
	
	// extended to make ifElse construction shorter and clearer
	TFunction<void ()> StimuliSight_FindAndRemoveChecked = [&]() {StimuliSight.FindAndRemoveChecked(Actor); AddLostSightEnemy(Actor, Stimulus); };

	if (Stimulus.WasSuccessfullySensed())
	{
		if		(SenseClass == UAISense_Sight::StaticClass())		StimuliSight.FindOrAdd(Actor, Stimulus);
		else if (SenseClass == UAISense_Hearing::StaticClass())
		{
			if (StimuliHear.Contains(Actor))
			{
				StimuliHear[Actor] = Stimulus;
				UKismetSystemLibrary::PrintString(this, "UPDATED", true, false, FColor::Red, 0.7f);

			}
			else
			{
				StimuliHear.FindOrAdd(Actor, Stimulus);
				UKismetSystemLibrary::PrintString(this, "ADDED NEW", true, false, FColor::Red, 0.7f);
			}
			
		}
		else if (SenseClass == UAISense_Damage::StaticClass())		StimuliDamaged.FindOrAdd(Actor, Stimulus);
	}
	else
	{
		if		(SenseClass == UAISense_Sight::StaticClass())			StimuliSight_FindAndRemoveChecked();
		else if (SenseClass == UAISense_Hearing::StaticClass())		StimuliHear.FindAndRemoveChecked(Actor);
		else if (SenseClass == UAISense_Damage::StaticClass())		StimuliDamaged.FindAndRemoveChecked(Actor);
	}

	RefreshLostSightEnemiesMap();

	EAIState NewState = UpdateAiState();
	OnAIStateChanged.Broadcast(NewState, PreferredAIState);
	PreferredAIState = NewState;
}

void USC_AIPerceptionComponent::AddLostSightEnemy(AActor* Actor, FAIStimulus Stimulus)
{
	float MaxAge = 0.f;

	for (UAISenseConfig* CursenseConfig : SensesConfig)
	{
		bool ClassMatch = 
			CursenseConfig->GetSenseImplementation() == UAISense_Sight::StaticClass();

		if (ClassMatch)					MaxAge = CursenseConfig->GetMaxAge();
		else							continue;
	}

	FStimulusSightLost SightLostStimulus(GetWorld()->UnpausedTimeSeconds, MaxAge);
	StimuliSightLost.FindOrAdd(Actor, SightLostStimulus);
}

void USC_AIPerceptionComponent::RefreshLostSightEnemiesMap()
{
	TArray<AActor*> CachedToRemove;

	for (const TPair<AActor*, FStimulusSightLost> CurPair : StimuliSightLost)
	{
		
		const AActor* Actor				= CurPair.Key;
		FStimulusSightLost Stimul		= CurPair.Value;
		bool bIsExpired					= Stimul.IsAgeExpired(this);
		bool bNotActualAnymore			= StimuliSight.Contains(Actor);


		if (bIsExpired || bNotActualAnymore)
			CachedToRemove.Add(CurPair.Key);

	}

	for (const AActor* RemoveMe : CachedToRemove)
		StimuliSightLost.FindAndRemoveChecked(RemoveMe);
	
}

EAIState USC_AIPerceptionComponent::UpdateAiState()
{
	// init
	bool bCritHP						= GetHealthIsCriticalLow();
	bool bHasSightEnemies				= false;
	TArray<AActor*> Enemies;
	AActor* CachedReachableEnemy		= nullptr;
	FVector DamagedFromLocation			= FVector::ZeroVector;
	FVector HearedLocation				= FVector::ZeroVector;
	FVector SeachAtLocation				= FVector::ZeroVector;

	if (bCritHP) return EAIState::Runaway;

	// first, try find reachable aggressor
	if (StimuliSight.Num() > 0)
	{
		// cache aggressor
		bHasSightEnemies = 
			USC_AIStaticHelpers::GetAliveSensedEnemiesByClass(this, UAISense_Sight::StaticClass(), Enemies);

		CachedReachableEnemy =
			USC_AIStaticHelpers::GetNearestReachableActor(this->AIOwner, Enemies);
		
	}

	// if aggressor found, set him and exit
	if (bHasSightEnemies && CachedReachableEnemy)
	{
		PreferredAggressor = CachedReachableEnemy;
		return EAIState::Chase;
	}
	else if (bHasSightEnemies && !CachedReachableEnemy)
	{
		PreferredAggressor = nullptr;
		return EAIState::Runaway;
	}

	if (StimuliDamaged.Num() > 0)
	{
		TArray<FAIStimulus> DamagedStimuli;
		StimuliDamaged.GenerateValueArray(DamagedStimuli);
		int32 RandomIndex = UKismetMathLibrary::RandomInteger(StimuliDamaged.Num());
		PreferredSearchLocation = DamagedStimuli[RandomIndex].StimulusLocation;

		return EAIState::Search;
	}
	else if (StimuliHear.Num() > 0)
	{
		TArray<FAIStimulus> HearedStimuli;
		StimuliHear.GenerateValueArray(HearedStimuli);
		int32 RandomIndex = UKismetMathLibrary::RandomInteger(StimuliHear.Num());
		PreferredSearchLocation = HearedStimuli[RandomIndex].StimulusLocation;

		UKismetSystemLibrary::PrintString(this, PreferredSearchLocation.ToString(), true, false, FColor::Red, 0.7f);

		return EAIState::Search;
	}
	else if (StimuliSightLost.Num() > 0)
	{
		TArray<FStimulusSightLost> SightLostStimuli;
		StimuliSightLost.GenerateValueArray(SightLostStimuli);
		int32 RandomIndex = UKismetMathLibrary::RandomInteger(StimuliSightLost.Num());
		PreferredSearchLocation = SightLostStimuli[RandomIndex].StimulusSightLost.StimulusLocation;

		return EAIState::Danger;
	}

	return EAIState::Unknown;
}

bool USC_AIPerceptionComponent::GetHealthIsCriticalLow()
{
	return GetAICharacter()->GetHealth() <= RunawayHealthThreshold;
}

void USC_AIPerceptionComponent::Routine()
{
	EAIState NewState = UpdateAiState();
	OnAIStateChanged.Broadcast(NewState, PreferredAIState);
	PreferredAIState = NewState;
}

ASC_AICharacterBase* USC_AIPerceptionComponent::GetAICharacter()
{
	return CastChecked<ASC_AICharacterBase>(AIOwner->GetPawn());
}

void USC_AIPerceptionComponent::OnRegister()
{
	Super::OnRegister();
	OnTargetPerceptionUpdated.AddDynamic(this, &USC_AIPerceptionComponent::OnPerception);

	FTimerHandle TH;
	GetWorld()->GetTimerManager().SetTimer(TH, this, &USC_AIPerceptionComponent::Routine, RoutineUpdateFrequency, true);
}

void USC_AIPerceptionComponent::OnUnregister()
{
	Super::OnUnregister();
	OnTargetPerceptionUpdated.RemoveDynamic(this, &USC_AIPerceptionComponent::OnPerception);
}