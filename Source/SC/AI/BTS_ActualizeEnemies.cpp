// Fill out your copyright notice in the Description page of Project Settings.


#include "BTS_ActualizeEnemies.h"
#include "SC_AIStaticHelpers.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "../Core/SC_StaticLibrary.h"

UBTS_ActualizeEnemies::UBTS_ActualizeEnemies(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	AggressorKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTS_ActualizeEnemies, AggressorKey), AActor::StaticClass());
	AggressorLocationKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTS_ActualizeEnemies, AggressorLocationKey));
	SearchLocationKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTS_ActualizeEnemies, SearchLocationKey));
	HideFromLocationKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTS_ActualizeEnemies, HideFromLocationKey));
	
	this->bCreateNodeInstance = true;
	this->bNotifyTick = true;
	this->bCallTickOnSearchStart = true;
}

void UBTS_ActualizeEnemies::UpdateEnemies()
{
	TArray<AActor*> KnownEnemies;

	// scan enemies
	bool HasKnownEnemies = USC_AIStaticHelpers::GetAliveSensedEnemies
	(
		AIController->GetPerceptionComponent(),
		KnownEnemies
	);

	// if empty, clear all BB keys
	if (!HasKnownEnemies)
	{
		ClearBBKeys();
		return;
	}

	// extract actual, reachable, expired enemies
	USC_AIStaticHelpers::GetSortedKnownEnemies(AIController, KnownEnemies, ExpiringEnemies, ActualEnemies);

	if (ActualEnemies.Num() > 0)
	{
		WorkActualEnemies();
	}
	else if (ExpiringEnemies.Num() > 0)
	{
		WorkExpiringEnemies();
	}

	USC_StaticLibrary::PrintMsg(0, FColor::Red, FString::SanitizeFloat(ActualEnemies.Num()));
	USC_StaticLibrary::PrintMsg(1, FColor::Blue, FString::SanitizeFloat(ExpiringEnemies.Num()));
}

void UBTS_ActualizeEnemies::WorkActualEnemies()
{
	bool HasReachable = false;
	USC_StaticLibrary::SortByDistance(AICharacter, ActualEnemies);
	AActor* NearestActor = USC_AIStaticHelpers::GetNearestReachableActor(AIController, ActualEnemies);

	if (HasReachable)
	{
		FAIStimulus SenseData = USC_AIStaticHelpers::GetSightSenseAboutActor(AIController, NearestActor);

		FVector OutLocation;
		USC_AIStaticHelpers::GetPredictedChasingLoc(AICharacter, NearestActor, 1000.f, OutLocation);
		
		AIController->GetBlackboardComponent()->ClearValue(SearchLocationKey.SelectedKeyName);
		AIController->GetBlackboardComponent()->ClearValue(HideFromLocationKey.SelectedKeyName);

		AIController->GetBlackboardComponent()->SetValueAsObject(AggressorKey.SelectedKeyName, NearestActor);
		AIController->GetBlackboardComponent()->SetValueAsVector(AggressorLocationKey.SelectedKeyName, OutLocation);
	}
	else
	{
		NearestActor = ActualEnemies[0];

		FAIStimulus SenseData = USC_AIStaticHelpers::GetSightSenseAboutActor(AIController, ActualEnemies[0]);

		AIController->GetBlackboardComponent()->ClearValue(AggressorKey.SelectedKeyName);
		AIController->GetBlackboardComponent()->ClearValue(AggressorLocationKey.SelectedKeyName);
		AIController->GetBlackboardComponent()->ClearValue(SearchLocationKey.SelectedKeyName);
		
		AIController->GetBlackboardComponent()->SetValueAsVector(HideFromLocationKey.SelectedKeyName, SenseData.StimulusLocation);
	}

}

void UBTS_ActualizeEnemies::WorkExpiringEnemies()
{
	USC_StaticLibrary::SortByDistance(AICharacter, ExpiringEnemies);
	AActor* NearestActor = ExpiringEnemies[0];

	FAIStimulus SenseData = USC_AIStaticHelpers::GetSightSenseAboutActor(AIController, NearestActor);

	AIController->GetBlackboardComponent()->ClearValue(AggressorKey.SelectedKeyName);
	AIController->GetBlackboardComponent()->ClearValue(AggressorLocationKey.SelectedKeyName);
	AIController->GetBlackboardComponent()->ClearValue(HideFromLocationKey.SelectedKeyName);

	AIController->GetBlackboardComponent()->SetValueAsVector(SearchLocationKey.SelectedKeyName, SenseData.StimulusLocation);
}

void UBTS_ActualizeEnemies::ClearBBKeys()
{
	AIController->GetBlackboardComponent()->ClearValue(AggressorKey.SelectedKeyName);
	AIController->GetBlackboardComponent()->ClearValue(AggressorLocationKey.SelectedKeyName);
	AIController->GetBlackboardComponent()->ClearValue(SearchLocationKey.SelectedKeyName);
	AIController->GetBlackboardComponent()->ClearValue(HideFromLocationKey.SelectedKeyName);
}

void UBTS_ActualizeEnemies::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	UpdateEnemies();
}

void UBTS_ActualizeEnemies::OnSearchStart(FBehaviorTreeSearchData& SearchData)
{
	UpdateEnemies();
}

void UBTS_ActualizeEnemies::OnInstanceCreated(UBehaviorTreeComponent& OwnerComp)
{
	AIController = Cast<ASC_AIController>(OwnerComp.GetAIOwner());
	AICharacter = Cast<ASC_AICharacterBase>(AIController->GetPawn());
}