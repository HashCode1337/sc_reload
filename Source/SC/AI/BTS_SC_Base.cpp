// Fill out your copyright notice in the Description page of Project Settings.


#include "BTS_SC_Base.h"
#include "BlueprintNodeHelpers.h"

UBTS_SC_Base::UBTS_SC_Base()
{
	bNotifyOnSearch = true;
	bNotifyTick = true;
	bCreateNodeInstance = true;
	bNotifyBecomeRelevant = true;
	bNotifyCeaseRelevant = true;
}

void UBTS_SC_Base::OnSearchStart(FBehaviorTreeSearchData& SearchData)
{
	ReceiveSearchStartAI(AIController, AICharacter);
}

void UBTS_SC_Base::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	ReceiveTickAI(AIController, AICharacter, DeltaSeconds);
}

void UBTS_SC_Base::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AIPerception->OnAIStateChanged.AddDynamic(this, &UBTS_SC_Base::OnAIStateChanged);
	ReceiveActivationAI(AIController, AICharacter);
}

void UBTS_SC_Base::OnCeaseRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AIPerception->OnAIStateChanged.RemoveDynamic(this, &UBTS_SC_Base::OnAIStateChanged);
	BlueprintNodeHelpers::AbortLatentActions(OwnerComp, *this);
	ReceiveDeactivationAI(AIController, AICharacter);
}

void UBTS_SC_Base::SetOwner(AActor* ActorOwner)
{
	AICharacter = CastChecked<ASC_AICharacterBase>(ActorOwner);
	AIController = CastChecked<ASC_AIController>(AICharacter->GetController());
	AIPerception = CastChecked<USC_AIPerceptionComponent>(AIController->GetPerceptionComponent());
}
