// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "AIController.h"
#include "Perception/AIPerceptionTypes.h"
#include "SC_AIStaticHelpers.generated.h"

/**
 * 
 */
UCLASS()
class SC_API USC_AIStaticHelpers : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable)
	static bool PredictActorPosition(APawn* Pawn, AActor* TargetActor, float PredictTime, FAIStimulus& ResultStimulus);

	UFUNCTION(BlueprintCallable)
	static bool GetPredictedChasingLoc(APawn* Pawn, AActor* TargetActor, float PredictUntilDistance, FVector& OutLocation);

	UFUNCTION(BlueprintCallable)
	static bool CanReachLocation(AAIController* Controller, FVector Destination);

	UFUNCTION(BlueprintCallable)
	static bool GetAliveSensedEnemies(UAIPerceptionComponent* PerceptionComp, TArray<AActor*>& OutEnemies);
	
	UFUNCTION(BlueprintCallable)
	static bool GetAliveSensedEnemiesByClass(UAIPerceptionComponent* PerceptionComp, TSubclassOf<UAISense> SenseClass, TArray<AActor*>& OutEnemies);

	UFUNCTION(BlueprintCallable)
	static FAIStimulus GetSightSenseAboutActor(AAIController* AIController, AActor* Actor);
	
	UFUNCTION(BlueprintCallable)
	static AActor* GetNearestReachableActor(AAIController* Controller, const TArray<AActor*>& InActors);
	
	UFUNCTION(BlueprintCallable)
	static AActor* GetNearestActor(AAIController* Controller, TArray<AActor*>& InActors, bool& bSuccess);

	// bool - result has at least one not expired actor by sense age
	UFUNCTION(BlueprintCallable)
	static bool GetKnownEnemies(AAIController* AIController, const TArray<AActor*>& InEnemies, TMap<AActor*, bool>& OutEnemies);
	
	UFUNCTION(BlueprintCallable)
	static bool GetSortedKnownEnemies(AAIController* AIController, const TArray<AActor*>& InEnemies, TArray<AActor*>& Expiring, TArray<AActor*>& Actual);


};
