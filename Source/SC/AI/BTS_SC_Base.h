// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "SC_AICharacterBase.h"
#include "SC_AIPerceptionComponent.h"
#include "SC_AIController.h"
#include "BTS_SC_Base.generated.h"

UCLASS(Abstract, Blueprintable)
class SC_API UBTS_SC_Base : public UBTService
{
	GENERATED_BODY()

	UBTS_SC_Base();

public:

	UPROPERTY(BlueprintReadOnly)	ASC_AICharacterBase* AICharacter;
	UPROPERTY(BlueprintReadOnly)	ASC_AIController* AIController;
	UPROPERTY(BlueprintReadOnly)	USC_AIPerceptionComponent* AIPerception;
	

	UFUNCTION(BlueprintImplementableEvent)
	void ReceiveTickAI(ASC_AIController* OwnerController, ASC_AICharacterBase* ControlledPawn, float DeltaSeconds);

	UFUNCTION(BlueprintImplementableEvent)
	void ReceiveSearchStartAI(ASC_AIController* OwnerController, ASC_AICharacterBase* ControlledPawn);

	UFUNCTION(BlueprintImplementableEvent)
	void ReceiveActivationAI(ASC_AIController* OwnerController, ASC_AICharacterBase* ControlledPawn);

	UFUNCTION(BlueprintImplementableEvent)
	void ReceiveDeactivationAI(ASC_AIController* OwnerController, ASC_AICharacterBase* ControlledPawn);

	UFUNCTION(BlueprintImplementableEvent)
	void OnAIStateChanged(EAIState NewState, EAIState OldState);


protected:
	virtual void OnSearchStart(FBehaviorTreeSearchData& SearchData) override;
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void OnCeaseRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void SetOwner(AActor* ActorOwner) override;
};
