// Fill out your copyright notice in the Description page of Project Settings.


#include "BTT_SC_Base.h"
#include "BlueprintNodeHelpers.h"
#include "BehaviorTree/BehaviorTree.h"
#include "SC_AICharacterBase.h"
#include "SC_AIController.h"
#include "SC_AIPerceptionComponent.h"

UBTT_SC_Base::UBTT_SC_Base()
{
	bCreateNodeInstance = true;
	bNotifyTick = true;
	bNotifyTaskFinished = true;
}

void UBTT_SC_Base::FinishExecute(bool bSuccess)
{
	UBehaviorTreeComponent* OwnerComp = Cast<UBehaviorTreeComponent>(GetOuter());
	EBTNodeResult::Type NodeResult(bSuccess ? EBTNodeResult::Succeeded : EBTNodeResult::Failed);
	
	UnbindDelegates();
	OnAIFinishExecute(AIController, AICharacter, bSuccess);
	FinishLatentTask(*OwnerComp, NodeResult);
}

void UBTT_SC_Base::FinishAbort()
{
	UBehaviorTreeComponent* OwnerComp = Cast<UBehaviorTreeComponent>(GetOuter());
	FinishLatentAbort(*OwnerComp);
}

EForceTaskFinishType UBTT_SC_Base::IsForceFinishRequired(EAIState NewState)
{
	if (ForceFailStates.Contains(NewState)) return		EForceTaskFinishType::Fail;
	if (ForceSuccessStates.Contains(NewState)) return	EForceTaskFinishType::Success;
	return												EForceTaskFinishType::Ignore;
}

void UBTT_SC_Base::OnAIStateChangedNative(EAIState NewState, EAIState OldState)
{
	if (NewState == OldState) return;

	EForceTaskFinishType FinishType = IsForceFinishRequired(NewState);

	switch (FinishType)
	{
	case EForceTaskFinishType::Fail:		FinishExecute(false); break;
	case EForceTaskFinishType::Success:		FinishExecute(true); break;
	}
}

void UBTT_SC_Base::InitializeFromAsset(UBehaviorTree& Asset)
{
	Super::InitializeFromAsset(Asset);
	if (Asset.BlackboardAsset)
	{
		BlueprintNodeHelpers::ResolveBlackboardSelectors(*this, *StaticClass(), *Asset.BlackboardAsset);
	}
}

void UBTT_SC_Base::SetOwner(AActor* ActorOwner)
{
	AIController = CastChecked<ASC_AIController>(ActorOwner);
	AICharacter = CastChecked<ASC_AICharacterBase>(AIController->GetPawn());
	AIPerception = CastChecked<USC_AIPerceptionComponent>(AIController->GetPerceptionComponent());
}

void UBTT_SC_Base::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	if (TickInterval.Tick(DeltaSeconds))
	{
		DeltaSeconds = TickInterval.GetElapsedTimeWithFallback(DeltaSeconds);
		ReceiveTickAI(AIController, AICharacter, DeltaSeconds);

		TickInterval.Reset();
	}
}

EBTNodeResult::Type UBTT_SC_Base::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	BindDelegates();
	ReceiveExecuteAI(AIController, AICharacter);
	return EBTNodeResult::InProgress;
}

EBTNodeResult::Type UBTT_SC_Base::AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ReceiveAbortAI(AIController, AICharacter);
	return EBTNodeResult::Aborted;
}

void UBTT_SC_Base::BindDelegates()
{
	UnbindDelegates();

	AIPerception->OnAIStateChanged.AddDynamic(this, &UBTT_SC_Base::OnAIStateChanged);
	AIPerception->OnAIStateChanged.AddDynamic(this, &UBTT_SC_Base::OnAIStateChangedNative);
}

void UBTT_SC_Base::UnbindDelegates()
{
	if (AIPerception->OnAIStateChanged.IsAlreadyBound(this, &UBTT_SC_Base::OnAIStateChanged))
		AIPerception->OnAIStateChanged.RemoveDynamic(this, &UBTT_SC_Base::OnAIStateChanged);

	if (AIPerception->OnAIStateChanged.IsAlreadyBound(this, &UBTT_SC_Base::OnAIStateChangedNative))
		AIPerception->OnAIStateChanged.RemoveDynamic(this, &UBTT_SC_Base::OnAIStateChangedNative);
}
