// Fill out your copyright notice in the Description page of Project Settings.


#include "BTT_FindAggressor.h"
#include "../Core/SC_StaticLibrary.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "SC_AIStaticHelpers.h"

UBTT_FindAggressor::UBTT_FindAggressor(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	AggressorKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTT_FindAggressor, AggressorKey), AActor::StaticClass());
	AggressorLocationKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTT_FindAggressor, AggressorLocationKey));
	SearchLocationKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTT_FindAggressor, SearchLocationKey));
	HideFromLocationKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTT_FindAggressor, HideFromLocationKey));
}

EBTNodeResult::Type UBTT_FindAggressor::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	// init
	TArray<AActor*> KnownEnemies;
	AAIController* AIController = OwnerComp.GetAIOwner();
	APawn* AIPawn = AIController->GetPawn();

	// clear
	OwnerComp.GetBlackboardComponent()->ClearValue(AggressorKey.SelectedKeyName);
	OwnerComp.GetBlackboardComponent()->ClearValue(AggressorLocationKey.SelectedKeyName);
	OwnerComp.GetBlackboardComponent()->ClearValue(SearchLocationKey.SelectedKeyName);
	OwnerComp.GetBlackboardComponent()->ClearValue(HideFromLocationKey.SelectedKeyName);

	Actual.Empty();
	Expiring.Empty();

	// scan enemies
	bool HasKnownEnemies = USC_AIStaticHelpers::GetAliveSensedEnemies
	(
		AIController->GetPerceptionComponent(),
		KnownEnemies
	);

	// abort if no enemies
	if (!HasKnownEnemies)
		return EBTNodeResult::Failed;


	// extract enemy actual data
	USC_AIStaticHelpers::GetSortedKnownEnemies(AIController, KnownEnemies, Expiring, Actual);

	// actualize bb keys
	if (Actual.Num() > 0)
	{
		USC_StaticLibrary::SortByDistance(AIPawn, Actual);

		bool HasReachable = false;
		AActor* NearestActor = USC_AIStaticHelpers::GetNearestReachableActor(AIController, Actual);

		// if has reachable enemy
		if (HasReachable)
		{
			FAIStimulus SenseData = USC_AIStaticHelpers::GetSightSenseAboutActor(AIController, NearestActor);

			FVector OutLocation;
			USC_AIStaticHelpers::GetPredictedChasingLoc(AIPawn, NearestActor, PredictUntilDistance, OutLocation);

			OwnerComp.GetBlackboardComponent()->SetValueAsObject(AggressorKey.SelectedKeyName, NearestActor);
			OwnerComp.GetBlackboardComponent()->SetValueAsVector(AggressorLocationKey.SelectedKeyName, OutLocation/*SenseData.StimulusLocation*/);
		}
		// if cant reach any enemy, we want hide from nearest one
		else
		{
			NearestActor = Actual[0];

			FAIStimulus SenseData = USC_AIStaticHelpers::GetSightSenseAboutActor(AIController, Actual[0]);
			OwnerComp.GetBlackboardComponent()->SetValueAsVector(HideFromLocationKey.SelectedKeyName, SenseData.StimulusLocation);
		}

		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
	else if (Expiring.Num() > 0)
	{
		USC_StaticLibrary::SortByDistance(AIPawn, Expiring);
		AActor* NearestActor = Expiring[0];

		FAIStimulus SenseData = USC_AIStaticHelpers::GetSightSenseAboutActor(AIController, NearestActor);
		OwnerComp.GetBlackboardComponent()->SetValueAsVector(SearchLocationKey.SelectedKeyName, SenseData.StimulusLocation);

		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
	else
	{
		// it could not be happened, but anyway
		FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
	}

	return EBTNodeResult::InProgress;
}

FString UBTT_FindAggressor::GetStaticDescription() const
{
	return FString::Printf
	(TEXT(
		"Write aggressor to %s\nWrite aggressor location to %s\nWrite search location to %s\nWrite hide from location to %s")
		, *AggressorKey.SelectedKeyName.ToString()
		, *AggressorLocationKey.SelectedKeyName.ToString()
		, *SearchLocationKey.SelectedKeyName.ToString()
		, *HideFromLocationKey.SelectedKeyName.ToString()
	);

}
