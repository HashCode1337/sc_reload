// Fill out your copyright notice in the Description page of Project Settings.


#include "BTD_SC_CompareAIState.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "SC_AICharacterBase.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTD_SC_CompareAIState::UBTD_SC_CompareAIState(const FObjectInitializer& ObjectInitializer)
{
	CheckHimState.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTD_SC_CompareAIState, CheckHimState), AActor::StaticClass());
	bNotifyBecomeRelevant = true;
	NodeName = "AIState Compare Node";
}

bool UBTD_SC_CompareAIState::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	ASC_AICharacterBase* CheckSubject = Cast<ASC_AICharacterBase>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(CheckHimState.SelectedKeyName));
	if (!CheckSubject)
	{
		USC_StaticLibrary::Log(ELogType::Error, TEXT("UBTD_SC_CompareAIState::CalculateRawConditionValue() cant cast pawn!"));
		return false;
	}

	 return CheckSubject->GetAIPerception()->PreferredAIState == ExpectedState;
}

FString UBTD_SC_CompareAIState::GetStaticDescription() const
{
	FString Description = "State is ";

	switch (ExpectedState)
	{
	case EAIState::Unknown:				Description.Append(TEXT("Unknown"));break;
	case EAIState::WaitingTask:			Description.Append(TEXT("WaitingTask")); break;
	case EAIState::Walk:				Description.Append(TEXT("Walk")); break;
	case EAIState::Chase:				Description.Append(TEXT("Chase")); break;
	case EAIState::Search:				Description.Append(TEXT("Search")); break;
	case EAIState::Danger:				Description.Append(TEXT("Danger")); break;
	case EAIState::Runaway:				Description.Append(TEXT("Runaway")); break;
	}

	return Description;
}
