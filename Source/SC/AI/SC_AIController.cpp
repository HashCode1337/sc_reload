// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_AIController.h"
#include "SC_AICharacterBase.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AISense_Hearing.h"
#include "../Core/SC_StaticLibrary.h"
#include "Perception/AISense_Prediction.h"
#include "Perception/AISense_Damage.h"

ASC_AIController::ASC_AIController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	AIPerceptionComp = CreateDefaultSubobject<USC_AIPerceptionComponent>(TEXT("AIPerception"));
}

FGenericTeamId ASC_AIController::GetTeamId()
{
	return GetGenericTeamId();
}

void ASC_AIController::OnEnemySeen_Implementation(AActor* Actor, FAIStimulus Stimulus)
{

}

void ASC_AIController::OnEnemyLost_Implementation(AActor* Actor, FAIStimulus Stimulus)
{

}

void ASC_AIController::OnPerceptionTarget(AActor* Actor, FAIStimulus Stimulus)
{
	TSubclassOf<UAISense> PerceptionClass = UAIPerceptionSystem::GetSenseClassForStimulus(this, Stimulus);

	if (PerceptionClass == UAISense_Sight::StaticClass())
	{
		if (Stimulus.IsValid() && Stimulus.WasSuccessfullySensed())
		{
			OnEnemySeen(Actor, Stimulus);

		}
		else
		{
			OnEnemyLost(Actor, Stimulus);
		}
	}
	else if (PerceptionClass == UAISense_Hearing::StaticClass())
	{

	}
	else if (PerceptionClass == UAISense_Prediction::StaticClass())
	{

	}
	else if (PerceptionClass == UAISense_Damage::StaticClass())
	{

	}
	else
	{
		USC_StaticLibrary::Log(ELogType::Error, "ASC_AIController::OnPerceptionTarget() Sense is not implemented yet!");
	}

	
}

ETeamAttitude::Type ASC_AIController::GetTeamAttitude(AActor* OtherActor)
{
	return GetTeamAttitudeTowards(*OtherActor);
}

void ASC_AIController::SetGenericTeamId(const FGenericTeamId& NewTeamID)
{
	Super::SetGenericTeamId(FGenericTeamId(NewTeamID));
	GenericFactionId = FGenericTeamId(NewTeamID);

	FGenericTeamId newT = AIPerceptionComp->GetTeamIdentifier();
}

FGenericTeamId ASC_AIController::GetGenericTeamId() const
{
	return GenericFactionId;
}

ETeamAttitude::Type ASC_AIController::GetTeamAttitudeTowards(const AActor& Other) const
{
	ASC_AICharacterBase* ControlledPawn = GetPawn<ASC_AICharacterBase>();
	if (ControlledPawn)
	{
		return ControlledPawn->GetTeamAttitudeTowards(Other);
	}
	else
	{
		return ETeamAttitude::Neutral;
	}
}

void ASC_AIController::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		AIPerceptionComp->
			OnTargetPerceptionUpdated.AddDynamic(this, &ASC_AIController::OnPerceptionTarget);
	}

	
}

void ASC_AIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	if (ASC_AICharacterBase* MyPawn = Cast<ASC_AICharacterBase>(InPawn)/*GetPawn<ASC_AICharacterBase>()*/)
	{
		if (MyPawn->OverrideControllerFaction != EFaction::Unknown)
		{
			SetGenericTeamId((uint8)MyPawn->OverrideControllerFaction);
		}
		else
		{
			SetGenericTeamId((uint8)FactionId);
		}
	}
}
