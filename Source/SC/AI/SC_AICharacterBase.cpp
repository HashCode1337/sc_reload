// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_AICharacterBase.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "../SCGameMode.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundCue.h"
#include "BrainComponent.h"
#include "../Core/SC_StaticLibrary.h"
#include "Perception/AISense_Damage.h"

void ASC_AICharacterBase::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	
	ASC_AIController* AIController = Cast<ASC_AIController>(NewController);
	USC_AIPerceptionComponent* Perception = Cast<USC_AIPerceptionComponent>(AIController->AIPerceptionComp);

	Perception->RegisterComponent();
	Perception->Activate();

	UAIPerceptionSystem* AIPerceptionSys = UAIPerceptionSystem::GetCurrent(GetWorld());
	AIPerceptionSys->UnregisterListener(*Perception);
}

void ASC_AICharacterBase::Damaged(FDamagedTenParams DamageDescription)
{
	M_PlaySoundPain();

	/*ASC_AIController* AIController = Cast<ASC_AIController>(GetController());
	UAISense_Damage::ReportDamageEvent
	(
		GetWorld(),
		this,
		DamageDescription.DamageCauser,
		DamageDescription.Damage,
		DamageDescription.DamageCauser->GetActorLocation(),
		DamageDescription.HitLocation
	);*/

	//AIController->SetFocus(DamageDescription.DamageCauser, EAIFocusPriority::Default);
}

void ASC_AICharacterBase::Die(FDamagedTenParams DamageDescription)
{
	M_PlaySoundDeath();
	M_ImpactRagdoll(DamageDescription.HitLocation, DamageDescription.DamageType.GetDefaultObject()->DamageImpulse);

	ASC_AIController* AIController = Cast<ASC_AIController>(GetController());
	if (AIController)
	{
		AIController->StopMovement();
		AIController->GetBrainComponent()->StopLogic("Dead");
	}

}

ASC_AICharacterBase::ASC_AICharacterBase()
{
	AudioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("SoundSource"));
	AudioComp->SetupAttachment(GetMesh());

	if (MovementModes.Num() <= 0)
	{
		MovementModes.Add(EMovementSpeed::Walk, 90.f);
		MovementModes.Add(EMovementSpeed::Run, 600.f);
		MovementModes.Add(EMovementSpeed::Sprint, 600.f);
	}
	

	PrimaryActorTick.bCanEverTick = true;
}

void ASC_AICharacterBase::SetMovementMode(EMovementSpeed NewSpeed)
{
	UCharacterMovementComponent* MovementComp = Cast<UCharacterMovementComponent>(GetMovementComponent());

	if (MovementComp)
	{
		if (MovementModes.Contains(NewSpeed))
		{
			MovementComp->MaxWalkSpeed = *MovementModes.Find(NewSpeed);

		}
	}
}

void ASC_AICharacterBase::M_PlaySoundAggressive_Implementation()
{
	if (AggressiveSound)
	{
		AudioComp->SetSound(AggressiveSound);
		AudioComp->Play();
	}
}

void ASC_AICharacterBase::M_PlaySoundPain_Implementation()
{
	if (PainSound)
	{
		AudioComp->SetSound(PainSound);
		AudioComp->Play();
	}
}

void ASC_AICharacterBase::M_PlaySoundDeath_Implementation()
{
	if (DeathSound)
	{
		AudioComp->SetSound(DeathSound);
		AudioComp->Play();
	}
}

void ASC_AICharacterBase::M_PlayDangerMontageByIndex_Implementation(int32 Index)
{
	int32 MontagesCount = DangerAnims.Num();

	if (MontagesCount > 0)
	{
		GetMesh()->GetAnimInstance()->Montage_Play(DangerAnims[Index]);
	}
}

void ASC_AICharacterBase::M_PlayAttackMontageByIndex_Implementation(int32 Index)
{
	int32 MontagesCount = AttackAnims.Num();

	if (MontagesCount > 0)
	{
		GetMesh()->GetAnimInstance()->Montage_Play(AttackAnims[Index]);
	}
}

void ASC_AICharacterBase::M_PlayAttackInMoveMontageByIndex_Implementation(int32 Index)
{
	int32 MontagesCount = AttackInMoveAnims.Num();

	if (MontagesCount > 0)
	{
		GetMesh()->GetAnimInstance()->Montage_Play(AttackInMoveAnims[Index]);
	}
}

void ASC_AICharacterBase::M_ImpactRagdoll_Implementation(FVector ImpactLocation /*= FVector::ZeroVector*/, float ImpactForce /*= 0.0f*/)
{

	USC_StaticLibrary::RagdollWithImpact(GetMesh(), ImpactLocation, ImpactForce);
}

void ASC_AICharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASC_AICharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

FGenericTeamId ASC_AICharacterBase::GetGenericTeamId() const
{
	if (HasAuthority())
	{
		if (ASC_AIController* AC = GetController<ASC_AIController>())
		{
			return AC->GetTeamId();
		}
	}

	return 255;
}

ETeamAttitude::Type ASC_AICharacterBase::GetTeamAttitudeTowards(const AActor& Other) const
{

	ASCGameMode* GM = Cast<ASCGameMode>(UGameplayStatics::GetGameMode(this));
	if (GM)
	{
		return GM->GetFactionRelations(GetController()->GetPawn(), ((AActor*)&Other));
	}

	return ETeamAttitude::Neutral;
}

