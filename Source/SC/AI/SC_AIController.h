// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "SC_AIPerceptionComponent.h"
#include "../Core/SC_CommonDataModels.h"
#include "SC_AIController.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class SC_API ASC_AIController : public AAIController
{
	GENERATED_BODY()

public:
	ASC_AIController(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	EFaction FactionId = EFaction::Unknown;

	UPROPERTY()
	FGenericTeamId GenericFactionId;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USC_AIPerceptionComponent* AIPerceptionComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UBehaviorTree* BehaviorTree;

	UPROPERTY(BlueprintReadWrite)
	TArray<AActor*> KnownEnemies;

public:

	UFUNCTION(BlueprintPure)
	FGenericTeamId GetTeamId();

	UFUNCTION(BlueprintNativeEvent)
	void OnEnemySeen(AActor* Actor, FAIStimulus Stimulus);

	UFUNCTION(BlueprintNativeEvent)
	void OnEnemyLost(AActor* Actor, FAIStimulus Stimulus);

	UFUNCTION()
	void OnPerceptionTarget(AActor* Actor, FAIStimulus Stimulus);

	UFUNCTION(BlueprintPure)
	ETeamAttitude::Type GetTeamAttitude(AActor* OtherActor);


public:
	void SetGenericTeamId(const FGenericTeamId& NewTeamID) override;
	FGenericTeamId GetGenericTeamId() const override;

private:
	ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;

protected:
	void BeginPlay() override;
	void OnPossess(APawn* InPawn) override;

};
