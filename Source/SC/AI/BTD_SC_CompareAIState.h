// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "../Core/SC_CommonDataModels.h"
#include "BTD_SC_CompareAIState.generated.h"

/**
 * 
 */
UCLASS()
class SC_API UBTD_SC_CompareAIState : public UBTDecorator
{
	GENERATED_BODY()

	UBTD_SC_CompareAIState(const FObjectInitializer& ObjectInitializer);
	
public:

	/** blackboard key selector */
	UPROPERTY(EditAnywhere)
	FBlackboardKeySelector CheckHimState;

	UPROPERTY(EditAnywhere)
	EAIState ExpectedState;

protected:

	bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

private:

	void InitializeFromAsset(UBehaviorTree& Asset) override
	{
		Super::InitializeFromAsset(Asset);

		UBlackboardData* BBAsset = GetBlackboardAsset();
		if (BBAsset)
		{
			CheckHimState.ResolveSelectedKey(*BBAsset);
		}
		else
		{
			UE_LOG(LogBehaviorTree, Warning, TEXT("Can't initialize %s due to missing blackboard data."), *GetName());
			CheckHimState.InvalidateResolvedKey();
		}
	};

	FString GetStaticDescription() const override;

};
