// Fill out your copyright notice in the Description page of Project Settings.


#include "BTD_BoolKeyCheck.h"
#include "../Core/SC_StaticLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTD_BoolKeyCheck::UBTD_BoolKeyCheck(const FObjectInitializer& ObjectInitializer)
{
	BlackboardKey.AddBoolFilter(this, GET_MEMBER_NAME_CHECKED(UBTD_BoolKeyCheck, BlackboardKey));
	bNotifyBecomeRelevant = true;
}

bool UBTD_BoolKeyCheck::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	bool SelKeyValue = OwnerComp.GetBlackboardComponent()->GetValueAsBool(BlackboardKey.SelectedKeyName);
	return SelKeyValue == bCondition;
}

void UBTD_BoolKeyCheck::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::OnBecomeRelevant(OwnerComp, NodeMemory);
}

void UBTD_BoolKeyCheck::InitializeFromAsset(UBehaviorTree& Asset)
{
	Super::InitializeFromAsset(Asset);

	UBlackboardData* BBAsset = GetBlackboardAsset();
	if (BBAsset)
	{
		BlackboardKey.ResolveSelectedKey(*BBAsset);
	}
	else
	{
		UE_LOG(LogBehaviorTree, Warning, TEXT("Can't initialize %s due to missing blackboard data."), *GetName());
		BlackboardKey.InvalidateResolvedKey();
	}
}

FString UBTD_BoolKeyCheck::GetStaticDescription() const
{
	FString CondCompare = bCondition ? "true" : "false";

	return FString::Printf(TEXT("%s is %s")
		, *BlackboardKey.SelectedKeyName.ToString()
		, *CondCompare
	);
}
