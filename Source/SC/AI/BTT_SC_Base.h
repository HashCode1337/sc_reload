// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_SC_Base.generated.h"

class ASC_AICharacterBase;
class ASC_AIController;
class USC_AIPerceptionComponent;

UCLASS(Abstract, Blueprintable)
class SC_API UBTT_SC_Base : public UBTTaskNode
{
	GENERATED_BODY()

protected:

	UBTT_SC_Base();

public:

	UPROPERTY(BlueprintReadOnly)	ASC_AICharacterBase* AICharacter;
	UPROPERTY(BlueprintReadOnly)	ASC_AIController* AIController;
	UPROPERTY(BlueprintReadOnly)	USC_AIPerceptionComponent* AIPerception;
	UPROPERTY(EditAnywhere)			TArray<EAIState> ForceFailStates;
	UPROPERTY(EditAnywhere)			TArray<EAIState> ForceSuccessStates;
	UPROPERTY(EditAnywhere)			FIntervalCountdown TickInterval;

	UFUNCTION(BlueprintImplementableEvent)
	void ReceiveExecuteAI(AAIController* OwnerController, ASC_AICharacterBase* ControlledPawn);

	UFUNCTION(BlueprintImplementableEvent)
	void ReceiveAbortAI(AAIController* OwnerController, ASC_AICharacterBase* ControlledPawn);

	UFUNCTION(BlueprintImplementableEvent)
	void ReceiveTickAI(AAIController* OwnerController, ASC_AICharacterBase* ControlledPawn, float DeltaSeconds);

	UFUNCTION(BlueprintImplementableEvent)
	void OnAIFinishExecute(AAIController* OwnerController, ASC_AICharacterBase* ControlledPawn, bool bSuccess);

	UFUNCTION(BlueprintImplementableEvent)
	void OnAIStateChanged(EAIState NewState, EAIState OldState);

	UFUNCTION(BlueprintCallable)
	void FinishExecute(bool bSuccess);

	UFUNCTION(BlueprintCallable)
	void FinishAbort();
	
protected:

	UFUNCTION(BlueprintPure)
	virtual EForceTaskFinishType IsForceFinishRequired(EAIState NewState);

	UFUNCTION()
	virtual void OnAIStateChangedNative(EAIState NewState, EAIState OldState);

protected:

	virtual void InitializeFromAsset(UBehaviorTree& Asset) override;
	virtual void SetOwner(AActor* ActorOwner) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual EBTNodeResult::Type AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
private:

	void BindDelegates();
	void UnbindDelegates();

};
