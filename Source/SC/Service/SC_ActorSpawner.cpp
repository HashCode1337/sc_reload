// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_ActorSpawner.h"
#include "SC/Core/SC_SpawnPoint.h"
#include "Kismet/GameplayStatics.h"
#include "SC/Core/SC_StaticLibrary.h"

class ASCCharacter;

// Sets default values
ASC_ActorSpawner::ASC_ActorSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bNetLoadOnClient = false;
	bReplicates = false;
	SetCanBeDamaged(false);
}

void ASC_ActorSpawner::CreateSpawnPoint(int Count, TArray<ASC_SpawnPoint*>& OutArray)
{
	FVector SpawnLoc;
	FRotator SpawnRot;

	for (int i = 0; i < Count; i++)
	{
		ASC_SpawnPoint* NewActor = GetWorld()->SpawnActor<ASC_SpawnPoint>(SpawnLoc, SpawnRot);
		OutArray.Add(NewActor);
	}
}

ASpectatorPawn* ASC_ActorSpawner::CreatePreloggedSpectator()
{
	FVector SpawnLoc;
	FRotator SpawnRot;
	FActorSpawnParameters Params;
	Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	Params.bAllowDuringConstructionScript = true;

	ASpectatorPawn* Spectator = GetWorld()->SpawnActor<ASpectatorPawn>(PreloggedSpectator, SpawnLoc, SpawnRot);

	return Spectator;
}

ASCCharacter* ASC_ActorSpawner::CreateDefaultPlayerCharacter(const FVector& SpawnLoc, const FRotator& SpawnRot)
{
	FActorSpawnParameters Params;
	Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	Params.bAllowDuringConstructionScript = true;

	ASCCharacter* PlayerPawn = GetWorld()->SpawnActor<ASCCharacter>(DefaultPlayerCharacter, SpawnLoc, SpawnRot, Params);

	return PlayerPawn;
}

// Called when the game starts or when spawned
void ASC_ActorSpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASC_ActorSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

