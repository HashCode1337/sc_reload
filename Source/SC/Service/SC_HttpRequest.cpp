// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_HttpRequest.h"
#include "JsonObjectConverter.h"

void USC_HttpRequest::EnableCallback(bool Enable)
{
	BindLambda = Enable;
}

void USC_HttpRequest::MakeGet(FString Url)
{
	FHttpModule& httpModule = FHttpModule::Get();
	
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> pRequest = httpModule.CreateRequest();
	pRequest->SetVerb(TEXT("GET"));
	pRequest->SetURL(Url);
	pRequest->SetTimeout(1);

	if (BindLambda)
	{
		pRequest->OnProcessRequestComplete().BindLambda(
			[&](FHttpRequestPtr pReq, FHttpResponsePtr pRes, bool connectedSuccessfully)
			{
				
				//if (pReq->GetElapsedTime() > pReq->GetTimeout().GetValue()) return;
				
				if (connectedSuccessfully) {
					auto resp = pReq->GetResponse();
					auto backendAnswer = resp->GetContentAsString();
					backendAnswer = resp->GetContentAsString();

					FBackendAnswer NewAnswer;
					bool Converted = FJsonObjectConverter::JsonObjectStringToUStruct(backendAnswer, &NewAnswer);

					OnAnswered(NewAnswer);
					Answer.Broadcast(NewAnswer);
				}
				else
				{
					FBackendAnswer FailedAnswer;
					FailedAnswer.Success = false;
					FailedAnswer.Error = "Timeout";

					OnAnswered(FailedAnswer);
					Answer.Broadcast(FailedAnswer);

					switch (pReq->GetStatus()) {
					case EHttpRequestStatus::Failed_ConnectionError:
						UE_LOG(LogTemp, Error, TEXT("Connection failed."));
					default:
						UE_LOG(LogTemp, Error, TEXT("Request failed."));
					}
				}

			}
		);
	}

	pRequest->ProcessRequest();
}

void USC_HttpRequest::MakePost(FString Url, FString Content)
{
	FHttpModule& httpModule = FHttpModule::Get();

	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> pRequest = httpModule.CreateRequest();
	pRequest->SetHeader("Content-Type", "application/json; charset=utf-8"); //x-www-form-urlencoded
	pRequest->SetVerb(TEXT("POST"));
	pRequest->SetURL(Url);
	pRequest->AppendToHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	pRequest->SetContentAsString(*Content);
	pRequest->SetTimeout(1);

	if (BindLambda)
	{
		pRequest->OnProcessRequestComplete().BindLambda(
			[&](FHttpRequestPtr pRequest, FHttpResponsePtr pResponse, bool connectedSuccessfully)
			{
				if (connectedSuccessfully) {
					auto resp = pRequest->GetResponse();
					auto backendAnswer = resp->GetContentAsString();
					backendAnswer = resp->GetContentAsString();

					FBackendAnswer NewAnswer;
					bool Converted = FJsonObjectConverter::JsonObjectStringToUStruct(backendAnswer, &NewAnswer);

					OnAnswered(NewAnswer);
					Answer.Broadcast(NewAnswer);
				}
				else
				{
					FBackendAnswer FailedAnswer;
					FailedAnswer.Success = false;
					FailedAnswer.Error = "Timeout";

					OnAnswered(FailedAnswer);
					Answer.Broadcast(FailedAnswer);

					switch (pRequest->GetStatus()) {
					case EHttpRequestStatus::Failed_ConnectionError:
						UE_LOG(LogTemp, Error, TEXT("Connection failed."));
					default:
						UE_LOG(LogTemp, Error, TEXT("Request failed."));
					}
				}

			}
		);
	}

	pRequest->ProcessRequest();
}

void USC_HttpRequest::OnAnswered_Implementation(FBackendAnswer Answered)
{
	
}
