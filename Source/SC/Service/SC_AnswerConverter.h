// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SC/Core/SC_CommonDataModels.h"
#include "SC_AnswerConverter.generated.h"
 
/**
 * 
 */
UCLASS()
class SC_API USC_AnswerConverter : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "SC | Convert"))
	static bool ConvertToServerSettings(const FBackendAnswer& Answer, FCommonGameInfo& Out);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "SC | Convert"))
	static bool ConvertToVectorRotation(const FBackendAnswer& Answer, FVectorRotation& Out);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "SC | Convert"))
	static bool ConvertToFactionData(const FBackendAnswer& Answer, TArray<FFactionData>& Out);
	
	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "SC | Convert"))
	static bool ConvertFromFactionData(const FFactionData& Input, FString& Out);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "SC | Convert"))
	static bool ConvertFromFactionsData(const FFactionsDataArray& Input, FString& Out);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "SC | Convert"))
	static bool ConvertJsonStringToVector(const FString& Input, FVector& Out);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "SC | Convert"))
	static bool ConvertJsonStringToRotator(const FString& Input, FRotator& Out);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "SC | Convert"))
	static bool ConvertToPlayerAccountData(const FBackendAnswer& Answer, FPlayerAccountData& Out);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (Keywords = "SC | Convert"))
	static bool ConvertToPlayerCharacterData(const FBackendAnswer& Answer, FPlayerCharacterData& Out);
};
