// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/SpectatorPawn.h"
#include "SC/Core/SC_CommonDataModels.h"
#include "SC/SCCharacter.h"
#include "SC_ActorSpawner.generated.h"

class ASC_SpawnPoint;

UCLASS()
class SC_API ASC_ActorSpawner : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ASpectatorPawn> PreloggedSpectator;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ASCCharacter> DefaultPlayerCharacter;

	
public:	
	ASC_ActorSpawner();

public:
	UFUNCTION(BlueprintCallable)
	void CreateSpawnPoint(int Count, TArray<ASC_SpawnPoint*>& OutArray);

	UFUNCTION(BlueprintCallable)
	ASpectatorPawn* CreatePreloggedSpectator();

	UFUNCTION(BlueprintCallable)
	ASCCharacter* CreateDefaultPlayerCharacter(const FVector& SpawnLoc, const FRotator& SpawnRot);

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	

};
