// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_AnswerConverter.h"
#include "JsonObjectConverter.h"
#include "SC/Core/SC_StaticLibrary.h"
#include "Serialization/JsonReader.h"
#include "Serialization/JsonTypes.h"

bool USC_AnswerConverter::ConvertToServerSettings(const FBackendAnswer& Answer, FCommonGameInfo& Out)
{
	FCommonGameInfo ToConvert;
	FJsonObjectConverter::JsonObjectStringToUStruct(Answer.Answer, &ToConvert);

	if (Answer.Success)
	{
		Out = ToConvert;
		return true;
	}
	else
	{
		USC_StaticLibrary::Log(ELogType::Error, "ERROR: USC_AnswerConverter::ConvertToServerSettings(), Can't convert structure!");
	}


	return false;
}

bool USC_AnswerConverter::ConvertToVectorRotation(const FBackendAnswer& Answer, FVectorRotation& Out)
{
	FVectorRotation ToConvert;
	FJsonObjectConverter::JsonObjectStringToUStruct(Answer.Answer, &ToConvert);

	if (Answer.Success)
	{
		Out = ToConvert;
		return true;
	}
	else
	{
		USC_StaticLibrary::Log(ELogType::Error, "ERROR: USC_AnswerConverter::ConvertToVectorRotation(), Can't convert structure!");
	}


	return false;
}

bool USC_AnswerConverter::ConvertToFactionData(const FBackendAnswer& Answer, TArray<FFactionData>& Out)
{
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
	TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create(Answer.Answer);

	if (FJsonSerializer::Deserialize(JsonReader, JsonObject))
	{
		TArray<TSharedPtr<FJsonValue>> JsonArr = JsonObject->GetArrayField(TEXT("factions"));

		for (TSharedPtr<FJsonValue> factionElement : JsonArr)
		{
			FJsonObject* Faction = factionElement->AsObject().Get();

			int fId = Faction->GetIntegerField("id");
			FString fName = Faction->GetStringField("name");
			int fBank = Faction->GetIntegerField("bank");
			int fMaxMember = Faction->GetIntegerField("max_member");

			FVectorRotation fRespawnC;
			FVectorRotation fGuestC;

			FString fRespawn = Faction->GetStringField("respawn");
			FString fGuest = Faction->GetStringField("guest");

			FJsonObjectConverter::JsonObjectStringToUStruct(fRespawn, &fRespawnC);
			FJsonObjectConverter::JsonObjectStringToUStruct(fGuest, &fGuestC);

			FFactionData CurFaction;

			CurFaction.id = static_cast<EFaction>(fId);
			CurFaction.name = fName;
			CurFaction.bank = fBank;
			CurFaction.max_member = fMaxMember;
			CurFaction.respawn = fRespawnC;
			CurFaction.guest = fGuestC;

			Out.Add(CurFaction);
		}

		return true;
	}

	return false;
}

bool USC_AnswerConverter::ConvertFromFactionsData(const FFactionsDataArray& Input, FString& Out)
{
	TSharedPtr<FJsonObject> RootObject = MakeShareable(new FJsonObject);
	TArray<TSharedPtr<FJsonValue>> JsonContent;

	FString OutputString;
	TSharedRef<TJsonWriter<>> JsonWriter = TJsonWriterFactory<>::Create(&OutputString);

	for (const FFactionData & CurFaction : Input.Factions)
	{
		TSharedPtr<FJsonObject> CurJsonVal = MakeShareable(new FJsonObject);
		CurJsonVal->SetNumberField("id", static_cast<int>(CurFaction.id));
		CurJsonVal->SetStringField("name", CurFaction.name);
		CurJsonVal->SetNumberField("bank", CurFaction.bank);
		CurJsonVal->SetNumberField("max_member", CurFaction.max_member);
		
		TSharedPtr<FJsonObject> RespawnJson = FJsonObjectConverter::UStructToJsonObject(CurFaction.respawn);
		CurJsonVal->SetObjectField("respawn", RespawnJson);

		TSharedPtr<FJsonObject> GuestJson = FJsonObjectConverter::UStructToJsonObject(CurFaction.respawn);
		CurJsonVal->SetObjectField("guest", GuestJson);

		JsonContent.Add(MakeShareable(new FJsonValueObject(CurJsonVal)));
	}
	
	RootObject->SetArrayField("factions", JsonContent);
	FJsonSerializer::Serialize(RootObject.ToSharedRef(), JsonWriter);

	Out = OutputString;

	return JsonContent.Num() > 0;
}

bool USC_AnswerConverter::ConvertFromFactionData(const FFactionData& Input, FString& Out)
{
	FString ResultString;

	if (FJsonObjectConverter::UStructToJsonObjectString(Input, ResultString, 0, 0, 0, nullptr, false))
	{
		Out = ResultString;
		return true;
	}

	return false;

}

//bool USC_AnswerConverter::ConvertToPlayerData(const FBackendAnswer& Answer, FPlayerData& Out)
//{
//	FPlayerData ToConvert;
//	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Answer.Answer);
//	TSharedPtr<FJsonObject> RootObject = MakeShareable(new FJsonObject);
//
//	FJsonSerializer::Deserialize(Reader, RootObject);
//
//	ToConvert.guid = RootObject->GetStringField("guid");
//	ToConvert.account = RootObject->GetStringField("account");
//	ToConvert.custom_name = RootObject->GetStringField("custom_name");
//	ToConvert.faction = static_cast<EFaction>(RootObject->GetNumberField("faction"));
//	ToConvert.admin_level = RootObject->GetNumberField("admin_level");
//	ToConvert.prem_level = RootObject->GetNumberField("prem_level");
//	ToConvert.database_id = RootObject->GetNumberField("database_id");
//
//	// Convert location
//	FVector Location;
//	USC_AnswerConverter::ConvertJsonStringToVector(RootObject->GetStringField("location"), Location);
//	ToConvert.location = Location;
//
//	// Convert rotation
//	FRotator Rotator;
//	USC_AnswerConverter::ConvertJsonStringToRotator(RootObject->GetStringField("rotation"), Rotator);
//	ToConvert.rotation = Rotator;
//	
//	ToConvert.health = RootObject->GetNumberField("health");
//	ToConvert.food = RootObject->GetNumberField("food");
//	ToConvert.water = RootObject->GetNumberField("water");
//	ToConvert.radiation = RootObject->GetNumberField("radiation");
//	ToConvert.psy = RootObject->GetNumberField("psy");
//
//	if (Answer.Success)
//	{
//		Out = ToConvert;
//		return true;
//	}
//	else if (!Answer.Success && Answer.Error == "NotExist")
//	{
//		Out = ToConvert;
//		return true;
//	}
//	else
//	{
//		USC_StaticLibrary::Log(ELogType::Error, "ERROR: USC_AnswerConverter::ConvertToPlayerData(), Can't convert structure!");
//	}
//
//
//	return false;
//}

//bool USC_AnswerConverter::ConvertFromPlayerData(const FPlayerData& Input, FString& Out)
//{
//	FString OutputString;
//	TSharedRef<TJsonWriter<>> JsonWriter = TJsonWriterFactory<>::Create(&OutputString);
//
//	TSharedPtr<FJsonObject> LocationJson = MakeShareable(new FJsonObject);
//	LocationJson->SetNumberField("x", Input.location.X);
//	LocationJson->SetNumberField("y", Input.location.Y);
//	LocationJson->SetNumberField("z", Input.location.Z);
//
//	TSharedPtr<FJsonObject> RotationJson = MakeShareable(new FJsonObject);
//	RotationJson->SetNumberField("pitch", Input.rotation.Pitch);
//	RotationJson->SetNumberField("yaw", Input.rotation.Yaw);
//	RotationJson->SetNumberField("roll", Input.rotation.Roll);
//
//	TSharedPtr<FJsonObject> CurJsonVal = MakeShareable(new FJsonObject);
//	CurJsonVal->SetStringField("guid", Input.guid);
//	CurJsonVal->SetStringField("account", Input.account);
//	CurJsonVal->SetStringField("custom_name", Input.custom_name);
//	CurJsonVal->SetNumberField("faction", static_cast<int>(Input.faction));
//	CurJsonVal->SetNumberField("admin_level", Input.admin_level);
//	CurJsonVal->SetNumberField("prem_level", Input.prem_level);
//	CurJsonVal->SetNumberField("database_id", Input.database_id);
//	CurJsonVal->SetObjectField("location", LocationJson);
//	CurJsonVal->SetObjectField("rotation", RotationJson);
//	CurJsonVal->SetNumberField("health", Input.health);
//	CurJsonVal->SetNumberField("food", Input.food);
//	CurJsonVal->SetNumberField("water", Input.water);
//	CurJsonVal->SetNumberField("radiation", Input.radiation);
//	CurJsonVal->SetNumberField("psy", Input.psy);
//
//	bool result = FJsonSerializer::Serialize(CurJsonVal.ToSharedRef(), JsonWriter);
//
//	Out = OutputString;
//	return result;
//}

bool USC_AnswerConverter::ConvertJsonStringToVector(const FString& Input, FVector& Out)
{
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Input);
	TSharedPtr<FJsonObject> RootObject = MakeShareable(new FJsonObject);
	bool result = FJsonSerializer::Deserialize(Reader, RootObject);

	auto x = RootObject->GetNumberField("x");
	auto y = RootObject->GetNumberField("y");
	auto z = RootObject->GetNumberField("z");

	FVector Vec = FVector(x, y, z);
	Out = Vec;

	return result;
}

bool USC_AnswerConverter::ConvertJsonStringToRotator(const FString& Input, FRotator& Out)
{
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Input);
	TSharedPtr<FJsonObject> RootObject = MakeShareable(new FJsonObject);
	bool result = FJsonSerializer::Deserialize(Reader, RootObject);

	auto pitch = RootObject->GetNumberField("pitch");
	auto yaw = RootObject->GetNumberField("yaw");
	auto roll = RootObject->GetNumberField("roll");

	FRotator Rot = FRotator(pitch, yaw, roll);
	Out = Rot;

	return result;
}

bool USC_AnswerConverter::ConvertToPlayerAccountData(const FBackendAnswer& Answer, FPlayerAccountData& Out)
{
	FPlayerAccountData ToConvert;
	FJsonObjectConverter::JsonObjectStringToUStruct(Answer.Answer, &ToConvert);

	if (Answer.Success)
	{
		Out = ToConvert;
		return true;
	}
	else
	{
		USC_StaticLibrary::Log(ELogType::Error, "ERROR: USC_AnswerConverter::ConvertToPlayerAccountData(), Can't convert structure!");
	}


	return false;
}

bool USC_AnswerConverter::ConvertToPlayerCharacterData(const FBackendAnswer& Answer, FPlayerCharacterData& Out)
{
	FPlayerCharacterData ToConvert;
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Answer.Answer);
	TSharedPtr<FJsonObject> RootObject = MakeShareable(new FJsonObject);

	bool result = FJsonSerializer::Deserialize(Reader, RootObject);

	ToConvert.database_id = RootObject->GetNumberField("database_id");

	// convert loc
	FVector Location;
	ConvertJsonStringToVector(RootObject->GetStringField("location"), Location);
	ToConvert.location = Location;

	// convert rot
	FRotator Rotation;
	ConvertJsonStringToRotator(RootObject->GetStringField("rotation"), Rotation);
	ToConvert.rotation = Rotation;

	ToConvert.health = RootObject->GetNumberField("health");
	ToConvert.food = RootObject->GetNumberField("food");
	ToConvert.water = RootObject->GetNumberField("water");
	ToConvert.radiation = RootObject->GetNumberField("radiation");
	ToConvert.psy = RootObject->GetNumberField("psy");

	Out = ToConvert;

	return result;
}
