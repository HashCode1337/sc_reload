// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../Core/SC_CommonDataModels.h"

#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"

#include "SC_HttpRequest.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAnswer, FBackendAnswer, Answer);

/**
 * 
 */
UCLASS(Blueprintable)
class SC_API USC_HttpRequest : public UObject
{
	GENERATED_BODY()

	UPROPERTY();
	bool BindLambda = true;

public:

	UPROPERTY(BlueprintAssignable, Category = "SC")
	FOnAnswer Answer;

	UFUNCTION(BlueprintCallable)
	void EnableCallback(bool Enable);

	UFUNCTION(BlueprintCallable)
	void MakeGet(FString Url);

	UFUNCTION(BlueprintCallable)
	void MakePost(FString Url, FString Content);

	UFUNCTION(BlueprintNativeEvent)
	void OnAnswered(FBackendAnswer Answered);
	
};
