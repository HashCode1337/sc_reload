// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../Components/SC_HealthComponent.h"
#include "SC_Damagable.generated.h"

UINTERFACE(MinimalAPI)
class USC_Damagable : public UInterface
{
	GENERATED_BODY()
};

class SC_API ISC_Damagable
{
	GENERATED_BODY()

public:

	virtual float GetHealth() = 0;
	virtual void Die(FDamagedTenParams DamageDescription) = 0;
	virtual void Damaged(FDamagedTenParams DamageDescription) = 0;
};
