// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SC_DatabaseSavable.generated.h"

UENUM(BlueprintType)
enum class EDatabaseSaveRequestType : uint8
{
	Unknown = 0 UMETA(DisplayName = "Unknown"),
	FullPlayerSave = 1 UMETA(DisplayName = "FullPlayerUpdate")
};

// Structure to holding data about subject, which will be send to backend
USTRUCT(BlueprintType)
struct FDatabaseObjectDetails
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Database")
	int DatabaseId = -1;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Database")
	FString DatabaseGuid = "";

	// Database table
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Database")
	EDatabaseSaveRequestType Type;

	// Cached Json string prepared to transfer to backend
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Database")
	FString JsonContent = "";
};

// This class does not need to be modified.
UINTERFACE(MinimalAPI, BlueprintType, meta = (CannotImplementInterfaceInBlueprint))
class USC_DatabaseSavable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SC_API ISC_DatabaseSavable
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	virtual void SetupDatabaseObjectDetails(int DatabaseId, FString DatabaseGuid, EDatabaseSaveRequestType Type) = 0;
	UFUNCTION(BlueprintCallable)
	virtual void CacheContent() = 0;
	UFUNCTION(BlueprintCallable)
	virtual void MakeFullSaveRequest() = 0;

};
