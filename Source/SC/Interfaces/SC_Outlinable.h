// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SC_Outlinable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, BlueprintType, meta = (CannotImplementInterfaceInBlueprint))
class USC_Outlinable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SC_API ISC_Outlinable
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	virtual void SetOutlineEnabled(bool bEnableOutline) = 0;

	UFUNCTION(BlueprintCallable)
	virtual bool CanBeOutlined() = 0;

};
