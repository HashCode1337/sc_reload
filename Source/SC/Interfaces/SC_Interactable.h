// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SC_Interactable.generated.h"

UENUM(BlueprintType)
enum class EInteractableObjectType : uint8
{
	Unknown = 0 UMETA(DisplayName = "Unknown"),
	Weapon = 1 UMETA(DisplayName = "Weapon")
};

// This class does not need to be modified.
UINTERFACE(MinimalAPI, BlueprintType, meta = (CannotImplementInterfaceInBlueprint))
class USC_Interactable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SC_API ISC_Interactable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual EInteractableObjectType GetInteractType() = 0;
	virtual bool IsInteractable() = 0;
	virtual void SetOutlineEnabled(bool bEnabled) = 0;
	virtual bool IsOutlined() = 0;

	virtual void Pickup(APawn* NewOwner) {};
	virtual void Drop() {};
	virtual void Interact();

};
