// Copyright Epic Games, Inc. All Rights Reserved.

#include "SCCharacter.h"
#include "SCProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "JsonObjectConverter.h"
#include "Core/SC_PlayerState.h"
#include "Core/SC_StaticLibrary.h"
#include "Service/SC_AnswerConverter.h"
#include "Components/FPSTemplate_CharacterComponent.h"
#include "Components/FPS_CharacterMovementComponent.h"
#include "Service/SC_HttpRequest.h"
#include "SCGameMode.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/KismetMathLibrary.h"
#include "SCBaseWeapon.h"
#include "Components/SC_WeaponFireStatusHandler.h"
#include "Core/SC_PlayerController.h"
#include "Components/AudioComponent.h"
#include "Components/SC_CharWeaponHandler.h"
#include "SubSystems/SC_GameConfigSubsystem.h"
#include "Components/SC_CharacterInteractionComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

ASCCharacter::ASCCharacter(const FObjectInitializer& ObjectInitializer)
	:Super
	(
		ObjectInitializer
		.SetDefaultSubobjectClass<UFPS_CharacterMovementComponent>(ACharacter::CharacterMovementComponentName)
		.SetDefaultSubobjectClass<USkeletalMeshComponent>(TEXT("ThirdPersonMesh"))
	)
{
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	CharWeaponHandler = CreateDefaultSubobject<USC_CharWeaponHandler>(TEXT("CharWeaponHandler"));
	InteractionComponent = CreateDefaultSubobject<USC_CharacterInteractionComponent>(TEXT("CharacterInteractionComp"));

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("PlayerCamera"));
	Camera->bUsePawnControlRotation = true;
	Camera->SetupAttachment(RootComponent);

	FirstPersonMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonMesh"));
	FirstPersonMesh->SetupAttachment(Camera);
	FirstPersonMesh->SetOnlyOwnerSee(true);
	FirstPersonMesh->SetCastShadow(false);

	ThirdPersonMesh->SetOwnerNoSee(true);
	ThirdPersonMesh->SetCastHiddenShadow(true);

	AudioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComp"));
	AudioComp->SetupAttachment(FirstPersonMesh);
}

#pragma region Core

void ASCCharacter::BeginPlay()
{
	Super::BeginPlay();
	WeaponStatusHandler = NewObject<USC_WeaponFireStatusHandler>(this);
}

void ASCCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	float NewFOV = UKismetMathLibrary::FInterpTo(Camera->FieldOfView, CurrentFOV, DeltaSeconds, 5.f);
	Camera->SetFieldOfView(NewFOV);
}

void ASCCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	FPAnimInstance = Cast<USC_FirstPersonAnimInstance>(FirstPersonMesh->GetAnimInstance());
	TPAnimInstance = Cast<USC_ThirdPersonAnimInstance>(ThirdPersonMesh->GetAnimInstance());
	
	if (!FPAnimInstance)	USC_StaticLibrary::Log(ELogType::Error, "ERROR! ASCCharacter::BeginPlay(), FPAnimInstance cast fails!");
	if (!TPAnimInstance)	USC_StaticLibrary::Log(ELogType::Error, "ERROR! ASCCharacter::BeginPlay(), FPAnimInstance cast fails!");
	if (FPAnimInstance)		FPAnimInstance->Setup();
}

void ASCCharacter::InputTurn(float val)
{
	if (FMath::Abs(GetWorld()->DeltaTimeSeconds) >= 0.39f) return;

	float currentVal = val * GetWorld()->DeltaTimeSeconds * 50.f;
	LastSwayOffset.X = currentVal;

	float ValSign = UKismetMathLibrary::SignOfFloat(currentVal);
	float DeltaTime = GetWorld()->DeltaTimeSeconds;
	float SwayTarget = LastSwayOffset.X * SwayMultiplier;

	CurrentSwayOffset.X = UKismetMathLibrary::FInterpTo(CurrentSwayOffset.X, SwayTarget, DeltaTime, SwaySpeed);
	
	// Too long warning
	if (GetWorld()->DeltaTimeSeconds > 0.2f)
	{
		UKismetSystemLibrary::PrintString(GetWorld(), FString::Printf(TEXT("DT : %f"), GetWorld()->DeltaTimeSeconds));
	}

	AddControllerYawInput(currentVal);
}

void ASCCharacter::InputLookUp(float val)
{
	if (FMath::Abs(GetWorld()->DeltaTimeSeconds) >= 0.39f) return;
	
	float currentVal = val * GetWorld()->DeltaTimeSeconds * 50.f;
	LastSwayOffset.Y = currentVal;

	float ValSign = UKismetMathLibrary::SignOfFloat(currentVal);
	float DeltaTime = GetWorld()->DeltaTimeSeconds;
	float SwayTarget = LastSwayOffset.Y * SwayMultiplier;
	
	CurrentSwayOffset.Y = UKismetMathLibrary::FInterpTo(CurrentSwayOffset.Y, SwayTarget, DeltaTime, SwaySpeed);
	
	AddControllerPitchInput(currentVal);
}

void ASCCharacter::CycleWeaponState(FKey Key)
{
	bool bCanLower = WeaponState == ECharacterWeaponState::Raised && WeaponStatusHandler->IsCharacterCanLowerWeapon();
	bool bCanRaise = WeaponState == ECharacterWeaponState::Lowered && WeaponStatusHandler->IsCharacterCanRaiseWeapon();

	if			(bCanLower) S_SetArmed(ECharacterWeaponState::Lowered);
	else if		(bCanRaise) S_SetArmed(ECharacterWeaponState::Raised);
	else		UKismetSystemLibrary::PrintString(GetWorld(), "Cant Raise/Lower Weapon", true, true, FColor::Red); 
}

void ASCCharacter::AimPressed()
{
	bool bCanAim = WeaponStatusHandler->IsCharacterCanAim();

	if (bCanAim) CurrentFOV = 50.f;
	S_SetAiming(true);
}

void ASCCharacter::AimReleased()
{
	CurrentFOV = DefaultFOV;
	S_SetAiming(false);
}

void ASCCharacter::SetFiring(bool bFiring)
{
	if (!GetCurrentWeapon()) return;

	if (bFiring)	GetCurrentWeapon()->FireModeComp->FireStart();
	else			GetCurrentWeapon()->FireModeComp->FireEnd();
}

void ASCCharacter::S_SetAiming_Implementation(bool bAim)
{
	if (bAim)
	{
		if (WeaponStatusHandler->IsCharacterCanAim())
		{
			Aiming = true;
			WeaponState = ECharacterWeaponState::Aiming;
		}
	}
	else
	{
		if (WeaponState == ECharacterWeaponState::Aiming)
		{
			Aiming = false;
			WeaponState = ECharacterWeaponState::Raised;
		}
	}
}

void ASCCharacter::S_SetArmed_Implementation(ECharacterWeaponState NewStatus)
{
	WeaponState = NewStatus;
}

void ASCCharacter::M_PlaySoundPain_Implementation()
{
	AudioComp->SetSound(SoundPain);
	AudioComp->Play();
}

void ASCCharacter::M_ImpactRagdoll_Implementation(FVector ImpactLocation /*= FVector::ZeroVector*/, float ImpactForce /*= 0.0f*/)
{
	USC_StaticLibrary::RagdollWithImpact(ThirdPersonMesh, ImpactLocation, ImpactForce);
	Camera->bUsePawnControlRotation = false;

	FAttachmentTransformRules TransformRulers(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, false);
	Camera->AttachToComponent(ThirdPersonMesh, TransformRulers, "eyelid_1");
}

FTransform ASCCharacter::GetFPHandGunSocketTransform(ERelativeTransformSpace RTS)
{
	return FirstPersonMesh->GetSocketTransform("lead_gun", RTS);
}

void ASCCharacter::PlayFireMontage(UAnimMontage* TPFireMontage, UAnimMontage* FPFireMontage)
{
	bool bIsLocal = IsLocallyControlled();

	if (bIsLocal)		FPAnimInstance->Montage_Play(FPFireMontage, 1.f, EMontagePlayReturnType::MontageLength, 0.f, true);
	else				TPAnimInstance->Montage_Play(TPFireMontage, 1.f, EMontagePlayReturnType::MontageLength, 0.f, true);
}

void ASCCharacter::PlayReloadMontage(UAnimMontage* TPReloadMontage, UAnimMontage* FPReloadMontage)
{
	if (IsLocallyControlled())
	{
		FPAnimInstance->Montage_Play(FPReloadMontage, 1.f, EMontagePlayReturnType::MontageLength, 0.f, true);
		FTimerHandle TH;
		FTimerDelegate TD;

	}
	else
	{
		float FPLen = FPReloadMontage->CalculateSequenceLength();
		float TPLen = TPReloadMontage->CalculateSequenceLength();
		TPAnimInstance->Montage_Play(TPReloadMontage, TPLen/FPLen, EMontagePlayReturnType::MontageLength, 0.f, true);
	}
}

#pragma endregion

#pragma region BackendCaching
void ASCCharacter::SetPlayerAccountData(const FPlayerAccountData& PlayerAccountData)
{
	ASC_PlayerState* PS = GetPlayerState<ASC_PlayerState>();
	PS->SetPlayerAccountData(PlayerAccountData);
}

void ASCCharacter::SetPlayerCharacterData(const FPlayerCharacterData& PlayerCharacterData)
{
	this->m_PlayerCharacterData = PlayerCharacterData;
}
#pragma endregion
#pragma region DatabaseSavable
void ASCCharacter::SetupDatabaseObjectDetails(int DatabaseId, FString DatabaseGuid, EDatabaseSaveRequestType Type)
{
	FDatabaseObjectDetails DatabaseObjectDetails;
	DatabaseObjectDetails.DatabaseId = DatabaseId;
	DatabaseObjectDetails.DatabaseGuid = DatabaseGuid;
	DatabaseObjectDetails.Type = Type;

	ASC_PlayerState* PS = GetPlayerState<ASC_PlayerState>();
	PS->SetDatabaseObjectDetails(DatabaseObjectDetails);
}

void ASCCharacter::CacheContent()
{
	ASC_PlayerState* PS = GetPlayerState<ASC_PlayerState>();
	
	FDatabaseObjectDetails DatabaseObjectDetails;
	FPlayerAccountData PlayerAccountData;

	m_PlayerCharacterData.location = GetActorLocation();
	m_PlayerCharacterData.rotation = GetActorRotation();

	PS->GetDatabasableData(DatabaseObjectDetails, PlayerAccountData);

	FString PlayerAccountData_Json;
	FString PlayerCharacterData_Json;

	TSharedPtr<FJsonObject> JsonAccount = FJsonObjectConverter::UStructToJsonObject(PlayerAccountData);
	TSharedPtr<FJsonObject> JsonCharacter = FJsonObjectConverter::UStructToJsonObject(m_PlayerCharacterData);

	TSharedPtr<FJsonObject> JsonContent = MakeShareable(new FJsonObject());
	JsonContent->SetObjectField("account", JsonAccount);
	JsonContent->SetObjectField("character", JsonCharacter);

	FString CachedContent;
	TSharedRef<TJsonWriter<>> JsonWriter = TJsonWriterFactory<>::Create(&CachedContent);
	FJsonSerializer::Serialize(JsonContent.ToSharedRef(), JsonWriter);

	PS->UpdateSavableCache(CachedContent);

}

void ASCCharacter::MakeFullSaveRequest()
{
	USC_HttpRequest* Request = NewObject<USC_HttpRequest>();
	ASC_PlayerState* PS = GetPlayerState<ASC_PlayerState>();

	FGameConfig GC;
	FString Content;

	
	USC_GameConfigSubsystem* GCS = GetWorld()->GetGameInstance()->GetSubsystem<USC_GameConfigSubsystem>();
	GC = GCS->GetGameConfig();

	PS->GetSavableCache(Content);
	
	FString UrlReq = FString::Printf(TEXT("%s/save_player"), *GC.Backend);
	Request->EnableCallback(false);
	Request->MakePost(UrlReq, Content);
}
#pragma endregion

void ASCCharacter::UnPossessed()
{

	TScriptInterface<ISC_DatabaseSavable> Casted = TScriptInterface<ISC_DatabaseSavable>(Cast<UObject>(this));

	if (HasAuthority())
	{
		ASCGameMode* GM =		Cast<ASCGameMode>(GetWorld()->GetAuthGameMode());
		if (GM)					GM->SaveRequest(Casted);
	}

	Super::UnPossessed();
	CharWeaponHandler->DestroyCurrentWeapon();

}

void ASCCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	if (PlayerInputComponent)
	{
		PlayerInputComponent->BindAxis("Turn", this, &ASCCharacter::InputTurn);
		PlayerInputComponent->BindAxis("LookUp", this, &ASCCharacter::InputLookUp);
		PlayerInputComponent->BindAction("CycleWeaponState", IE_Pressed, this, &ASCCharacter::CycleWeaponState);
		PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &ASCCharacter::AimPressed);
		PlayerInputComponent->BindAction("Aim", IE_Released, this, &ASCCharacter::AimReleased);
		PlayerInputComponent->BindAction("Drop", IE_Pressed, CharWeaponHandler, &USC_CharWeaponHandler::S_DropCurrentWeaponRequest);
	}
}

void ASCCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ASCCharacter, Aiming);
	DOREPLIFETIME(ASCCharacter, WeaponState);
}

FGenericTeamId ASCCharacter::GetGenericTeamId() const
{
	if (HasAuthority())
	{
		ASC_PlayerController* PC =		GetController<ASC_PlayerController>();
		if (PC)							return PC->GetFactionId();
	}
	
	return FGenericTeamId(255);
}
