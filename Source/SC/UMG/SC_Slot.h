// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SC_Slot.generated.h"

/**
 * 
 */
UCLASS()
class SC_API USC_Slot : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UObject* Content;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = "true"))
	class UTexture2D* ImgSlotTexture;

	UPROPERTY( meta = ( BindWidget ) )
	class UImage* ImgSlot;

public:

	UFUNCTION(BlueprintCallable)
	void SetTexture(UTexture2D* NewTexture);
	UFUNCTION(BlueprintCallable)
	void SetContent(UObject* NewContent);


protected:

	void NativeConstruct() override;
	FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	void NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent, UDragDropOperation*& OutOperation) override;
	bool NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation) override;
};
