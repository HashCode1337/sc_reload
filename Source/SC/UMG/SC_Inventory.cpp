// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_Inventory.h"
#include "../SCCharacter.h"
#include "SC_InventoryWeaponSlot.h"


void USC_Inventory::UpdateItems()
{
	MyChar = GetOwningPlayerPawn<ASCCharacter>();

	if (MyChar)
	{
		ASCBaseWeapon* Weapon = MyChar->GetCurrentWeapon();
		if (Weapon)
		{
			PrimaryWeaponSlot->SetItemToSlot(Weapon);
		}
	}
}
