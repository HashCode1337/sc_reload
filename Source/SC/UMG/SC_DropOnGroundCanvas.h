// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SC_DropOnGroundCanvas.generated.h"

class UCanvasPanel;

/**
 * 
 */
UCLASS(BlueprintType)
class SC_API USC_DropOnGroundCanvas : public UUserWidget
{
	GENERATED_BODY()
	
protected:

	UPROPERTY( meta = ( BindWidget ) )
	UCanvasPanel* DropArea;

	bool NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation) override;

};
