// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_InventoryWeaponSlot.h"
#include "SC_Slot.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "../SCBaseWeapon.h"
#include "Kismet/KismetSystemLibrary.h"

void USC_InventoryWeaponSlot::SetItemToSlot(ASCBaseWeapon* Weapon)
{
	SlotWeapon->SetContent(Weapon);
	SlotWeapon->SetTexture(Weapon->WeaponImage);
}

void USC_InventoryWeaponSlot::NativeConstruct()
{
	Super::NativeConstruct();
}
