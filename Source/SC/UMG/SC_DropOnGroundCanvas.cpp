// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_DropOnGroundCanvas.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../SCBaseWeapon.h"
#include "Blueprint/DragDropOperation.h"

bool USC_DropOnGroundCanvas::NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation)
{
	bool Result = Super::NativeOnDrop(InGeometry, InDragDropEvent, InOperation);

	ASCBaseWeapon* DropWeapon = Cast<ASCBaseWeapon>(InOperation->Payload);
	if (DropWeapon)
	{
		UKismetSystemLibrary::PrintString(this, DropWeapon->GetName());
		//DropWeapon->PawnOwner->
	}
	


	return Result;
}
