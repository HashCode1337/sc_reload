// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_Slot.h"
#include "Components/Image.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

void USC_Slot::SetTexture(UTexture2D* NewTexture)
{
	ImgSlot->SetBrushFromTexture(NewTexture);

}

void USC_Slot::SetContent(UObject* NewContent)
{
	Content = NewContent;
}

void USC_Slot::NativeConstruct()
{
	SetTexture(ImgSlotTexture);
	Super::NativeConstruct();
}

FReply USC_Slot::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	auto result = Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
	
	FKey LMB("LeftMouseButton"); 

	FEventReply Reply = UWidgetBlueprintLibrary::DetectDragIfPressed(InMouseEvent, this, LMB);

	return Reply.NativeReply;
}

void USC_Slot::NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent, UDragDropOperation*& OutOperation)
{
	Super::NativeOnDragDetected(InGeometry, InMouseEvent, OutOperation);

	UDragDropOperation* DragOp = NewObject<UDragDropOperation>();
	DragOp->Pivot = EDragPivot::MouseDown;
	DragOp->DefaultDragVisual = this;
	DragOp->Payload = Content;

	OutOperation = DragOp;

	UWidgetBlueprintLibrary::CreateDragDropOperation(DragOp->GetClass());

	UKismetSystemLibrary::PrintString(this, "NativeOnDragDetected");
}

bool USC_Slot::NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation)
{
	auto result = Super::NativeOnDrop(InGeometry, InDragDropEvent, InOperation);

	UKismetSystemLibrary::PrintString(this, "NativeOnDrop");

	return result;
}
