// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SC_Inventory.generated.h"

class USC_InventoryWeaponSlot;
class ASCCharacter;
class USC_DropOnGroundCanvas;

UCLASS(BlueprintType)
class SC_API USC_Inventory : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ASCCharacter* MyChar;

	UPROPERTY( meta = ( BindWidget ) )
	USC_InventoryWeaponSlot* PrimaryWeaponSlot;

	UPROPERTY( meta = ( BindWidget ) )
	USC_DropOnGroundCanvas* DropArea;



public:

	UFUNCTION(BlueprintCallable)
	void UpdateItems();
};
