// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SC_InventoryWeaponSlot.generated.h"

class UImage;
class UTextBlock;
class USC_Slot;
class ASCBaseWeapon;

UCLASS(BlueprintType)
class SC_API USC_InventoryWeaponSlot : public UUserWidget
{
	GENERATED_BODY()
	
protected:

	UPROPERTY( meta = ( BindWidget ) )
	UImage* BackDetails;

	UPROPERTY( meta = ( BindWidget ) )
	UTextBlock* TextDetails;

	UPROPERTY( meta = ( BindWidget ) )
	UImage* BackWeapon;

	UPROPERTY( meta = ( BindWidget ) )
	USC_Slot* SlotWeapon;

	UPROPERTY( meta = ( BindWidget ) )
	UImage* BackMagazine;
	
	UPROPERTY( meta = ( BindWidget ) )
	UImage* ImgMagazine;

	UPROPERTY( meta = ( BindWidget ) )
	UImage* BackBullets;

	UPROPERTY( meta = ( BindWidget ) )
	UImage* ImgBullets;

	UPROPERTY( meta = ( BindWidget ) )
	UImage* BackModule_1;

	UPROPERTY( meta = ( BindWidget ) )
	UImage* BackModule_2;

	UPROPERTY( meta = ( BindWidget ) )
	UImage* BackModule_3;

	UPROPERTY( meta = ( BindWidget ) )
	UImage* BackModule_4;


public:

	UFUNCTION(BlueprintCallable)
	void SetItemToSlot(ASCBaseWeapon* Weapon);

	void NativeConstruct() override;

};
