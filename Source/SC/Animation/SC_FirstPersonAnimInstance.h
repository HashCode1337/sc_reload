// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "../Core/SC_CommonDataModels.h"
#include "SC_FirstPersonAnimInstance.generated.h"

class ASCCharacter;

/**
 * 
 */
UCLASS()
class SC_API USC_FirstPersonAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
private:

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	ASCCharacter* Character;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FVector2D SwayOffsets;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FRotator ViewRotation;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float Speed;

	UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float AimFactor;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FVector RecoilXYFront;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float LoweredWeaponFactor;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	bool bIsReady;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	EWeaponClass WeaponClass;

	// Relative
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FVector AimSocketLocation;

	// Relative
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FRotator AimSocketRotation;

	// Relative
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FVector AimPointLocation;

	// Relative
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FRotator AimPointRotation;



public:
	
	void Setup();
	void SetWeaponClass();

	void NativeInitializeAnimation() override;
	void NativeUpdateAnimation(float DeltaSeconds) override;
	void NativeBeginPlay() override;

private:
	void UpdatePlayerViewRotation();
	void UpdatePlayerWeaponRecoil();
	
	UFUNCTION(BlueprintCallable)
	void SetAimSocket();

	UFUNCTION(BlueprintCallable)
	void SetAimPoint();

	UFUNCTION(BlueprintCallable)
	void UpdateCharacterWeaponStatus();

protected:
	bool HandleNotify(const FAnimNotifyEvent& AnimNotifyEvent) override;
	void PreUpdateAnimation(float DeltaSeconds) override;

};
