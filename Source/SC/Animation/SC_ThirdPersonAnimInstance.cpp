// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_ThirdPersonAnimInstance.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "../SCCharacter.h"
#include "../Core/SC_StaticLibrary.h"
#include "../SCBaseWeapon.h"
#include "Kismet/KismetSystemLibrary.h"

void USC_ThirdPersonAnimInstance::UpdateWeaponGripOffset()
{
	if (Player)
	{
		if (Player->GetCurrentWeapon())
		{
			ASCBaseWeapon* PlayerWeapon = Player->GetCurrentWeapon();

			FTransform RightHandSocketTransform = Player->GetMesh()->GetSocketTransform(Player->RightHandSocketName, ERelativeTransformSpace::RTS_World);
			FTransform WeaponGripTransform = PlayerWeapon->GetWeaponMesh()->GetSocketTransform(PlayerWeapon->SocketGrip, ERelativeTransformSpace::RTS_World);

			LeftHandIK = WeaponGripTransform.GetRelativeTransform(RightHandSocketTransform);
		}
	}
	else
	{
		USC_StaticLibrary::Log(ELogType::Error, "ERROR! USC_ThirdPersonAnimInstance::SetWeaponGripOffset(), Cant set grip offset!");
	}
}

void USC_ThirdPersonAnimInstance::Setup()
{
	SetWeaponClass();
}

void USC_ThirdPersonAnimInstance::SetWeaponClass()
{
	if (Player && Player->GetCurrentWeapon())
	{
		WeaponClass = Player->GetCurrentWeapon()->WeaponClassEnum;
	}
	else
	{
		WeaponClass = EWeaponClass::Free;
	}
}

void USC_ThirdPersonAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	Player = Cast<ASCCharacter>(TryGetPawnOwner());
	if (!Player)
	{
		USC_StaticLibrary::Log(ELogType::Error, "USC_ThirdPersonAnimInstance::NativeInitializeAnimation(), Player cast fails!");
	}

	Setup();
}

void USC_ThirdPersonAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();

}

void USC_ThirdPersonAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	if (Player)
	{
		FVector PlayerVelocity = Player->GetVelocity();
		FTransform PlayerTransform = Player->GetActorTransform();
		Speed = PlayerVelocity.Size2D();
		Angle = UKismetMathLibrary::InverseTransformDirection(PlayerTransform, PlayerVelocity).Rotation().Yaw;
		AimFactor = UKismetMathLibrary::FInterpTo(AimFactor, Player->Aiming, DeltaSeconds, 10.f);
		ViewPitch = ((float)Player->RemoteViewPitch / 255.f) * 360.f;
		PlayerWeaponState = Player->WeaponState;

		if (GetWorld()->WorldType == EWorldType::EditorPreview)
		{
			bIsOnAir = false;
		}
		else
		{
			bIsOnAir = !Player->GetMovementComponent()->IsMovingOnGround();
		}
		
		bIsCrouching = Player->GetMovementComponent()->IsCrouching();
		UpdateWeaponGripOffset();
	}
}

