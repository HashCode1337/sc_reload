// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "../Core/SC_CommonDataModels.h"
#include "SC_ThirdPersonAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class SC_API USC_ThirdPersonAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
protected:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	class ASCCharacter* Player;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float Speed;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float Angle;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float AimFactor;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float ViewPitch;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	bool bIsAlive = true;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	bool bIsOnAir = false;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	bool bIsCrouching = false;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	ECharacterWeaponState PlayerWeaponState;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	EWeaponClass WeaponClass;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	FTransform LeftHandIK;

protected:

	UFUNCTION()
	void UpdateWeaponGripOffset();


public:

	void Setup();
	void SetWeaponClass();

	void NativeInitializeAnimation() override;
	void NativeBeginPlay() override;
	void NativeUpdateAnimation(float DeltaSeconds) override;
};
