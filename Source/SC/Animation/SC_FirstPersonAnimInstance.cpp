// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_FirstPersonAnimInstance.h"
#include "SC/SCBaseWeapon.h"
#include "../SCCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Camera/CameraComponent.h"
#include "../Core/SC_PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "../Components/SC_RecoilComponent.h"

void USC_FirstPersonAnimInstance::Setup()
{
	bIsReady = false;

	InitializeAnimation(false);

	if (GetSkelMeshComponent()->GetOwner())
	{
		Character = Cast<ASCCharacter>(TryGetPawnOwner());

		if (Character->IsLocallyControlled())
		{
			AimSocketLocation = FVector::ZeroVector;
			AimSocketRotation = FRotator::ZeroRotator;
			AimPointLocation = FVector::ZeroVector;
			AimPointRotation = FRotator::ZeroRotator;
			SetWeaponClass();

			if (Character->GetCurrentWeapon())
			{
				FTimerDelegate TD;
				TD.BindLambda([&]() {
					
					SetAimSocket();
					SetAimPoint();
					bIsReady = true;
					});
				Character->GetWorld()->GetTimerManager().SetTimerForNextTick(TD);
			}


		}
	}
}

void USC_FirstPersonAnimInstance::SetWeaponClass()
{
	if (Character && Character->GetCurrentWeapon())
	{
		WeaponClass = Character->GetCurrentWeapon()->WeaponClassEnum;
	}
	else
	{
		WeaponClass = EWeaponClass::Free;
	}
}

void USC_FirstPersonAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
}

void USC_FirstPersonAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	
	if (Character && Character->GetController() && bIsReady)
	{
		UpdatePlayerViewRotation();
		UpdatePlayerWeaponRecoil();
	}
}

void USC_FirstPersonAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();
}

void USC_FirstPersonAnimInstance::UpdatePlayerViewRotation()
{
	ViewRotation = Character->GetController()->GetControlRotation();
	SwayOffsets = Character->GetWeaponSwayOffset();
	Speed = Character->GetVelocity().Size();
}

void USC_FirstPersonAnimInstance::UpdatePlayerWeaponRecoil()
{
	ASCBaseWeapon* CharWeapon = Character->GetCurrentWeapon();

	if (CharWeapon)
	{
		RecoilXYFront.X = -CharWeapon->RecoilComp->XAccumulated;
		RecoilXYFront.Y = CharWeapon->RecoilComp->YAccumulated;
		RecoilXYFront.Z = CharWeapon->RecoilComp->YAccumulated;
	}
	else
	{
		RecoilXYFront = FVector::ZeroVector;
	}
}

void USC_FirstPersonAnimInstance::SetAimSocket()
{
	if (Character)
	{
		FTransform AimSocketTransform = Character->GetCurrentWeapon()->WeaponMesh->GetSocketTransform("S_Aim");
		FTransform FPMeshTransform = Character->FirstPersonMesh->GetComponentTransform();

		FTransform Offset = UKismetMathLibrary::MakeRelativeTransform(AimSocketTransform, FPMeshTransform);

		AimSocketLocation = Offset.GetLocation();
		AimSocketRotation = Offset.GetRotation().Rotator();
	}
}

void USC_FirstPersonAnimInstance::SetAimPoint()
{
	if (Character)
	{
		FTransform CameraTransform = Character->Camera->GetComponentTransform();
		FVector CameraAimPointOffset = CameraTransform.GetTranslation() + CameraTransform.GetRotation().GetForwardVector() * 20.f;
		CameraTransform.SetTranslation(CameraAimPointOffset);

		FTransform FPMeshTransform = Character->FirstPersonMesh->GetComponentTransform();

		FTransform Offset = UKismetMathLibrary::MakeRelativeTransform(CameraTransform, FPMeshTransform);

		
		AimPointLocation = Offset.GetLocation();
		AimPointRotation = Offset.GetRotation().Rotator();
	}
}

void USC_FirstPersonAnimInstance::UpdateCharacterWeaponStatus()
{
	float dt = GetWorld()->GetDeltaSeconds();

	if (Character)
	{
		switch (Character->WeaponState)
		{
		case ECharacterWeaponState::Lowered:
			LoweredWeaponFactor = UKismetMathLibrary::FInterpTo(LoweredWeaponFactor, 1.f, dt, 7.f);
			break;
		case ECharacterWeaponState::Raised:
		case ECharacterWeaponState::Aiming:
			LoweredWeaponFactor = UKismetMathLibrary::FInterpTo(LoweredWeaponFactor, 0.f, dt, 7.f);
			break;
		
		default:
			break;
		}
	}
}

bool USC_FirstPersonAnimInstance::HandleNotify(const FAnimNotifyEvent& AnimNotifyEvent)
{
	bool Result = Super::HandleNotify(AnimNotifyEvent);
	return Result;
}

void USC_FirstPersonAnimInstance::PreUpdateAnimation(float DeltaSeconds)
{
	Super::PreUpdateAnimation(DeltaSeconds);
}
