
#include "SCBaseWeapon.h"

#include "SCCharacter.h"
#include "SCProjectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/AudioComponent.h"
#include "Net/UnrealNetwork.h"
#include "Core/SC_StaticLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Weapon/SC_BulletBase.h"
#include "Animation/AnimMontage.h"
#include "Weapon/SC_WeaponMagazineBase.h"
#include "Engine/ActorChannel.h"
#include "Weapon/SC_WeaponModuleBase.h"
#include "Weapon/SC_WeaponModuleBarrel.h"
#include "Components/SC_RecoilComponent.h"

// Sets default values
ASCBaseWeapon::ASCBaseWeapon()
{
	PrimaryActorTick.bCanEverTick = true;

	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("RootComp"));
	RootComponent = SceneComp;

	OutlineComp = CreateDefaultSubobject<USC_OutlineComponent>(TEXT("OutlineComp"));
	RecoilComp = CreateDefaultSubobject<USC_RecoilComponent>(TEXT("RecoilComp"));
	FireModeComp = CreateDefaultSubobject<USC_WeaponFireModeHandler>(TEXT("FireModeComp"));

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetupAttachment(SceneComp);
	WeaponMesh->SetCollisionProfileName("Weapon");

	FiredFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FiredFX"));
	FiredFX->SetupAttachment(WeaponMesh, SocketMuzzle);

	FiredFX->SetAutoActivate(false);

	bReplicates = true;
	NetUpdateFrequency = 100.f;

	if (Tags.Num() <= 0)
	{
		Tags.Add(TEXT("Outline"));
	}

}

void ASCBaseWeapon::OnRep_AttachmentReplication()
{
	// that bitch makes his children reattach, even when children not replicated and I don't want to reattach them

	if (PawnOwner)
	{
		ASCCharacter* MyChar = Cast<ASCCharacter>(PawnOwner);

		if (PawnOwner->IsLocallyControlled())
		{
			WeaponMesh->SetRelativeRotation(FRotator(0, 180, 90));
			MyChar->FPAnimInstance->Setup();
			bool bAlreadyAttached = GetRootComponent()->GetAttachParent() == MyChar->FirstPersonMesh;

			if (bAlreadyAttached)
			{
				return;
			}
		}
		else
		{
			MyChar->TPAnimInstance->Setup();
		}
	}

	Super::OnRep_AttachmentReplication();

}

bool ASCBaseWeapon::ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool bWroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);
	bWroteSomething |= Channel->ReplicateSubobject(CurrentLoadedMagazine, *Bunch, *RepFlags);
	return bWroteSomething;
}

void ASCBaseWeapon::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	UpdateInstalledModules();
	for (USC_WeaponModuleBase* Module : WeaponInstalledModules)
	{
		Module->SetupAttachment(WeaponMesh);
	}
}

void ASCBaseWeapon::SetOwner(AActor* NewOwner)
{
	PawnOwner = Cast<ASCCharacter>(NewOwner);
	Super::SetOwner(NewOwner);
}

void ASCBaseWeapon::ResetAttachToRoot()
{
	WeaponMesh->GetBodyInstance()->bUseCCD = false;
	WeaponMesh->SetSimulatePhysics(false);
	FAttachmentTransformRules AttachemtRules(EAttachmentRule::SnapToTarget, true);
	WeaponMesh->AttachToComponent(RootComponent, AttachemtRules);
}

void ASCBaseWeapon::OnRep_Owner()
{
	Super::OnRep_Owner();
	PawnOwner = Cast<ASCCharacter>(GetOwner());
}

void ASCBaseWeapon::SetNotOwned()
{
	SetOwner(nullptr);
}

void ASCBaseWeapon::UpdateInstalledModules()
{
	WeaponInstalledModules.Empty();
	GetComponents<USC_WeaponModuleBase>(WeaponInstalledModules, true);
	
	for (auto CurrentModule : WeaponInstalledModules)
	{
		if (CurrentModule->GetModuleType() == EWeaponModuleType::Barrel)
		{
			FAttachmentTransformRules Rules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, false);
			USC_WeaponModuleBarrel* Barrel = Cast<USC_WeaponModuleBarrel>(CurrentModule);
			
			if (FiredFX->GetAttachParent())
			{
				FDetachmentTransformRules DRules(EDetachmentRule::KeepRelative, true);
				FiredFX->DetachFromComponent(DRules);
			}
			
			FiredFX->AttachToComponent(CurrentModule, Rules, Barrel->GetSocketMuzzleName());

			break;
		}
	}

	// TODO get silencer muzzle socket override
}

FTransform ASCBaseWeapon::GetAimSocketTransform(ERelativeTransformSpace RTS)
{
	return WeaponMesh->GetSocketTransform("S_Aim", RTS);
}

bool ASCBaseWeapon::IsCanFire()
{
	bool CanFire = false;

	bool FireCooldownFinished = IsFireCoolDownFinished();
	bool HasLoadedBullet = IsValid(CurrentLoadedBullet);
	bool MagHasBullets = IsValid(CurrentLoadedMagazine);
	if (CurrentLoadedMagazine)
	{
		MagHasBullets = CurrentLoadedMagazine->GetLoadedBulletsCount() > 0;
	}

	CanFire = FireCooldownFinished && (HasLoadedBullet || MagHasBullets);

	return CanFire;
}

bool ASCBaseWeapon::IsFireCoolDownFinished()
{
	float FireInterval = (60.f / RPM);
	float CurTime = GetWorld()->GetUnpausedTimeSeconds();
	bool FireCooldownFinished = CurTime >= (LastShotTime + FireInterval);

	return FireCooldownFinished;
}

bool ASCBaseWeapon::L_TryFire()
{
	if (bIsReloading) return false;
	if (!IsCanFire())
	{
		if (IsFireCoolDownFinished())
		{
			S_EmptySoundRequest();
			LastShotTime = GetWorld()->GetUnpausedTimeSeconds();
		}

		return false;
	}

	bool bHasBullet = false;

	if (!CurrentLoadedBullet)
	{
		if (CurrentLoadedMagazine)
			bHasBullet = CurrentLoadedMagazine->CheckFirstBullet(CurrentLoadedBullet, PawnOwner->HasAuthority());
		else
			bHasBullet = false;
	}
	else
	{
		bHasBullet = true;
	}

	if (bHasBullet)
	{
		FVector MuzzleLoc;
		FRotator MuzzleRot;

		GetMuzzleSocket(MuzzleLoc, MuzzleRot);
		S_TryFire(MuzzleLoc, MuzzleRot);
		LastShotTime = GetWorld()->GetUnpausedTimeSeconds();

		PlayFireSound();
		
		if (!HasAuthority())
			PlayFireFX();

		if (!HasAuthority())
			CurrentLoadedBullet = nullptr;

		RecoilComp->FiredWeapon();
	}
	else
	{
		UKismetSystemLibrary::PrintString(GetWorld(), "No Bullets Left", true, true, FColor::Red, 1.f);
	}
	

	return bHasBullet;
}

void ASCBaseWeapon::S_TryFire_Implementation(FVector Loc, FRotator Rot)
{
	bool bHasBullet = false;

	if (!CurrentLoadedBullet)
	{
		if (CurrentLoadedMagazine)
			bHasBullet = CurrentLoadedMagazine->TakeFirstBullet(CurrentLoadedBullet);
		else
			bHasBullet = false;
	}
	else
	{
		bHasBullet = true;
	}

	if (bHasBullet)
	{
		FActorSpawnParameters SpawnParams;
		ASC_BulletBase* BulletDefaults = CurrentLoadedBullet->GetDefaultObject<ASC_BulletBase>();

		SpawnParams.Owner = this;
		SpawnParams.Instigator = PawnOwner;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		float BulletsPerShot = BulletDefaults->BulletsPreShot;
		float BulletSpread = BulletDefaults->RandomSpreadOnSpawn;

		for (int i = 0; i < BulletsPerShot; i++)
		{
			FRotator SpreadedRotation = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(Rot.Vector(), BulletSpread).Rotation();
			
			if (bSupportSuperBullet && GetWorld()->GetUnpausedTimeSeconds() > LastShotTime + SuperBulletCooldown)
				GetWorld()->SpawnActor(CurrentLoadedBullet, &Loc, &Rot, SpawnParams);
			else
				GetWorld()->SpawnActor(CurrentLoadedBullet, &Loc, &SpreadedRotation, SpawnParams);

			LastShotTime = GetWorld()->GetUnpausedTimeSeconds();
		}

		if (HasAuthority())
			PlayFireFX();

		M_TryFire();
		CurrentLoadedBullet = nullptr;
	}
	
}

void ASCBaseWeapon::M_TryFire_Implementation()
{
	if (PawnOwner && !PawnOwner->IsLocallyControlled())
	{
		PlayFireFX();
		PlayFireSound();
	}

	PlayFireAnim();
}

void ASCBaseWeapon::S_EmptySoundRequest_Implementation()
{
	M_EmptySoundRequest();
}

void ASCBaseWeapon::M_EmptySoundRequest_Implementation()
{	
	if (EmptySound)
	{
		UGameplayStatics::SpawnSoundAttached(EmptySound, WeaponMesh, SocketAim);
	}
	else
	{
		USC_StaticLibrary::Log(ELogType::Error, "ASCBaseWeapon::M_EmptySoundRequest_Implementation(): Weapon doesn't have empty shot sound!");
	}
}

void ASCBaseWeapon::OnRep_CurrentLoadedMagazine()
{
	if (CurrentLoadedMagazine)
	{
		FString NewMagName = CurrentLoadedMagazine->GetName();
		//UKismetSystemLibrary::PrintString(GetWorld(), "CurrentMagazineChanged: " + NewMagName, true, true, FColor::Purple, 10.f);

	}
}

void ASCBaseWeapon::OnRep_CurrentLoadedBullet()
{
	UKismetSystemLibrary::PrintString(GetWorld(), "CurrentBulletChanged", true, true, FColor::Red, 10.f);
}

void ASCBaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	LastShotTime = -(60.f / RPM);
}

bool ASCBaseWeapon::TryReloadWeapon(USC_WeaponMagazineBase* NewMagazine)
{
	if (bIsReloading) 
	{
		UKismetSystemLibrary::PrintString(this, "Already reloading", true, false, FColor::Red);
		return false;
	}

	S_Reload();
	bIsReloading = true;
	return true;
}

void ASCBaseWeapon::S_Reload_Implementation()
{
	bIsReloading = true;
	M_Reload();
	BindReloadTimer(true);
}

void ASCBaseWeapon::M_Reload_Implementation()
{
	WeaponReloaing.Broadcast(ThirdPersonReload, FirstPersonReload);
	if (PawnOwner->IsLocallyControlled())
	{
		WeaponMesh->GetAnimInstance()->Montage_Play(WeaponReload);
		BindReloadTimer(false);
	}
}

void ASCBaseWeapon::BindReloadTimer(bool bIsServer)
{
	float AnimTimeLen = FirstPersonReload->CalculateSequenceLength();
	FTimerHandle TH;
	FTimerDelegate TD;
	TD.BindLambda([&]() {bIsReloading = false; });
	GetWorld()->GetTimerManager().SetTimer(TH, TD, AnimTimeLen, false);

	if (bIsServer)
	{
		CurrentLoadedMagazine = NewObject<USC_WeaponMagazineBase>(this, TestMagazineType);
	}
}

USC_WeaponMagazineBase* ASCBaseWeapon::SetCurrentWeaponMagazine(USC_WeaponMagazineBase* NewMagazine)
{
	USC_WeaponMagazineBase* OldMagazine = CurrentLoadedMagazine;
	CurrentLoadedMagazine = NewMagazine;
	
	if (HasAuthority())
		OnRep_CurrentLoadedMagazine();

	return OldMagazine;
}

void ASCBaseWeapon::SetWeaponMeshRelativeRotation(FRotator Rot)
{
	WeaponMesh->SetRelativeRotation(Rot);
}

void ASCBaseWeapon::PlayFireSound()
{
	UGameplayStatics::SpawnSoundAttached(ShotSound, WeaponMesh, SocketMuzzle);
}

void ASCBaseWeapon::PlayFireFX()
{
	FiredFX->Template = ShotFX;
	FiredFX->Activate(true);
}

void ASCBaseWeapon::PlayFireAnim()
{
	ASCCharacter* Character = Cast<ASCCharacter>(PawnOwner);

	if (Character)
	{
		if (Character->Aiming)
			WeaponFired.Broadcast(ThirdPersonFireAim, FirstPersonFire);
		else
			WeaponFired.Broadcast(ThirdPersonFireNoAim, FirstPersonFire);

		WeaponMesh->GetAnimInstance()->Montage_Play(WeaponFire);
	}
}

void ASCBaseWeapon::GetMuzzleSocket(FVector& Loc, FRotator& Rot)
{
	USC_WeaponModuleBarrel* PossibleBarrel = nullptr;
	bool BarrelFound = false;
	bool BarrelSocketFound = false;
	
	for (int i = 0; i < WeaponInstalledModules.Num(); i++)
	{
		USC_WeaponModuleBase* CurMod = WeaponInstalledModules[i];
		if (WeaponInstalledModules[i]->GetModuleType() == EWeaponModuleType::Barrel)
		{
			PossibleBarrel = Cast<USC_WeaponModuleBarrel>(WeaponInstalledModules[i]);
			BarrelFound = true;
			break;
		}
	}

	if (BarrelFound)
		BarrelSocketFound = PossibleBarrel->GetSocketMuzzleLocationAndRotation(Loc, Rot);

	if (!BarrelSocketFound)
		WeaponMesh->GetSocketWorldLocationAndRotation(SocketMuzzle, Loc, Rot);
}

void ASCBaseWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME_CONDITION(ASCBaseWeapon, PawnOwner, COND_None);
	DOREPLIFETIME_CONDITION(ASCBaseWeapon, CurrentLoadedBullet, COND_None);
	DOREPLIFETIME_CONDITION(ASCBaseWeapon, CurrentLoadedMagazine, COND_None);
}