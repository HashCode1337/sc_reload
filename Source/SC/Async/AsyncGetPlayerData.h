// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "SC/Core/SC_StaticLibrary.h"
#include "SC/Service/SC_HttpRequest.h"
#include "SC/Service/SC_AnswerConverter.h"
#include "AsyncGetPlayerData.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDone, FPlayerCharacterData, PlayerCharacterData);

UCLASS()
class SC_API UAsyncGetPlayerData : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()
	
public:

	UPROPERTY()
	USC_HttpRequest* Request;

	UPROPERTY(BlueprintAssignable)
	FOnDone OnSuccess;
	UPROPERTY(BlueprintAssignable)
	FOnDone OnFails;

	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"))
	static UAsyncGetPlayerData* AsyncGetPlayerData(UObject* WorldContextObject, const FString& PlayerSession);

private:

	UPROPERTY()
	UAsyncGetPlayerData* AsyncNode;
	UPROPERTY()
	FString Session;

	void StartRequest();
	UFUNCTION()
	void HandleAnswer(FBackendAnswer Answer);

public:
	void Activate() override;

};
