// Fill out your copyright notice in the Description page of Project Settings.


#include "AsyncGetAccountData.h"
#include "../SubSystems/SC_GameConfigSubsystem.h"

UAsyncGetAccountData* UAsyncGetAccountData::AsyncGetAccountData(UObject* WorldContextObject, const FString& Session)
{
	UAsyncGetAccountData* AsyncNode = NewObject<UAsyncGetAccountData>(WorldContextObject);
	AsyncNode->PlayerSession = Session;
	AsyncNode->AsyncNode = AsyncNode;
	return AsyncNode;
}

void UAsyncGetAccountData::StartRequest()
{
	Request = NewObject<USC_HttpRequest>();
	FGameConfig GC;

	checkf(GetWorld(), TEXT("GetWorld"));
	checkf(GetWorld()->GetGameInstance(), TEXT("GI"));
	checkf(GetWorld()->GetGameInstance()->GetSubsystem<USC_GameConfigSubsystem>(), TEXT("Subsys"));

	USC_GameConfigSubsystem* GCS = GetWorld()->GetGameInstance()->GetSubsystem<USC_GameConfigSubsystem>();
	GC = GCS->GetGameConfig();

	Request->Answer.AddDynamic(this, &UAsyncGetAccountData::HandleAnswer);
	FString UrlReq = FString::Printf(TEXT("%s/get_account?session=%s"), *GC.Backend, *AsyncNode->PlayerSession);
	Request->MakeGet(UrlReq);
}

void UAsyncGetAccountData::HandleAnswer(FBackendAnswer Answer)
{
	FPlayerAccountData PlayerAccountData;
	if (Answer.Success)
	{
		if (USC_AnswerConverter::ConvertToPlayerAccountData(Answer, PlayerAccountData))
		{
			OnSuccess.Broadcast(PlayerAccountData);
		}
		else
		{
			OnFails.Broadcast(PlayerAccountData);
		}
		
	}
	else
	{
		OnFails.Broadcast(PlayerAccountData);
	}
}

void UAsyncGetAccountData::Activate()
{
	StartRequest();
}
