// Fill out your copyright notice in the Description page of Project Settings.


#include "AsyncCreatePlayerData.h"
#include "JsonObjectConverter.h"
#include "../SubSystems/SC_GameConfigSubsystem.h"

UAsyncCreatePlayerData* UAsyncCreatePlayerData::AsyncCreatePlayerCharacterData(UObject* WorldContextObject, const FString& PlayerSession, const FVector& Location, const FRotator& Rotation, uint8 Health, uint8 Food, uint8 Water, uint8 Radiation, uint8 Psy)
{
	UAsyncCreatePlayerData* AsyncNode = NewObject<UAsyncCreatePlayerData>(WorldContextObject);
	AsyncNode->Session = PlayerSession;
	AsyncNode->Location = Location;
	AsyncNode->Rotation = Rotation;
	AsyncNode->Health = Health;
	AsyncNode->Food = Food;
	AsyncNode->Water = Water;
	AsyncNode->Radiation = Radiation;
	AsyncNode->Psy = Psy;
	AsyncNode->AsyncNode = AsyncNode;

	return AsyncNode;
}

void UAsyncCreatePlayerData::StartRequest()
{
	USC_HttpRequest* Request = NewObject<USC_HttpRequest>();
	FGameConfig GC;
	Request->Answer.AddDynamic(this, &UAsyncCreatePlayerData::HandleAnswer);
	
	USC_GameConfigSubsystem* GCS = GetWorld()->GetGameInstance()->GetSubsystem<USC_GameConfigSubsystem>();
	GC = GCS->GetGameConfig();

	FString UrlReq = FString::Printf(TEXT("%s/create_player?session=%s"), *GC.Backend, *Session);

	// Prepare json content

	FPlayerCharacterData PlayerCharData;
	FString PostContent;

	PlayerCharData.database_id = -1; // will be assigned in database by "autoincrement"
	PlayerCharData.location = Location;
	PlayerCharData.rotation = Rotation;
	PlayerCharData.health = Health;
	PlayerCharData.food = Food;
	PlayerCharData.water = Water;
	PlayerCharData.radiation = Radiation;
	PlayerCharData.psy = Psy;

	FJsonObjectConverter::UStructToJsonObjectString(PlayerCharData, PostContent);

	// End prepare

	Request->MakePost(UrlReq, PostContent);
}

void UAsyncCreatePlayerData::HandleAnswer(FBackendAnswer Answer)
{
	FPlayerCharacterData PlayerCharacterData;

	if (Answer.Success)
	{
		USC_AnswerConverter::ConvertToPlayerCharacterData(Answer, PlayerCharacterData);
		OnSuccess.Broadcast(PlayerCharacterData);
	}
	else
	{
		OnFails.Broadcast(PlayerCharacterData);
	}
}

void UAsyncCreatePlayerData::Activate()
{
	StartRequest();
}
