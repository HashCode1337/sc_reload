// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "SC/Core/SC_StaticLibrary.h"
#include "SC/Service/SC_HttpRequest.h"
#include "SC/Core/SC_CommonDataModels.h"
#include "SC/Service/SC_AnswerConverter.h"
#include "AsyncGetAccountData.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGetAccountData, FPlayerAccountData, PlayerAccountData);

/**
 * 
 */
UCLASS()
class SC_API UAsyncGetAccountData : public UBlueprintAsyncActionBase
{
	UPROPERTY()
	UAsyncGetAccountData* AsyncNode;
	UPROPERTY()
	USC_HttpRequest* Request;
	UPROPERTY()
	FString PlayerSession;

	UPROPERTY(BlueprintAssignable)
	FOnGetAccountData OnSuccess;
	UPROPERTY(BlueprintAssignable)
	FOnGetAccountData OnFails;

	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"))
	static UAsyncGetAccountData* AsyncGetAccountData(UObject* WorldContextObject, const FString& Session);

	UFUNCTION()
	void StartRequest();
	UFUNCTION()
	void HandleAnswer(FBackendAnswer Answer);
	void Activate() override;

	GENERATED_BODY()
	
};
