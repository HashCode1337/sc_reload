// Fill out your copyright notice in the Description page of Project Settings.


#include "AsyncBackendRequest.h"
#include "../SubSystems/SC_GameConfigSubsystem.h"

UAsyncBackendRequest* UAsyncBackendRequest::AsyncBackendRequest(UObject* WorldContextObject, EHttpRequestType RequestType, const FString& Url, const FString& Content)
{
	UAsyncBackendRequest* AsyncNode = NewObject<UAsyncBackendRequest>(WorldContextObject);
	AsyncNode->ReqType = RequestType;
	AsyncNode->UrlPath = Url;
	AsyncNode->PostContent = Content;
	AsyncNode->AsyncNode = AsyncNode;

	return AsyncNode;
}

void UAsyncBackendRequest::StartRequest()
{
	Request = NewObject<USC_HttpRequest>();
	FGameConfig GC;

	Request->Answer.AddDynamic(this, &UAsyncBackendRequest::HandleAnswer);

	USC_GameConfigSubsystem* GCS = GetWorld()->GetGameInstance()->GetSubsystem<USC_GameConfigSubsystem>();
	GC = GCS->GetGameConfig();

	FString UrlReq = FString::Printf(TEXT("%s/%s"), *GC.Backend, *AsyncNode->UrlPath);

	if (AsyncNode->ReqType == EHttpRequestType::Get)
	{
		Request->MakeGet(UrlReq);
	}
	else if (AsyncNode->ReqType == EHttpRequestType::Post)
	{
		Request->MakePost(UrlReq, AsyncNode->PostContent);
	}

}

void UAsyncBackendRequest::HandleAnswer(FBackendAnswer Answer)
{
	if (Answer.Success)
	{
		OnSuccess.Broadcast(Answer);
	}
	else
	{
		OnFails.Broadcast(Answer);
	}
}

void UAsyncBackendRequest::Activate()
{
	Super::Activate();
	StartRequest();
}
