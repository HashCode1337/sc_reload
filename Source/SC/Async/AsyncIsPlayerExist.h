// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "SC/Core/SC_StaticLibrary.h"
#include "SC/Service/SC_HttpRequest.h"
#include "SC/Core/SC_CommonDataModels.h"
#include "AsyncIsPlayerExist.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerExist);

UCLASS()
class SC_API UAsyncIsPlayerExist : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

	UPROPERTY(BlueprintAssignable)
	FOnPlayerExist OnExist;
	UPROPERTY(BlueprintAssignable)
	FOnPlayerExist OnNotExist;

	UPROPERTY()
	UAsyncIsPlayerExist* AsyncNode;
	UPROPERTY()
	FString PlayerSession;

	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"))
	static UAsyncIsPlayerExist* AsyncIsPlayerExist(UObject* WorldContextObject, const FString& Session);

	void StartRequest();
	UFUNCTION()
	void HandleAnswer(FBackendAnswer Answer);

public:
	void Activate() override;
	
};
