// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "SC/Core/SC_StaticLibrary.h"
#include "SC/Service/SC_HttpRequest.h"
#include "SC/Core/SC_CommonDataModels.h"
#include "AsyncBackendRequest.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnMessage, FBackendAnswer, Answer);

UCLASS()
class SC_API UAsyncBackendRequest : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable)
	FOnMessage OnSuccess;
	UPROPERTY(BlueprintAssignable)
	FOnMessage OnFails;

	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"))
	static UAsyncBackendRequest* AsyncBackendRequest(UObject* WorldContextObject, EHttpRequestType RequestType, const FString& Url, const FString& Content);

private:

	UPROPERTY()
	UAsyncBackendRequest* AsyncNode;
	UPROPERTY()
	EHttpRequestType ReqType;
	UPROPERTY()
	FString UrlPath;
	UPROPERTY()
	FString PostContent;
	UPROPERTY()
	USC_HttpRequest* Request;

	void StartRequest();
	UFUNCTION()
	void HandleAnswer(FBackendAnswer Answer);

public:
	void Activate() override;
	
};
