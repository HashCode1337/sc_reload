// Fill out your copyright notice in the Description page of Project Settings.


#include "AsyncIsPlayerExist.h"
#include "../SubSystems/SC_GameConfigSubsystem.h"

UAsyncIsPlayerExist* UAsyncIsPlayerExist::AsyncIsPlayerExist(UObject* WorldContextObject, const FString& Session)
{
	UAsyncIsPlayerExist* AsyncNode = NewObject<UAsyncIsPlayerExist>(WorldContextObject);
	AsyncNode->PlayerSession = Session;
	AsyncNode->AsyncNode = AsyncNode;

	return AsyncNode;
}

void UAsyncIsPlayerExist::StartRequest()
{
	USC_HttpRequest* Request = NewObject<USC_HttpRequest>();
	FGameConfig GC;
	
	USC_GameConfigSubsystem* GCS = GetWorld()->GetGameInstance()->GetSubsystem<USC_GameConfigSubsystem>();
	GC = GCS->GetGameConfig();

	Request->Answer.AddDynamic(this, &UAsyncIsPlayerExist::HandleAnswer);

	FString UrlReq = FString::Printf(TEXT("%s/player_exist?session=%s"), *GC.Backend, *AsyncNode->PlayerSession);

	Request->MakeGet(UrlReq);
}

void UAsyncIsPlayerExist::HandleAnswer(FBackendAnswer Answer)
{
	if (Answer.Success)
	{
		OnExist.Broadcast();
	}
	else
	{
		OnNotExist.Broadcast();
	}
}

void UAsyncIsPlayerExist::Activate()
{
	StartRequest();
}
