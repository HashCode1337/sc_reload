// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "SC/Core/SC_StaticLibrary.h"
#include "SC/Service/SC_HttpRequest.h"
#include "SC/Service/SC_AnswerConverter.h"
#include "AsyncCreatePlayerData.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerCharacterDataCreated, FPlayerCharacterData, PlayerCharacterData);

UCLASS()
class SC_API UAsyncCreatePlayerData : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable)
	FOnPlayerCharacterDataCreated OnSuccess;
	UPROPERTY(BlueprintAssignable)
	FOnPlayerCharacterDataCreated OnFails;

	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"))
	static UAsyncCreatePlayerData* AsyncCreatePlayerCharacterData(UObject* WorldContextObject, const FString& PlayerSession, const FVector& Location, const FRotator& Rotation, uint8 Health, uint8 Food, uint8 Water, uint8 Radiation, uint8 Psy);

private:

	UPROPERTY()
	UAsyncCreatePlayerData* AsyncNode;
	UPROPERTY()
	FString Session;
	UPROPERTY()
	FVector Location;
	UPROPERTY()
	FRotator Rotation;
	UPROPERTY()
	uint8 Health;
	UPROPERTY()
	uint8 Food;
	UPROPERTY()
	uint8 Water;
	UPROPERTY()
	uint8 Radiation;
	UPROPERTY()
	uint8 Psy;

	void StartRequest();
	UFUNCTION()
	void HandleAnswer(FBackendAnswer Answer);

public:
	void Activate() override;
	
};
