// Fill out your copyright notice in the Description page of Project Settings.


#include "AsyncGetPlayerData.h"
#include "../SubSystems/SC_GameConfigSubsystem.h"

UAsyncGetPlayerData* UAsyncGetPlayerData::AsyncGetPlayerData(UObject* WorldContextObject, const FString& PlayerSession)
{
	UAsyncGetPlayerData* AsyncNode = NewObject<UAsyncGetPlayerData>(WorldContextObject);
	AsyncNode->Session = PlayerSession;
	AsyncNode->AsyncNode = AsyncNode;

	return AsyncNode;
}

void UAsyncGetPlayerData::StartRequest()
{
	Request = NewObject<USC_HttpRequest>();
	FGameConfig GC;
	Request->Answer.AddDynamic(this, &UAsyncGetPlayerData::HandleAnswer);
	
	USC_GameConfigSubsystem* GCS = GetWorld()->GetGameInstance()->GetSubsystem<USC_GameConfigSubsystem>();
	GC = GCS->GetGameConfig();

	FString UrlReq = FString::Printf(TEXT("%s/get_player?session=%s"), *GC.Backend, *Session);
	Request->MakeGet(UrlReq);
}

void UAsyncGetPlayerData::HandleAnswer(FBackendAnswer Answer)
{
	FPlayerCharacterData PlayerCharacterData;
	
	if (Answer.Success)
	{
		USC_AnswerConverter::ConvertToPlayerCharacterData(Answer, PlayerCharacterData);
		OnSuccess.Broadcast(PlayerCharacterData);
	}
	else
	{
		OnFails.Broadcast(PlayerCharacterData);
	}
}

void UAsyncGetPlayerData::Activate()
{
	StartRequest();
}
