// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_CharWeaponHandler.h"
#include "../SCBaseWeapon.h"
#include "../SCCharacter.h"
#include "../Core/SC_StaticLibrary.h"
#include "Kismet/KismetSystemLibrary.h"

USC_CharWeaponHandler::USC_CharWeaponHandler()
{
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
}

void USC_CharWeaponHandler::BeginPlay()
{
	Super::BeginPlay();


}

void USC_CharWeaponHandler::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void USC_CharWeaponHandler::S_DropCurrentWeaponRequest_Implementation()
{
	if (!CurrentWeapon)
	{
		USC_StaticLibrary::Log(ELogType::Warn, "USC_CharWeaponHandler::S_DropCurrentWeaponRequest: Can't drop weapon because it is null!");
		return;
	}

	FDetachmentTransformRules DetachRules(EDetachmentRule::KeepRelative, true);
	CurrentWeapon->DetachFromActor(DetachRules);
	CurrentWeapon->WeaponMesh->GetBodyInstance()->bUseCCD = true;
	CurrentWeapon->WeaponMesh->SetSimulatePhysics(true);

	CurrentWeapon->SetOwner(nullptr);
	CurrentWeapon = nullptr;
	OnRep_CurrentWeapon();
}

void USC_CharWeaponHandler::S_TakeWeaponRequest_Implementation(ASCBaseWeapon* NewWeapon)
{
	if (NewWeapon == CurrentWeapon) return;

	if (CurrentWeapon)
	{
		S_DropCurrentWeaponRequest();
	}

	CurrentWeapon = NewWeapon;
	NewWeapon->ResetAttachToRoot();
	OnRep_CurrentWeapon();
	CurrentWeapon->SetOwner(GetCharOwner());

	M_AttachWeaponToHands(CurrentWeapon);

}

void USC_CharWeaponHandler::M_AttachWeaponToHands_Implementation(ASCBaseWeapon* NewWeapon)
{
	FAttachmentTransformRules AttachRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, false);

	if (GetCharOwner()->IsLocallyControlled())
	{
		NewWeapon->AttachToComponent(GetCharOwner()->FirstPersonMesh, AttachRules, TEXT("S_Weapon"));
		NewWeapon->GetWeaponMesh()->SetRelativeRotation(FRotator(0, 180, 90));

		GetCharOwner()->S_SetAiming(false);
		GetCharOwner()->S_SetArmed(ECharacterWeaponState::Raised);

		FVector NewFPMeshOffset = NewWeapon->FPMeshOffset;
		GetCharOwner()->FirstPersonMesh->SetRelativeLocation(NewFPMeshOffset);
		GetCharOwner()->FPAnimInstance->Setup();
	}
	else
	{
		NewWeapon->SetWeaponMeshRelativeRotation(FRotator::ZeroRotator);
		NewWeapon->AttachToComponent(GetCharOwner()->ThirdPersonMesh, AttachRules, TEXT("S_Weapon"));
		GetCharOwner()->TPAnimInstance->Setup();
	}
}

void USC_CharWeaponHandler::DestroyCurrentWeapon()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
	}
}

void USC_CharWeaponHandler::OnRep_CurrentWeapon()
{
	if (GetCharOwner()->FPAnimInstance && GetCharOwner()->TPAnimInstance)
	{
		GetCharOwner()->FPAnimInstance->Setup();
		GetCharOwner()->TPAnimInstance->Setup();
	}
}

ASCCharacter* USC_CharWeaponHandler::GetCharOwner()
{
	return Cast<ASCCharacter>(GetOwner());
}

void USC_CharWeaponHandler::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(USC_CharWeaponHandler, CurrentWeapon);
}