// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_WeaponFireModeHandler.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../SCBaseWeapon.h"
#include "Kismet/GameplayStatics.h"
#include "../Core/SC_StaticLibrary.h"

USC_WeaponFireModeHandler::USC_WeaponFireModeHandler()
{
	PrimaryComponentTick.bCanEverTick = true;

	AllowedFireModes.Empty();
	AllowedFireModes.Add(EWeaponFireMode::Safe);
	AllowedFireModes.Add(EWeaponFireMode::Single);

	SetIsReplicatedByDefault(true);

}

void USC_WeaponFireModeHandler::FireStart()
{
	switch (CurrentFireMode)
	{
	case EWeaponFireMode::Safe:
		break;
	case EWeaponFireMode::Single:
		GetWeapon()->L_TryFire();
		break;
	case EWeaponFireMode::Burst:
	{
		float RPM = GetWeapon()->RPM;
		float FireRate = (60.f / RPM);
		int32 BurstIterations = GetWeapon()->BurstBullets;
		float Interval = (FireRate * BurstIterations);
		AddFiringUntil(Interval);
		break;
	}
	case EWeaponFireMode::Auto:
		AddFiringUntil(999.f);
		break;
	case EWeaponFireMode::Douplet:
	{
		float RPM = GetWeapon()->RPM;
		float FireRate = (60.f / RPM);
		float Interval = (FireRate * 2);
		AddFiringUntil(Interval);
		break;
	}
	default:
		break;
	}
}

void USC_WeaponFireModeHandler::FireEnd()
{
	switch (CurrentFireMode)
	{
	case EWeaponFireMode::Safe:
		GetWeapon()->S_EmptySoundRequest();
		break;
	case EWeaponFireMode::Single:
		break;
	case EWeaponFireMode::Burst:
		break;
	case EWeaponFireMode::Auto:
		ResetFireUntil();
		break;
	case EWeaponFireMode::Douplet:
		break;
	default:
		break;
	}
}

EWeaponFireMode USC_WeaponFireModeHandler::SwitchMode()
{
	int32 CurModeIndex = AllowedFireModes.Find(CurrentFireMode);
	bool SwitchToFirst = !AllowedFireModes.IsValidIndex(CurModeIndex + 1);

	if (SwitchToFirst)
	{
		CurrentFireMode = AllowedFireModes[0];
	}
	else
	{
		CurrentFireMode = AllowedFireModes[CurModeIndex + 1];
	}
	
	UKismetSystemLibrary::PrintString(GetWorld(), "New Mode: " + FString::FromInt((int32)CurrentFireMode));

	S_SwitchSoundRequest();

	return CurrentFireMode;
}

bool USC_WeaponFireModeHandler::GetFiring()
{
	return GetWorld()->UnpausedTimeSeconds <= FireUntil;
}

void USC_WeaponFireModeHandler::AddFiringUntil(float AddInterval)
{
	FireUntil = GetWorld()->UnpausedTimeSeconds + AddInterval;
}

void USC_WeaponFireModeHandler::ResetFireUntil()
{
	FireUntil = 0.f;
}

void USC_WeaponFireModeHandler::S_SwitchSoundRequest_Implementation()
{
	M_SwitchSoundRequest();
}

void USC_WeaponFireModeHandler::M_SwitchSoundRequest_Implementation()
{
	ASCBaseWeapon* MyWeapon = GetWeapon();
	if (MyWeapon)
	{
		USoundBase* SwitchSound = MyWeapon->SwitchSound;

		if (SwitchSound)
		{
			UGameplayStatics::SpawnSoundAttached(SwitchSound, MyWeapon->WeaponMesh, MyWeapon->SocketAim);
		}
		else
		{
			USC_StaticLibrary::Log(ELogType::Error, "USC_WeaponFireModeHandler::M_SwitchSoundRequest_Implementation(): Weapon doesn't have switch sound!");
		}
	}
}

void USC_WeaponFireModeHandler::BeginPlay()
{
	Super::BeginPlay();
}

ASCBaseWeapon* USC_WeaponFireModeHandler::GetWeapon()
{
	ASCBaseWeapon* MyWeapon = Cast<ASCBaseWeapon>(GetOwner());
	return MyWeapon;
}

void USC_WeaponFireModeHandler::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetFiring())
	{
		GetWeapon()->L_TryFire();
	}

}

