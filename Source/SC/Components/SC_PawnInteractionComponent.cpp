// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_PawnInteractionComponent.h"
#include "Net/UnrealNetwork.h"

USC_PawnInteractionComponent::USC_PawnInteractionComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
}


void USC_PawnInteractionComponent::BeginPlay()
{
	Super::BeginPlay();

	
}


void USC_PawnInteractionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

