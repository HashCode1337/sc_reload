// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_HealthComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/DamageType.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/ActorComponent.h"
#include "../Interfaces/SC_Damagable.h"
#include "../Core/SC_CommonDataModels.h"

void USC_HealthComponent::OnRep_CurrentHealth()
{
	FString NewHealth = FString::FromInt(CurrentHealth);

	//UKismetSystemLibrary::PrintString(GetOwner(), "HealthReplicated: " + NewHealth, true, false, FColor::Yellow);
}

float USC_HealthComponent::GetHealthUncompressed()
{
	return ((float)CurrentHealth / 255) * MaxHealth;
}

uint8 USC_HealthComponent::CompressHealth(float NewHealth)
{
	return ((float)NewHealth / MaxHealth) * 255;
}

void USC_HealthComponent::AddDamage_Implementation(
	AActor* DamagedActor,
	float Damage,
	class AController* InstigatedBy,
	FVector HitLocation,
	class UPrimitiveComponent* FHitComponent,
	FName BoneName,
	FVector ShotFromDirection,
	TSubclassOf<UDamageType> DamageType,
	AActor* DamageCauser
)
{
	float Health = GetHealthUncompressed();
	float NewHealth = UKismetMathLibrary::Clamp(Health - Damage, 0.f, MaxHealth);
	
	CurrentHealth = CompressHealth(NewHealth);
	OnRep_CurrentHealth();

	bool bNewIsAlive = NewHealth > 0;
	bool bOldAlive = bAlive;
	
	FDamagedTenParams DamageDetails;
	DamageDetails.DamagedActor = DamagedActor;
	DamageDetails.Damage = Damage;
	DamageDetails.InstigatedBy = InstigatedBy;
	DamageDetails.HitLocation = HitLocation;
	DamageDetails.FHitComponent = FHitComponent;
	DamageDetails.BoneName = BoneName;
	DamageDetails.ShotFromDirection = ShotFromDirection;
	DamageDetails.DamageType = DamageType;
	DamageDetails.DamageCauser = DamageCauser;
	DamageDetails.bIsAlive = bNewIsAlive;
	bAlive = bNewIsAlive;

	// if it already dead
	if (bOldAlive)
	{
	
		// Native implementation
		if (!bNewIsAlive)
		{
			DOnDeath.Broadcast(DamageDetails);
			DOnDamaged.Broadcast(DamageDetails);
			OnDeath(DamagedActor, Damage, InstigatedBy, HitLocation, FHitComponent, BoneName, ShotFromDirection, DamageType, DamageCauser, bNewIsAlive);
			OnDamaged(DamagedActor, Damage, InstigatedBy, HitLocation, FHitComponent, BoneName, ShotFromDirection, DamageType, DamageCauser, bNewIsAlive);
		}
		else
		{
			DOnDamaged.Broadcast(DamageDetails);
			OnDamaged(DamagedActor, Damage, InstigatedBy, HitLocation, FHitComponent, BoneName, ShotFromDirection, DamageType, DamageCauser, bNewIsAlive);
		}

		// Repeat to interfaces
		ISC_Damagable* Victim = Cast<ISC_Damagable>(DamagedActor);
		if (!Victim) return;
		if (!bNewIsAlive)		Victim->Die(DamageDetails);
		else					Victim->Damaged(DamageDetails);
	}
	else
	{
		// it is already dead
	}
}

void USC_HealthComponent::SetHealth_Implementation(float NewHealth)
{
	CurrentHealth = CompressHealth(NewHealth);
}

void USC_HealthComponent::SetInvulnurable_Implementation(bool bNewInvulnurable)
{
	bInvulnurable = bNewInvulnurable;
}

void USC_HealthComponent::OnDamagedInternal
(
	AActor* DamagedActor,
	float Damage,
	class AController* InstigatedBy,
	FVector HitLocation,
	class UPrimitiveComponent* FHitComponent,
	FName BoneName,
	FVector ShotFromDirection,
	const class UDamageType* DamageType,
	AActor* DamageCauser
)
{
	UAISense_Damage::ReportDamageEvent(this, DamagedActor, DamageCauser, Damage, DamageCauser->GetActorLocation(), HitLocation, USC_CommonDataModels::Tag_Damage);

	if (!bInvulnurable)
	{
		AddDamage(DamagedActor, Damage, InstigatedBy, HitLocation, FHitComponent, BoneName, ShotFromDirection, DamageType->StaticClass(), DamageCauser);
	}
	else
	{
		AddDamage(DamagedActor, 0.f, InstigatedBy, HitLocation, FHitComponent, BoneName, ShotFromDirection, DamageType->StaticClass(), DamageCauser);
	}
	
}

USC_HealthComponent::USC_HealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
}

void USC_HealthComponent::BeginPlay()
{
	if (GetOwner() && GetOwner()->HasAuthority())
	{
		GetOwner()->OnTakePointDamage.AddDynamic(this, &USC_HealthComponent::OnDamagedInternal);
	}

	Super::BeginPlay();	
}

void USC_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void USC_HealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME_CONDITION(USC_HealthComponent, CurrentHealth, COND_None);
}
