// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_CharacterInteractionComponent.h"

USC_CharacterInteractionComponent::USC_CharacterInteractionComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
}

void USC_CharacterInteractionComponent::PostInitProperties()
{
	Super::PostInitProperties();
	MyCharacter = Cast<ASCCharacter>(GetOwner());

}

void USC_CharacterInteractionComponent::BeginPlay()
{
	Super::BeginPlay();
}

void USC_CharacterInteractionComponent::RequestInteract(ISC_Interactable* InteractSubject)
{
	Super::RequestInteract(InteractSubject);

	if (InteractSubject)
	{
		switch (InteractSubject->GetInteractType())
		{
		case EInteractableObjectType::Weapon: MyCharacter->CharWeaponHandler->S_TakeWeaponRequest(Cast<ASCBaseWeapon>(InteractSubject)); break;
		}
	}

	//InteractSubject->Pickup(MyCharacter);
}

void USC_CharacterInteractionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}
