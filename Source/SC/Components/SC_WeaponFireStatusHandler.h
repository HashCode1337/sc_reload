// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../SCCharacter.h"
#include "SC_WeaponFireStatusHandler.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class SC_API USC_WeaponFireStatusHandler : public UObject
{
	GENERATED_BODY()

public:

	//ToDo RENAME ME PLEASE!!!

	USC_WeaponFireStatusHandler(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintReadOnly)
	ASCCharacter* CharOwner;

	UFUNCTION(BlueprintPure)
	bool IsCharacterCanAim();

	UFUNCTION(BlueprintPure)
	bool IsCharacterCanFire();

	UFUNCTION(BlueprintPure)
	bool IsCharacterCanSprint();

	UFUNCTION(BlueprintPure)
	bool IsCharacterCanLowerWeapon();

	UFUNCTION(BlueprintPure)
	bool IsCharacterCanRaiseWeapon();
};
