// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SC_RecoilComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SC_API USC_RecoilComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	USC_RecoilComponent();

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float XRecoilStrength = 0.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float XRecoilLimit = 3.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float YRecoilStrength = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float YRecoilLimit = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float IncreaseSpeed = 10.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float FalloffSpeed = 10.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<UCameraShakeBase> CameraShake;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float ShakeStrength = 1.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float XControllerShakeStrength = 1.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float YControllerShakeStrength = 1.f;



	// ---------------------------------------- //

	UPROPERTY(BlueprintReadWrite)
	class ASCBaseWeapon* Weapon = nullptr;

	UPROPERTY(BlueprintReadWrite)
	float XAccumulated = 0.f;
	float XTemp = 0.f;

	UPROPERTY(BlueprintReadWrite)
	float YAccumulated = 0.f;
	float YTemp = 0.f;

	float FiredTime = 0.f;
	float DecreaseAfter = 0.1f;

protected:
	
	virtual void BeginPlay() override;

public:

	virtual void FiredWeapon();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
