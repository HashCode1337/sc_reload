// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_RecoilComponent.h"
#include "../SCBaseWeapon.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../Core/SC_PlayerController.h"

USC_RecoilComponent::USC_RecoilComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bTickEvenWhenPaused = false;
}

void USC_RecoilComponent::BeginPlay()
{
	Super::BeginPlay();
	Weapon = Cast<ASCBaseWeapon>(GetOwner());
}

void USC_RecoilComponent::FiredWeapon()
{
	if (!Weapon) return;
	if (!Weapon->PawnOwner->IsLocallyControlled()) return;

	FiredTime = GetWorld()->UnpausedTimeSeconds;
	
	if (FMath::Abs(XTemp) <= XRecoilLimit)
		XTemp += UKismetMathLibrary::RandomFloatInRange(0.f, XRecoilStrength);
	

	if (FMath::Abs(YTemp) <= YRecoilLimit)
		YTemp += UKismetMathLibrary::RandomFloatInRange(-YRecoilStrength, YRecoilStrength);
	
	ASC_PlayerController* MyController = Cast<ASC_PlayerController>(Weapon->PawnOwner->GetController());
	MyController->PlayerCameraManager->StartCameraShake(CameraShake, 1.f);
	
	float RandomUp = UKismetMathLibrary::RandomFloatInRange(0.f, -XRecoilStrength) * XControllerShakeStrength;
	float RandomRight = UKismetMathLibrary::RandomFloatInRange(-YRecoilStrength, YRecoilStrength) * YControllerShakeStrength;

	MyController->AddPitchInput( RandomUp );
	MyController->AddYawInput( RandomRight );
	
}

void USC_RecoilComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (FiredTime + DecreaseAfter > GetWorld()->UnpausedTimeSeconds)
	{
		XAccumulated = UKismetMathLibrary::FInterpTo(XAccumulated, XTemp, DeltaTime, IncreaseSpeed);
		YAccumulated = UKismetMathLibrary::FInterpTo(YAccumulated, YTemp, DeltaTime, IncreaseSpeed);
	}
	else
	{
		XTemp = UKismetMathLibrary::FInterpTo(XAccumulated, 0.f, DeltaTime, FalloffSpeed);
		YTemp = UKismetMathLibrary::FInterpTo(XAccumulated, 0.f, DeltaTime, FalloffSpeed);

		XAccumulated = UKismetMathLibrary::FInterpTo(XAccumulated, XTemp, DeltaTime, FalloffSpeed);
		YAccumulated = UKismetMathLibrary::FInterpTo(YAccumulated, YTemp, DeltaTime, FalloffSpeed);
	}

	// Debug
	/*UKismetSystemLibrary::PrintString(GetWorld(), "X - " + FString::SanitizeFloat(XAccumulated), true, false, FColor::Red, 0.f);
	UKismetSystemLibrary::PrintString(GetWorld(), "Y - " + FString::SanitizeFloat(YAccumulated), true, false, FColor::Red, 0.f);
	
	if (FiredTime + DecreaseAfter > GetWorld()->UnpausedTimeSeconds)
	{
		UKismetSystemLibrary::PrintString(GetWorld(), "true", true, false, FColor::Red, 0.f);
	}
	else
	{
		UKismetSystemLibrary::PrintString(GetWorld(), "false", true, false, FColor::Red, 0.f);
	}*/

}

