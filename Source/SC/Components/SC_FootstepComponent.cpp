#include "SC_FootstepComponent.h"
#include "GameFramework/Character.h"
#include "../Core/SC_StaticLibrary.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Sound/SoundBase.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"

USC_FootstepComponent::USC_FootstepComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bAllowTickOnDedicatedServer = false;

	StepSoundFrequencies.Empty();

	StepSoundFrequencies.Add(EMovementSpeed::Walk, 0.6f);
	StepSoundFrequencies.Add(EMovementSpeed::Run, 0.37f);
	StepSoundFrequencies.Add(EMovementSpeed::Sprint, 0.3f);

}


void USC_FootstepComponent::PlayFootstepSound(bool FirstPerson)
{
	ACharacter* CharOwner = Cast<ACharacter>(GetOwner());
	if (!CharOwner) return;

	bool bIsLocal = CharOwner->IsLocallyControlled();

	if (FirstPerson)
	{
		if (!bIsLocal)
		{
			return;
		}
	}
	else if (!FirstPerson)
	{
		if (bIsLocal)
		{
			return;
		}
	}

	FVector PlayerCapsuleLocation = CharOwner->GetCapsuleComponent()->GetComponentLocation();
	float PlayerCapsuleHalfHeight = CharOwner->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	PlayerCapsuleLocation -= FVector(0.f, 0.f, PlayerCapsuleHalfHeight);

	FVector StartTrace = PlayerCapsuleLocation + (CharOwner->GetActorUpVector() * 1.f);
	FVector EndTrace = CharOwner->GetCapsuleComponent()->GetComponentLocation() + (CharOwner->GetActorUpVector() * -PlayerCapsuleHalfHeight * 1.1f);

	FHitResult HitResult;
	FCollisionQueryParams QP;
	QP.AddIgnoredActor(CharOwner);
	QP.bReturnFaceIndex = true;
	QP.bReturnPhysicalMaterial = true;
	QP.bTraceComplex = true;

	bool bTraceSuccess = USC_StaticLibrary::CapsuleTraceForPhysicalSurface(CharOwner, StartTrace, EndTrace, QP, "Surface", HitResult);
		
	if (!bTraceSuccess) return;

	EPhysicalSurface PhysSurface = HitResult.PhysMaterial->SurfaceType;
	float MovementSpeed = CharOwner->GetVelocity().Size2D();
	float Loudness = SpeedLoudnessCurve->GetFloatValue(MovementSpeed);

	if (Sounds.Contains(PhysSurface))
	{
		USoundBase* Sound = Sounds[PhysSurface];
		UGameplayStatics::SpawnSoundAttached(Sound, CharOwner->GetRootComponent(), NAME_None, FVector::ZeroVector, EAttachLocation::SnapToTarget, false, Loudness);
		UAISense_Hearing::ReportNoiseEvent(this, CharOwner->GetActorLocation(), Loudness, CharOwner, 2000.f, USC_CommonDataModels::Tag_Footstep);
	}
	else
	{
		UKismetSystemLibrary::PrintString(GetWorld(), "Footstep surface sound not found: " + FString::FromInt((int32)PhysSurface));
	}
}

void USC_FootstepComponent::SetCurrentMovementMode(EMovementSpeed NewMovementMode)
{
	CurrentMovementMode = NewMovementMode;
}

void USC_FootstepComponent::BeginPlay()
{
	Super::BeginPlay();
}


void USC_FootstepComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	ACharacter* CharOwner = Cast<ACharacter>(GetOwner());
	if (!CharOwner) return;
	if (!CharOwner->IsLocallyControlled()) return;
	if (CharOwner->GetVelocity().Size2D() < 1.f) return;

	float CurrentInterval = StepSoundFrequencies[CurrentMovementMode];

	if (GetWorld()->UnpausedTimeSeconds > LastStepSoundTime + CurrentInterval)
	{
		LastStepSoundTime = GetWorld()->UnpausedTimeSeconds;
		PlayFootstepSound(true);
	}

}

