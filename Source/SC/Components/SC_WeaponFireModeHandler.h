// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../Core/SC_CommonDataModels.h"
#include "SC_WeaponFireModeHandler.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SC_API USC_WeaponFireModeHandler : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TArray<EWeaponFireMode> AllowedFireModes;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	EWeaponFireMode CurrentFireMode = EWeaponFireMode::Single;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float FireUntil = 0.f;

public:	
	USC_WeaponFireModeHandler();
	void FireStart();
	void FireEnd();

	UFUNCTION(BlueprintCallable)
	EWeaponFireMode SwitchMode();
	
private:
	bool GetFiring();
	void AddFiringUntil(float AddInterval);
	void ResetFireUntil();

	UFUNCTION(Server, Unreliable)
	void S_SwitchSoundRequest();
	UFUNCTION(NetMulticast, Unreliable)
	void M_SwitchSoundRequest();

protected:
	virtual void BeginPlay() override;
	class ASCBaseWeapon* GetWeapon();

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
