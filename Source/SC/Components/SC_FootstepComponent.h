// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Sound/SoundCue.h"
#include "../Core/SC_CommonDataModels.h"
#include "SC_FootstepComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SC_API USC_FootstepComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	USC_FootstepComponent();

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	EMovementSpeed CurrentMovementMode = EMovementSpeed::Run;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TMap<EMovementSpeed, float> StepSoundFrequencies;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TMap<TEnumAsByte<EPhysicalSurface>, USoundCue*> Sounds;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UCurveFloat* SpeedLoudnessCurve;

private:

	float LastStepSoundTime = 0.f;

public:

	UFUNCTION(BlueprintCallable)
	void PlayFootstepSound(bool FirstPerson);
	UFUNCTION(BlueprintCallable)
	void SetCurrentMovementMode(EMovementSpeed NewMovementMode);

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
