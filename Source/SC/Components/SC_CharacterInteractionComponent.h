// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SC_PawnInteractionComponent.h"
#include "../SCCharacter.h"
#include "SC_CharacterInteractionComponent.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class SC_API USC_CharacterInteractionComponent : public USC_PawnInteractionComponent
{
	GENERATED_BODY()

private:

	UPROPERTY()
	ASCCharacter* MyCharacter;


public:
	USC_CharacterInteractionComponent();


	void PostInitProperties() override;

protected:
	virtual void BeginPlay() override;
	virtual void RequestInteract(ISC_Interactable* InteractSubject) override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
};
