// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Net/UnrealNetwork.h"
#include "SC_HealthComponent.generated.h"

class UDamageType;

USTRUCT(BlueprintType)
struct FDamagedTenParams
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite)
	AActor* DamagedActor;
	UPROPERTY(BlueprintReadWrite)
	float Damage;
	UPROPERTY(BlueprintReadWrite)
	class AController* InstigatedBy;
	UPROPERTY(BlueprintReadWrite)
	FVector HitLocation;
	UPROPERTY(BlueprintReadWrite)
	class UPrimitiveComponent* FHitComponent;
	UPROPERTY(BlueprintReadWrite)
	FName BoneName;
	UPROPERTY(BlueprintReadWrite)
	FVector ShotFromDirection;
	UPROPERTY(BlueprintReadWrite)
	TSubclassOf<UDamageType> DamageType;
	UPROPERTY(BlueprintReadWrite)
	AActor* DamageCauser;
	UPROPERTY(BlueprintReadWrite)
	bool bIsAlive;
	
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDamagedSignature, const FDamagedTenParams&, DamageDetails);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDeathSignature, const FDamagedTenParams&, DamageDetails);

UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SC_API USC_HealthComponent : public UActorComponent
{
	GENERATED_BODY()

	
public:

	UPROPERTY(BlueprintReadWrite, BlueprintAssignable, Category = "SC")
	FDamagedSignature DOnDamaged;
	UPROPERTY(BlueprintReadWrite, BlueprintAssignable, Category = "SC")
	FDamagedSignature DOnDeath;

protected:

	// 0 = death; 255 = full healthy; 
	UPROPERTY(ReplicatedUsing = OnRep_CurrentHealth, BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	uint8 CurrentHealth = 255;
	UFUNCTION()
	void OnRep_CurrentHealth();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	float MaxHealth = 100.f;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	bool bAlive = true;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	bool bInvulnurable = false;

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void OnDamaged(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, TSubclassOf<UDamageType> DamageType, AActor* DamageCauser, bool bIsAlive);

	UFUNCTION(BlueprintImplementableEvent)
	void OnDeath(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, TSubclassOf<UDamageType> DamageType, AActor* DamageCauser, bool bIsAlive);

public:

	UFUNCTION(BlueprintPure)
	float GetHealthUncompressed();

	UFUNCTION(BlueprintPure)
	uint8 CompressHealth(float NewHealth);

	UFUNCTION(BlueprintPure)
	bool IsAlive() const { return bAlive; };

	UFUNCTION(Server, Unreliable, BlueprintCallable, BlueprintAuthorityOnly)
	void AddDamage(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, TSubclassOf<UDamageType> DamageType, AActor* DamageCauser);

	UFUNCTION(Server, Unreliable, BlueprintCallable, BlueprintAuthorityOnly)
	void SetHealth(float NewHealth);

	UFUNCTION(Server, Unreliable, BlueprintCallable, BlueprintAuthorityOnly)
	void SetInvulnurable(bool bNewInvulnurable);

private:

	UFUNCTION()
	void OnDamagedInternal
	(
		AActor* DamagedActor,
		float Damage,
		class AController* InstigatedBy,
		FVector HitLocation,
		class UPrimitiveComponent* FHitComponent,
		FName BoneName,
		FVector ShotFromDirection,
		const class UDamageType* DamageType,
		AActor* DamageCauser
	);

public:	
	// Sets default values for this component's properties
	USC_HealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
public:
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

};
