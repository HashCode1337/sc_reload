// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../Interfaces/SC_Interactable.h"
#include "SC_PawnInteractionComponent.generated.h"


UCLASS( ClassGroup=(Custom), Abstract )
class SC_API USC_PawnInteractionComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	USC_PawnInteractionComponent();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void RequestInteract(ISC_Interactable* InteractSubject) {};
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
