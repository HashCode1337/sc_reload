// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/PostProcessComponent.h"
#include "Components/SceneComponent.h"
#include "Engine/Scene.h"
#include "SC_OutlineComponent.generated.h"

/**
 * 
 */

// Also need to implement color in hlsl node inside of outline pp material
UENUM(BlueprintType)
enum class EOutlineColor : uint8
{
	White = 0 UMETA(DisplayName = "White"),
	Red = 1 UMETA(DisplayName = "Red"),
	Green = 2 UMETA(DisplayName = "Green"),
	Blue = 3 UMETA(DisplayName = "Blue")
};

UCLASS(BlueprintType, Blueprintable)
class SC_API USC_OutlineComponent : public UPostProcessComponent
{
	GENERATED_BODY()


	UPROPERTY()
	UMaterialInstanceDynamic* DynamicOutlineMat;

	UPROPERTY()
	int Stencil;

	UPROPERTY();
	bool bIsEnabled = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	bool EnableOnStart = false;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FName OutlineTag = FName(TEXT("Outline"));

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UMaterialInterface* OutlineMaterialInst;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	EOutlineColor OutlineColor;



	
protected:
	void OnRegister();
	void OnUnregister();
	void Serialize(FArchive& Ar);

public:
	void BeginPlay() override;

public:
	void UpdateStencilValue();
	void UpdateOutlineStatus();
	
	UFUNCTION(BlueprintCallable)
	void SetOutlineEnabled(bool bIsCompEnabled);
	UFUNCTION(BlueprintCallable)
	bool GetOutlineEnabled();

};
