// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SC_CharWeaponHandler.generated.h"

class ASCCharacter;
class ASCBaseWeapon;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SC_API USC_CharWeaponHandler : public UActorComponent
{
	GENERATED_BODY()

public:	
	USC_CharWeaponHandler();

public:

	UPROPERTY(ReplicatedUsing = OnRep_CurrentWeapon, VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
	ASCBaseWeapon* CurrentWeapon;

public:

	UFUNCTION(Server, Unreliable, BlueprintCallable)
	void S_DropCurrentWeaponRequest();

	UFUNCTION(Server, Unreliable)
	void S_TakeWeaponRequest(ASCBaseWeapon* NewWeapon);

	UFUNCTION(NetMulticast, Unreliable)
	void M_AttachWeaponToHands(ASCBaseWeapon* NewWeapon);

	void DestroyCurrentWeapon();
	
protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

private:

	UFUNCTION()
	void OnRep_CurrentWeapon();

	ASCCharacter* GetCharOwner();
		
};
