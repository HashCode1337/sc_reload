// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_OutlineComponent.h"


void USC_OutlineComponent::OnRegister()
{
	USceneComponent::OnRegister();
	GetWorld()->InsertPostProcessVolume(this);
}

void USC_OutlineComponent::OnUnregister()
{
	USceneComponent::OnUnregister();
	GetWorld()->RemovePostProcessVolume(this);
}

void USC_OutlineComponent::Serialize(FArchive& Ar)
{
	USceneComponent::Serialize(Ar);

	if (Ar.IsLoading())
	{
#if WITH_EDITORONLY_DATA
		Settings.OnAfterLoad();
#endif
	}
}

void USC_OutlineComponent::BeginPlay()
{
	Super::BeginPlay();

	bIsEnabled = EnableOnStart;

	UpdateStencilValue();
	UpdateOutlineStatus();
	
}

void USC_OutlineComponent::UpdateStencilValue()
{
	Stencil = 0;
	switch (OutlineColor)
	{
	case EOutlineColor::White:
		Stencil = 1;
		break;
	case EOutlineColor::Red:
		Stencil = 2;
		break;
	case EOutlineColor::Green:
		Stencil = 3;
		break;
	case EOutlineColor::Blue:
		Stencil = 4;
		break;
	default:
		break;
	}
}

void USC_OutlineComponent::UpdateOutlineStatus()
{
	AActor* RootOwner = GetOwner();
	TArray<UActorComponent*> NeedToBeOutlined = RootOwner->GetComponentsByTag(UMeshComponent::StaticClass(), OutlineTag);

	for (UActorComponent* Comp : NeedToBeOutlined)
	{
		UPrimitiveComponent* CompCasted = Cast<UPrimitiveComponent>(Comp);
		CompCasted->SetRenderCustomDepth(bIsEnabled);
		CompCasted->SetCustomDepthStencilValue(Stencil);
	}

	DynamicOutlineMat = UMaterialInstanceDynamic::Create(OutlineMaterialInst, this);
	DynamicOutlineMat->SetScalarParameterValue("CustomStencil", Stencil);

	AddOrUpdateBlendable(DynamicOutlineMat->_getUObject(), 1.f);
}

void USC_OutlineComponent::SetOutlineEnabled(bool bIsCompEnabled)
{
	bIsEnabled = bIsCompEnabled;
	UpdateStencilValue();
	UpdateOutlineStatus();

	if (bIsCompEnabled)
	{
		GetWorld()->InsertPostProcessVolume(this);
	}
	else
	{
		GetWorld()->RemovePostProcessVolume(this);
	}

}

bool USC_OutlineComponent::GetOutlineEnabled()
{
	return bIsEnabled;
}
