// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_WeaponFireStatusHandler.h"
#include "../Core/SC_StaticLibrary.h"

USC_WeaponFireStatusHandler::USC_WeaponFireStatusHandler(const FObjectInitializer& ObjectInitializer)
{
	ASCCharacter* CastedOuter = GetTypedOuter<ASCCharacter>();

	if (CastedOuter)
	{
		CharOwner = CastedOuter;
	}
	else
	{
#if WITH_EDITOR
		//USC_StaticLibrary::Log(ELogType::Error, "USC_WeaponFireStatusHandler::Constructer Cant get correct owner!");
#endif
	}
}

bool USC_WeaponFireStatusHandler::IsCharacterCanAim()
{
	bool Result = 
		CharOwner->WeaponState == ECharacterWeaponState::Raised
		&& !CharOwner->Sprinting;

	return Result;
}

bool USC_WeaponFireStatusHandler::IsCharacterCanFire()
{
	bool Result = 
		CharOwner->WeaponState == ECharacterWeaponState::Raised
		|| CharOwner->WeaponState == ECharacterWeaponState::Aiming
		&& !CharOwner->Sprinting;

	return Result;
}

bool USC_WeaponFireStatusHandler::IsCharacterCanSprint()
{
	bool Result =
		CharOwner->WeaponState == ECharacterWeaponState::Lowered;

	return Result;
}

bool USC_WeaponFireStatusHandler::IsCharacterCanLowerWeapon()
{
	bool Result =
		CharOwner->WeaponState == ECharacterWeaponState::Raised;

	return Result;
}

bool USC_WeaponFireStatusHandler::IsCharacterCanRaiseWeapon()
{
	bool Result =
		CharOwner->WeaponState == ECharacterWeaponState::Lowered
		&& !CharOwner->Sprinting;

	return Result;
}
