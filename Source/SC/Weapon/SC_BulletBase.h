// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Sound/SoundCue.h"

#include "SC_BulletBase.generated.h"

class UProjectileMovementComponent;
class UParticleSystem;
class USphereComponent;
class UCurveFloat;

UCLASS(Blueprintable, BlueprintType)
class SC_API ASC_BulletBase : public AActor
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float RandomSpreadOnSpawn = 5.f;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float InitSpeed = 1000.f;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float MaxSpeed = 1000.f;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float LifeTime = 3.f;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	uint8 BulletsPreShot = 1;

	// To calculate bullet dmg that depends of distance between instigator and victim
	UPROPERTY(BlueprintReadOnly)
	FVector BulletSpawnLocation;

protected:

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float MaxEffectiveDistance = 10000.f;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	USphereComponent* BulletCollision;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* BulletMesh;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	UParticleSystemComponent* TracerFX;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFX;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TMap<TEnumAsByte<EPhysicalSurface>, USoundCue*> HitSound;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UDamageType> DamageType = UDamageType::StaticClass();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	UCurveFloat* DamageDistanceFalloff;

	
public:	
	ASC_BulletBase();

	UFUNCTION(BlueprintPure)
	float GetActualDamage();

	UFUNCTION()
	void OnHitBullet(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION(BlueprintImplementableEvent)
	void OnHitTest(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

};
