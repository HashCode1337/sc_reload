// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SC_WeaponModuleBase.h"
#include "SC_WeaponModuleSimple.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, meta = (BlueprintSpawnableComponent))
class SC_API USC_WeaponModuleSimple : public USC_WeaponModuleBase
{
	GENERATED_BODY()

public:
	USC_WeaponModuleSimple();
	
};
