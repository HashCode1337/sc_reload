// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SC_WeaponModuleBase.h"
#include "SC_WeaponModuleRails.generated.h"

/**
 * 
 */
UCLASS()
class SC_API USC_WeaponModuleRails : public USC_WeaponModuleBase
{
	GENERATED_BODY()

public:

	USC_WeaponModuleRails();
	
};
