// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_WeaponModuleRails.h"

USC_WeaponModuleRails::USC_WeaponModuleRails()
{
	DesiredSocket = "Proxy_rail_bottom";
	ModuleType = EWeaponModuleType::Rail;
	RequiredForWeaponFunctionality = false;
}
