// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SkeletalMeshComponent.h"
#include "../Core/SC_CommonDataModels.h"
#include "SC_WeaponModuleBase.generated.h"

class UPrimitiveComponent;

UCLASS( ClassGroup=(Custom) )
class SC_API USC_WeaponModuleBase : public USkeletalMeshComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USC_WeaponModuleBase();
	virtual void SetupAttachment(UPrimitiveComponent* Parent);

	UFUNCTION(BlueprintCallable)
	void SetDesiredSocket(FName NewSocketName);

	UFUNCTION(BlueprintPure)
	EWeaponModuleType GetModuleType();

protected:
	
	UPROPERTY(BlueprintReadOnly)
	UPrimitiveComponent* ModuleParent;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "SC Module Settings", meta = (AllowPrivateAccess = "true"))
	FName DesiredSocket = TEXT("None");

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "SC Module Settings", meta = (AllowPrivateAccess = "true"))
	EWeaponModuleType ModuleType;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "SC Module Settings", meta = (AllowPrivateAccess = "true"))
	bool RequiredForWeaponFunctionality = false;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "SC Module Settings", meta = (AllowPrivateAccess = "true"))
	float Mass;

	virtual void BeginPlay() override;


public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

};
