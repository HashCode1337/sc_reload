// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_WeaponMagazineBase.h"
#include "Kismet/KismetSystemLibrary.h"

USC_WeaponMagazineBase::USC_WeaponMagazineBase()
{
	ResetEditorDefaultDetails();
}

void USC_WeaponMagazineBase::OnRep_LoadedBullets()
{
	UKismetSystemLibrary::PrintString(GetWorld(), "BulletGone");
}

void USC_WeaponMagazineBase::ResetEditorDefaultDetails()
{
	if (AllowedBullets.Num() <= 0)
	{
		AllowedBullets.Add(DefaultBulletClass);
	}

	if (LoadedBullets.Num() <= 0)
	{
		for (int i = 0; i < Capacity; i++)
		{
			LoadedBullets.Add(DefaultBulletClass);
		}
	}
}

bool USC_WeaponMagazineBase::TakeFirstBullet(TSubclassOf<ASC_BulletBase>& TopBullet)
{
	if (LoadedBullets.Num() <= 0)
	{
		return false;
	}
	else
	{
		LoadedBullets.HeapPop(TopBullet, true);
	}

	return true;
}

bool USC_WeaponMagazineBase::CheckFirstBullet(TSubclassOf<ASC_BulletBase>& TopBullet, bool bHasAuthority)
{
	if (LoadedBullets.Num() <= 0)
	{
		return false;
	}

	if (!bHasAuthority)
	{
		TopBullet = LoadedBullets[0];
	}

	return true;
}

bool USC_WeaponMagazineBase::InsertNewBullet(const TSubclassOf<ASC_BulletBase>& NewBullet)
{
	if (LoadedBullets.Num() >= Capacity)
	{
		return false;
	}

	TArray<TSubclassOf<ASC_BulletBase>> NewLoadedBullets;
	NewLoadedBullets.Add(NewBullet);

	for (int i = 0; i < LoadedBullets.Num(); i++)
	{
		NewLoadedBullets.Add(LoadedBullets[i]);
	}

	LoadedBullets = NewLoadedBullets;

	return true;
}

bool USC_WeaponMagazineBase::FillMagWith(const TSubclassOf<ASC_BulletBase>& NewBullet)
{
	if (LoadedBullets.Num() >= Capacity)
	{
		return false;
	}

	int32 Difference = Capacity - LoadedBullets.Num();

	for (int i = 0; i < Difference; i++)
	{
		LoadedBullets.Add(NewBullet);
	}

	return true;
}

int32 USC_WeaponMagazineBase::GetLoadedBulletsCount()
{
	return LoadedBullets.Num();
}

#if WITH_EDITOR

void USC_WeaponMagazineBase::PreEditChange(FProperty* PropertyAboutToChange)
{
	ResetEditorDefaultDetails();
	Super::PreEditChange(PropertyAboutToChange);
}

#endif

void USC_WeaponMagazineBase::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME_CONDITION(USC_WeaponMagazineBase, LoadedBullets, COND_None);
}

bool USC_WeaponMagazineBase::IsSupportedForNetworking() const
{
	return true;
}

bool USC_WeaponMagazineBase::CallRemoteFunction(UFunction* Function, void* Parms, struct FOutParmRec* OutParms, FFrame* Stack)
{
	check(!HasAnyFlags(RF_ClassDefaultObject));
	AActor* Owner = GetTypedOuter<AActor>();
	UNetDriver* NetDriver = Owner->GetNetDriver();
	if (NetDriver)
	{
		NetDriver->ProcessRemoteFunction(Owner, Function, Parms, OutParms, Stack, this);
		return true;
	}
	return false;

}

int32 USC_WeaponMagazineBase::GetFunctionCallspace(UFunction* Function, FFrame* Stack)
{
	return GetOuter()->GetFunctionCallspace(Function, Stack);
}

void USC_WeaponMagazineBase::Destroy()
{
	if (!IsValid(this))
	{
		checkf(GetTypedOuter<AActor>()->HasAuthority() == true, TEXT("Destroy:: Object does not have authority to destroy itself!"));
	}
}
