// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_WeaponModuleSimple.h"

USC_WeaponModuleSimple::USC_WeaponModuleSimple()
{
	DesiredSocket = "Proxy_Stock";
	ModuleType = EWeaponModuleType::Stock;
	RequiredForWeaponFunctionality = false;
}