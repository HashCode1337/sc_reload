// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_WeaponModuleBase.h"
#include "Components/SceneComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../Core/SC_StaticLibrary.h"

USC_WeaponModuleBase::USC_WeaponModuleBase()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void USC_WeaponModuleBase::SetupAttachment(UPrimitiveComponent* Parent)
{
	ModuleParent = Parent;

	if (Parent->DoesSocketExist(DesiredSocket))
	{
		FAttachmentTransformRules Rules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, true);
		AttachToComponent(Parent, Rules, DesiredSocket);
	}
}

void USC_WeaponModuleBase::SetDesiredSocket(FName NewSocketName)
{
	DesiredSocket = NewSocketName;
	if (ModuleParent)
	{
		SetupAttachment(ModuleParent);
	}
	else
	{
		USC_StaticLibrary::Log(ELogType::Error, TEXT("ERROR! USC_WeaponModuleBase::SetDesiredSocket() Module has not Parent!"));
	}
}

EWeaponModuleType USC_WeaponModuleBase::GetModuleType()
{
	return ModuleType;
}

void USC_WeaponModuleBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void USC_WeaponModuleBase::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

