// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_BulletBase.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/SphereComponent.h"
#include "Curves/CurveFloat.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "../Core/SC_StaticLibrary.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "UObject/Class.h"

ASC_BulletBase::ASC_BulletBase()
{
	PrimaryActorTick.bCanEverTick = true;
	
	BulletCollision = CreateDefaultSubobject<USphereComponent>("BulletCollision");
	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>("BulletMesh");
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovement");
	TracerFX = CreateDefaultSubobject<UParticleSystemComponent>("TracerFX");
	
	BulletCollision->SetUseCCD(true);
	BulletCollision->SetCanEverAffectNavigation(false);
	BulletCollision->bReturnMaterialOnMove = true;

	BulletCollision->SetSphereRadius(1.f);
	BulletCollision->SetCollisionProfileName("Projectile");

	RootComponent = BulletCollision;

	BulletMesh->SetupAttachment(RootComponent);
	TracerFX->SetupAttachment(RootComponent);

	ProjectileMovement->InitialSpeed = InitSpeed;
	ProjectileMovement->MaxSpeed = MaxSpeed;
	
	NetUpdateFrequency = 1.f;
	MinNetUpdateFrequency = 0.f;
	bReplicates = true;

	BulletCollision->OnComponentHit.AddDynamic(this, &ASC_BulletBase::OnHitBullet);

}

float ASC_BulletBase::GetActualDamage()
{
	float PastDistance = FVector::Distance(BulletSpawnLocation, RootComponent->GetComponentLocation());
	return DamageDistanceFalloff->GetFloatValue(PastDistance);
}

void ASC_BulletBase::OnHitBullet(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!Hit.bBlockingHit) return;

	// Apply Damage on server
	if (HasAuthority())
	{
		AController* InstigatorController = GetInstigator()->GetController();
		UGameplayStatics::ApplyPointDamage(OtherActor, GetActualDamage(), BulletSpawnLocation, Hit, InstigatorController, GetInstigator(), DamageType);
	}

	// Exit if dedic
	if (UKismetSystemLibrary::IsDedicatedServer(GetWorld())) return;

	EPhysicalSurface HitSurface = SurfaceType_Default;

	if (Hit.PhysMaterial->SurfaceType != EPhysicalSurface::SurfaceType_Default)
	{
		HitSurface = Hit.PhysMaterial->SurfaceType;

		FString EnumName = UEnum::GetValueAsString<EPhysicalSurface>(HitSurface);
		USC_StaticLibrary::PrintMsg(0, FColor::Purple, "ASC_BulletBase::OnHitBullet Hit - " + EnumName);
	}
	else
	{
		// Try get mask from material
		FHitResult HitRes;

		FCollisionQueryParams QP;
		QP.bReturnPhysicalMaterial = true;
		QP.bTraceComplex = true;
		QP.bReturnFaceIndex = true;

		FCollisionResponseParams QRP;

		FVector Dir = Hit.TraceEnd - Hit.TraceStart;
		Dir.Normalize();

		FVector StartLoc = Hit.Location + (Dir * 10.f);
		FVector EndLoc = Hit.Location + (Dir * -10.f);

		ECollisionChannel Chanel = ECC_Visibility;

		GetWorld()->LineTraceSingleByChannel
		(
			HitRes,
			StartLoc,
			EndLoc,
			Chanel,
			QP,
			QRP
		);

		// Visual debug trace
		//UKismetSystemLibrary::DrawDebugLine(GetWorld(), StartLoc, EndLoc, FColor::Black, 10.f, 1.f);
		

		FVector2D UV;
		int32 SurfaceIndex = USC_StaticLibrary::GetPhysMatIndex(HitRes, 0, UV);
		HitSurface = (EPhysicalSurface)SurfaceIndex;

		/*USC_StaticLibrary::PrintMsg(1, FColor::Purple, FString::FromInt(SurfaceIndex));
		
		if (SurfaceIndex != -1)
		{
			FString EnumName = UEnum::GetValueAsString<EPhysicalSurface>((EPhysicalSurface)SurfaceIndex);
			USC_StaticLibrary::PrintMsg(0, FColor::Purple, "ASC_BulletBase::OnHitBullet Hit - " + EnumName);
		}
		else
		{
			FString EnumName = UEnum::GetValueAsString<EPhysicalSurface>((EPhysicalSurface)SurfaceIndex);
			USC_StaticLibrary::PrintMsg(0, FColor::Purple, "ASC_BulletBase::OnHitBullet Hit - " + EnumName);
		}*/

	}

	// VFX
	if (HitFX.Contains(HitSurface))
	{

		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFX[HitSurface], Hit.Location, Hit.Normal.Rotation(), true);
	}

	// Sound
	if (HitSound.Contains(HitSurface))
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), HitSound[HitSurface], Hit.Location, Hit.Normal.Rotation());
	}

	// Hide Tracer
	TracerFX->SetVisibility(false);

	// Test in BP - ToDo - Remove it
	OnHitTest(HitComponent, OtherActor, OtherComp, NormalImpulse, Hit);
}

void ASC_BulletBase::BeginPlay()
{
	Super::BeginPlay();

	BulletCollision->IgnoreActorWhenMoving(GetOwner(), true);
	BulletCollision->IgnoreActorWhenMoving(GetInstigator(), true);
	BulletSpawnLocation = RootComponent->GetComponentLocation();

	SetLifeSpan(LifeTime);
}

void ASC_BulletBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
