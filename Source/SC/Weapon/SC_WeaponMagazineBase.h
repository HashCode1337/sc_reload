// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SC_BulletBase.h"
#include "Containers/Queue.h"
#include "SC/Core/SC_StaticLibrary.h"
#include "SC_WeaponMagazineBase.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class SC_API USC_WeaponMagazineBase : public UObject
{
	GENERATED_BODY()

private:

	USC_WeaponMagazineBase();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true", ExposeOnSpawn = "true"))
	bool SpawnFullLoaded = true;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMesh* Mesh;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	uint8 Capacity = 30;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<ASC_BulletBase> DefaultBulletClass = ASC_BulletBase::StaticClass();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<ASC_BulletBase>> AllowedBullets;

	UPROPERTY(ReplicatedUsing = OnRep_LoadedBullets, BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<ASC_BulletBase>> LoadedBullets;
	UFUNCTION()
	void OnRep_LoadedBullets();

private:
	UFUNCTION()
	void ResetEditorDefaultDetails();

public:
	UFUNCTION(BlueprintCallable)
	bool TakeFirstBullet(TSubclassOf<ASC_BulletBase>& TopBullet);

	UFUNCTION(BlueprintCallable)
	bool CheckFirstBullet(TSubclassOf<ASC_BulletBase>& TopBullet, bool bHasAuthority);

	UFUNCTION(BlueprintCallable)
	bool InsertNewBullet(const TSubclassOf<ASC_BulletBase>& NewBullet);

	UFUNCTION(BlueprintCallable)
	bool FillMagWith(const TSubclassOf<ASC_BulletBase>& NewBullet);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetLoadedBulletsCount();

#if WITH_EDITOR

	void PreEditChange(FProperty* PropertyAboutToChange) override;

#endif

	void GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintPure)
	bool IsSupportedForNetworking() const override;

	bool CallRemoteFunction(UFunction* Function, void* Parms, struct FOutParmRec* OutParms, FFrame* Stack) override;
	int32 GetFunctionCallspace(UFunction* Function, FFrame* Stack) override;
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	void Destroy();

};
