// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_WeaponModuleBarrel.h"

USC_WeaponModuleBarrel::USC_WeaponModuleBarrel()
{
	DesiredSocket = "Proxy_Barrel";
	ModuleType = EWeaponModuleType::Barrel;
	RequiredForWeaponFunctionality = true;

}

bool USC_WeaponModuleBarrel::GetSocketMuzzleLocationAndRotation(FVector& Loc, FRotator& Rot)
{
	bool Result = DoesSocketExist(SocketMuzzle);
	if (Result)
	{
		GetSocketWorldLocationAndRotation(SocketMuzzle, Loc, Rot);
	}

	return Result;
}

FName USC_WeaponModuleBarrel::GetSocketMuzzleName()
{
	return SocketMuzzle;
}
