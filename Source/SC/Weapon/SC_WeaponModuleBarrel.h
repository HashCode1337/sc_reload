// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SC_WeaponModuleBase.h"
#include "SC_WeaponModuleBarrel.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, meta = (BlueprintSpawnableComponent))
class SC_API USC_WeaponModuleBarrel : public USC_WeaponModuleBase
{
	GENERATED_BODY()

protected:

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "SC Module Settings", meta = (AllowPrivateAccess = "true"))
	FName SocketMuzzle = TEXT("S_Muzzle");

public:


	USC_WeaponModuleBarrel();

	UFUNCTION(BlueprintPure)
	bool GetSocketMuzzleLocationAndRotation(FVector& Loc, FRotator& Rot);

	UFUNCTION(BlueprintPure)
	FName GetSocketMuzzleName();

	
};
