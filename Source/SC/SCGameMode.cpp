// Copyright Epic Games, Inc. All Rights Reserved.

#include "SCGameMode.h"
#include "SCHUD.h"
#include "SCCharacter.h"
#include "Core/SC_PlayerState.h"
#include "Core/SC_StaticLibrary.h"
#include "Pawns/SC_PreloginPawn.h"
#include "UObject/ScriptInterface.h"
#include "UObject/ConstructorHelpers.h"

ASCGameMode::ASCGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	//DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	//HUDClass = ASCHUD::StaticClass();
}

void ASCGameMode::StartPlay()
{
	Super::StartPlay();
}

void ASCGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ASCGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
}

void ASCGameMode::Logout(AController* Exiting)
{
	ISC_DatabaseSavable* ExitingPawn = Cast<ISC_DatabaseSavable>(Exiting->GetPawn());
	USC_StaticLibrary::Log(ELogType::Warn, FString::Printf(TEXT("Logout Request: %s"), *Exiting->GetName()));
}

void ASCGameMode::PlayerSpawnRequest_Implementation(const FString& Session, ASC_PreloginPawn* PrelogindPawn)
{
	USC_StaticLibrary::Log(ELogType::Warn, FString::Printf(TEXT("Spawn Request: Session %s"), *Session));
}

void ASCGameMode::SaveRequest_Implementation(const TScriptInterface<ISC_DatabaseSavable>& SavingObject)
{
	//USC_StaticLibrary::Log(ELogType::Warn, FString::Printf(TEXT("Save Request: Session %s"), *Session));
}

ETeamAttitude::Type ASCGameMode::GetFactionRelations(AActor* A, AActor* B)
{
	IGenericTeamAgentInterface* ATeam = Cast<IGenericTeamAgentInterface>(A);
	IGenericTeamAgentInterface* BTeam = Cast<IGenericTeamAgentInterface>(B);

	if (ATeam && BTeam)
	{
		EFaction AFaction = static_cast<EFaction>(ATeam->GetGenericTeamId().GetId());
		EFaction BFaction = static_cast<EFaction>(BTeam->GetGenericTeamId().GetId());

		if (AFaction == BFaction)
		{
			return ETeamAttitude::Friendly;
		}


		if (FactionRelations.Contains(AFaction))
		{
			TArray<EFaction> Enemies = FactionRelations[AFaction].Enemies;

			if (Enemies.Contains(BFaction))
			{
				return ETeamAttitude::Hostile;
			}
			else
			{
				return ETeamAttitude::Neutral;
			}
		}
	}

	return ETeamAttitude::Neutral;
}
