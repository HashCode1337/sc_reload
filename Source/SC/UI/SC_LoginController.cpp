// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_LoginController.h"
#include "../Core/SC_StaticLibrary.h"
#include "../Core/SC_GameInstance.h"

#include "JsonObjectConverter.h"

#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"
#include "../SubSystems/SC_MetaDataSubsystem.h"
#include "../SubSystems/SC_GameConfigSubsystem.h"

DEFINE_LOG_CATEGORY(LogLoginController);

void USC_LoginController::TryLogin(FString Login, FString Password)
{

	FString NameRgxPtrn = USC_StaticLibrary::GetNicknameRgxPattern();
	FString PwdRgxPtrn = USC_StaticLibrary::GetPasswordRgxPattern();

	if (!USC_StaticLibrary::RegexMatch(NameRgxPtrn, Login))
	{
		OnLogged.Broadcast(false, ELoginError::BadLogin);
		return;
	}

	if (!USC_StaticLibrary::RegexMatch(PwdRgxPtrn, Password))
	{
		OnLogged.Broadcast(false, ELoginError::BadPassword);
		return;
	}


	SendRequest(Login, Password);
}

void USC_LoginController::SendRequest(FString Login, FString Password)
{
	FGameConfig GC;
	USC_GameConfigSubsystem* GCS = GetWorld()->GetGameInstance()->GetSubsystem<USC_GameConfigSubsystem>();
	GC = GCS->GetGameConfig();

	FString RequestBody = FString::Printf(TEXT("/login?login=%s&password=%s"), *Login, *Password);
	FString Url = GC.Backend + RequestBody;

	FHttpModule& httpModule = FHttpModule::Get();
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> pRequest = httpModule.CreateRequest();
	pRequest->SetVerb(TEXT("GET"));
	pRequest->SetURL(Url);

	pRequest->OnProcessRequestComplete().BindLambda(
		[&](FHttpRequestPtr pRequest, FHttpResponsePtr pResponse, bool connectedSuccessfully) 
		{

			if (connectedSuccessfully) {
				auto resp = pRequest->GetResponse();
				resp->GetContentAsString();
				auto backendAnswer = resp->GetContentAsString();

				HandleAnswer(backendAnswer);

			}
			else
			{
				switch (pRequest->GetStatus()) {
				case EHttpRequestStatus::Failed_ConnectionError:
					UE_LOG(LogTemp, Error, TEXT("Connection failed."));
				default:
					UE_LOG(LogTemp, Error, TEXT("Request failed."));
				}
			}
		});

	pRequest->ProcessRequest();
}

void USC_LoginController::HandleAnswer(FString Answer)
{
	FBackendAnswer AnswerModel;

	if (FJsonObjectConverter::JsonObjectStringToUStruct(Answer, &AnswerModel))
	{
		if (!AnswerModel.Success)
		{
			if (AnswerModel.Error == "BadCredentials")
			{
				OnLogged.Broadcast(false, ELoginError::InvalidCredentials);
				return;
			}
			if (AnswerModel.Error == "AccountNotFound")
			{
				OnLogged.Broadcast(false, ELoginError::NotExist);
				return;
			}
			if (AnswerModel.Error == "AccountNotActivated")
			{
				OnLogged.Broadcast(false, ELoginError::AccountNotActivated);
				return;
			}
			if (AnswerModel.Error == "NotWhiteListed")
			{
				OnLogged.Broadcast(false, ELoginError::NotWhiteListed);
				return;
			}
			if (AnswerModel.Error == "OnlyDevsMode")
			{
				OnLogged.Broadcast(false, ELoginError::OnlyDevsMode);
				return;
			}
		}
		else
		{
			OnLogged.Broadcast(true, ELoginError::Unknown);
			GI->GetSubsystem<USC_MetaDataSubsystem>()->SetCommonUserData(AnswerModel.Answer);
		}
	}
}
