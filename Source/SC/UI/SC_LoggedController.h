// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SC_ViewControllerBase.h"
#include "SC_LoggedController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLoggedIn, bool, bSuccess, ELoggedError, Error);

/**
 * 
 */
UCLASS()
class SC_API USC_LoggedController : public USC_ViewControllerBase
{
	GENERATED_BODY()

	UPROPERTY(BlueprintAssignable, Category = "SC")
	FOnLoggedIn OnLogged;

	UFUNCTION(BlueprintCallable)
	void TrySetNickname(FString Nickname);

	virtual void HandleAnswer(FString Answer) override;
	
};
