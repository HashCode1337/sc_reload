// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_ViewControllerBase.h"
#include "../Core/SC_StaticLibrary.h"

USC_ViewControllerBase::USC_ViewControllerBase()
{
	if (GEngine && GEngine->GetWorld())
	{
		if (UGameInstance* NGI = GEngine->GetWorld()->GetGameInstance())
			GI = NGI;
		else
			USC_StaticLibrary::Log(ELogType::Error, "USC_ViewControllerBase::USC_ViewControllerBase() GameInstance Error");
	}
}

USC_ViewControllerBase::~USC_ViewControllerBase()
{
}

void USC_ViewControllerBase::HandleAnswer(FString Answer)
{
}

void USC_ViewControllerBase::Destroy()
{
	BeginDestroy();
}

void USC_ViewControllerBase::InitScreen_Implementation(EViewType Type)
{
}
