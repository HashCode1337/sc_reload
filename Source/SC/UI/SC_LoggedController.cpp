// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_LoggedController.h"
#include "../Core/SC_StaticLibrary.h"

#include "JsonObjectConverter.h"

#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"

#include "../Core/SC_GameInstance.h"
#include "../SubSystems/SC_MetaDataSubsystem.h"
#include "../SubSystems/SC_GameConfigSubsystem.h"

void USC_LoggedController::TrySetNickname(FString NewNickname)
{
	FGameConfig GC;

	USC_GameInstance* CGI = Cast<USC_GameInstance>(GI);
	USC_MetaDataSubsystem* MDS = GI->GetSubsystem<USC_MetaDataSubsystem>();

	FString Login = MDS->GetCommonUserData().account;
	FString Pwd = MDS->GetCommonUserData().password;
	FString Session = MDS->GetCommonUserData().session;

	USC_GameConfigSubsystem* GCS = GetWorld()->GetGameInstance()->GetSubsystem<USC_GameConfigSubsystem>();
	GC = GCS->GetGameConfig();

	FString RequestBody = FString::Printf(TEXT("/setname?login=%s&password=%s&session=%s"), *Login, *Pwd, *Session);

	FString Url = GC.Backend + RequestBody;

	FHttpModule& httpModule = FHttpModule::Get();
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> pRequest = httpModule.CreateRequest();
	pRequest->SetVerb(TEXT("POST"));
	
	pRequest->SetURL(*Url);

	pRequest->SetHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
	pRequest->AppendToHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	
	FString ContentPOST = FString::Printf(TEXT("nickname=%s"), *NewNickname);
	pRequest->SetContentAsString(*ContentPOST);

	pRequest->OnProcessRequestComplete().BindLambda(
		[&](FHttpRequestPtr pRequest, FHttpResponsePtr pResponse, bool connectedSuccessfully)
		{

			if (connectedSuccessfully) {
				auto resp = pRequest->GetResponse();
				resp->GetContentAsString();
				auto backendAnswer = resp->GetContentAsString();
				backendAnswer = resp->GetContentAsString();


				USC_StaticLibrary::Log(ELogType::Log, *backendAnswer);
				HandleAnswer(backendAnswer);
			}
			else
			{
				switch (pRequest->GetStatus()) {
				case EHttpRequestStatus::Failed_ConnectionError:
					UE_LOG(LogTemp, Error, TEXT("Connection failed."));
				default:
					UE_LOG(LogTemp, Error, TEXT("Request failed."));
				}
			}
		});

	pRequest->ProcessRequest();
}

void USC_LoggedController::HandleAnswer(FString Answer)
{
	Super::HandleAnswer(Answer);

	FBackendAnswer AnswerModel;

	if (FJsonObjectConverter::JsonObjectStringToUStruct(Answer, &AnswerModel))
	{
		if (!AnswerModel.Success)
		{
			if (AnswerModel.Error == "InvalidNickname")
			{
				OnLogged.Broadcast(false, ELoggedError::InvalidNickname);
				return;
			}
			if (AnswerModel.Error == "NicknameAlreadyInUse")
			{
				OnLogged.Broadcast(false, ELoggedError::NicknameAlreadyInUse);
				return;
			}
		}
		else
		{
			OnLogged.Broadcast(true, ELoggedError::Unknown);
			USC_GameInstance* SCGI = Cast<USC_GameInstance>(GI);

			if (SCGI)
			{
				//SCGI->SetUserSelfData(AnswerModel.Answer);
			}
			else
			{
				//UE_LOG(LogLoginController, Fatal, TEXT("ERROR: USC_LoginController::HandleAnswer(), Can't get SC_GameInstance"));
			}

		}
	}

}
