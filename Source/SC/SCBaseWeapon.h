// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SC/Core/SC_CommonDataModels.h"
#include "SC/Weapon/SC_BulletBase.h"
#include "Components/SC_OutlineComponent.h"
#include "SC/Interfaces/SC_Outlinable.h"
#include "Interfaces/SC_Interactable.h"
#include "SCBaseWeapon.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FWeaponFiredSignature, UAnimMontage*, TPFireMontage, UAnimMontage*, FPFireMontage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FWeaponReloadSignature, UAnimMontage*, TPFireMontage, UAnimMontage*, FPFireMontage);

class ASCProjectile;
class UParticleSystemComponent;
class UAudioComponent;
class USC_WeaponMagazineBase;
class UAnimMontage;
class USC_WeaponModuleBase;

UCLASS(Blueprintable)
class SC_API ASCBaseWeapon : public AActor, public ISC_Outlinable, public ISC_Interactable
{
	GENERATED_BODY()
public:

#pragma region Settings
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	USceneComponent* SceneComp = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class USC_OutlineComponent* OutlineComp = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class USC_RecoilComponent* RecoilComp = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class USC_WeaponFireModeHandler* FireModeComp = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* WeaponMesh = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FVector FPMeshOffset = FVector::ZeroVector;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float RPM = 100.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	int32 BurstBullets = 3;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	bool bSupportSuperBullet = false;
		
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true", EditCondition = "bSupportSuperBullet"))
	float SuperBulletCooldown = 1.f;

	UPROPERTY(BlueprintReadOnly)
	float LastShotTime = -(60.f / RPM);

	UPROPERTY(BlueprintReadOnly)
	bool bIsReloading;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UParticleSystemComponent* FiredFX = nullptr;

	// Must be spawned by weapon and attached to muzzle socket
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UParticleSystem* ShotFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	USoundBase* ShotSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	USoundBase* EmptySound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	USoundBase* SwitchSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* WeaponFire;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* WeaponReload;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* FirstPersonFire;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* FirstPersonReload;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ThirdPersonFireAim;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ThirdPersonFireNoAim;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ThirdPersonReload;

#pragma endregion

	UPROPERTY(BlueprintReadWrite, Replicated, meta = (ExposeOnSpawn = "true"))
	APawn* PawnOwner = nullptr;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	EWeaponClass WeaponClassEnum;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UTexture2D* WeaponImage;

	UPROPERTY(BlueprintReadOnly)
	TArray<USC_WeaponModuleBase*> WeaponInstalledModules;

	UPROPERTY(BlueprintAssignable)
	FWeaponFiredSignature WeaponFired;
	
	UPROPERTY(BlueprintAssignable)
	FWeaponReloadSignature WeaponReloaing;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TSubclassOf<USC_WeaponMagazineBase> TestMagazineType;

#pragma region Sockets
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FName SocketAim = TEXT("S_Aim");
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FName SocketGrip = TEXT("S_Grip");
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FName SocketMuzzle = TEXT("S_Muzzle");
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FName SocketHand = TEXT("S_Weapon");
#pragma endregion

	void OnRep_AttachmentReplication() override;
	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	void OnConstruction(const FTransform& Transform) override;
	void SetOwner(AActor* NewOwner) override;
	void ResetAttachToRoot();

public:

	// Interfaces
	bool IsOutlined() override { return OutlineComp->GetOutlineEnabled(); };
	
	UFUNCTION(BlueprintCallable)
	void SetOutlineEnabled(bool bEnableOutline) override { OutlineComp->SetOutlineEnabled(bEnableOutline); };

	bool CanBeOutlined() override { return !IsValid(PawnOwner); };
	EInteractableObjectType GetInteractType() override { return EInteractableObjectType::Weapon; };
	bool IsInteractable() override { return !IsValid(PawnOwner); };

private:
	void UpdateInstalledModules();


public:
	// Sets default values for this actor's properties
	ASCBaseWeapon();

	UFUNCTION(BlueprintCallable)
	FTransform GetAimSocketTransform(ERelativeTransformSpace RTS);

	bool IsCanFire();
	bool IsFireCoolDownFinished();

	UFUNCTION(BlueprintCallable)
	bool L_TryFire();
	
	UFUNCTION(Server, Unreliable, BlueprintCallable)
	void S_TryFire(FVector Loc, FRotator Rot);

	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void M_TryFire();

	UFUNCTION(Server, Unreliable)
	void S_EmptySoundRequest();

	UFUNCTION(NetMulticast, Unreliable)
	void M_EmptySoundRequest();

	UFUNCTION(BlueprintCallable)
	bool TryReloadWeapon(USC_WeaponMagazineBase* NewMagazine);

	UFUNCTION(Server, Unreliable, BlueprintCallable)
	void S_Reload();

	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
	void M_Reload();

	UFUNCTION()
	void BindReloadTimer(bool bIsServer);

	UFUNCTION(BlueprintCallable)
	USC_WeaponMagazineBase* SetCurrentWeaponMagazine(USC_WeaponMagazineBase* NewMagazine);

	UFUNCTION(BlueprintCallable)
	virtual void SetWeaponMeshRelativeRotation(FRotator Rot);

	UFUNCTION(BlueprintCallable)
	virtual void PlayFireSound();

	UFUNCTION(BlueprintCallable)
	virtual void PlayFireFX();

	UFUNCTION(BlueprintCallable)
	virtual void PlayFireAnim();

	UFUNCTION(BlueprintCallable)
	virtual void GetMuzzleSocket(FVector& Loc, FRotator& Rot);

	UFUNCTION(BlueprintCallable)
	void SetNotOwned();

	UFUNCTION(BlueprintPure)
	USkeletalMeshComponent* GetWeaponMesh() const { return WeaponMesh; };

public:

	UPROPERTY(ReplicatedUsing = OnRep_CurrentLoadedMagazine, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USC_WeaponMagazineBase* CurrentLoadedMagazine;
	UFUNCTION()
	void OnRep_CurrentLoadedMagazine();

	UPROPERTY(ReplicatedUsing = OnRep_CurrentLoadedBullet, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<ASC_BulletBase> CurrentLoadedBullet;
	UFUNCTION()
	void OnRep_CurrentLoadedBullet();

	void OnRep_Owner() override;

protected:
	virtual void BeginPlay() override;

public:	

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;
};
